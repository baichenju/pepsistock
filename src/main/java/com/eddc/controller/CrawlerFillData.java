///**   
// * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
// * 
// * @Package: com.eddc.controller 
// * @author: jack.zhao   
// * @date: 2017年12月18日 上午10:04:42 
// */
//package com.eddc.controller;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import javax.servlet.http.HttpServletRequest;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.eddc.model.CommodityPrices;
//import com.eddc.model.CrawKeywordsInfo;
//import com.eddc.model.JobAndTrigger;
//import com.eddc.model.QrtzCrawlerTable;
//import com.eddc.service.CrawlerPublicClassService;
//import com.eddc.service.JobAndTriggerService;
//import com.eddc.task.FillCatch_All_CrawlTask;
//import com.eddc.task.GuoMei_DataCrawlTask;
//import com.eddc.task.Jd_DataCrawlTask;
//import com.eddc.util.Fields;
//import com.eddc.util.SpringContextUtil;
//import com.eddc.util.Validation;
//import com.eddc.util.WhetherShelves;
//import com.eddc.util.publicClass;
//import com.github.pagehelper.PageInfo;
//import com.google.gson.Gson;
//
///**   
// *    
// * 项目名称：Price_monitoring_crawler   
// * 类名称：CrawlerFillData   
// * 类描述：   数据抓取失败进行不抓
// * 创建人：jack.zhao   
// * 创建时间：2017年12月18日 上午10:04:42   
// * 修改人：jack.zhao   
// * 修改时间：2017年12月18日 上午10:04:42   
// * 修改备注：   
// * @version    
// *    
// */
//@RestController
//@RequestMapping(value="/crawler")
//public class CrawlerFillData extends BaseController{
//	private static Logger logger = LoggerFactory.getLogger(CrawlerFillData.class);
//	private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	@Autowired 
//	private JobAndTriggerService jobAndTriggerService;
//	@Autowired
//	WhetherShelves whetherShelves;
//	@Autowired
//	private CrawlerPublicClassService crawlerPublicClassService;
//	private String regex=".*[a-zA-Z]+.*";  
//	@PostMapping(value="/query_rawler_fill_catch")
//	public String query_rawler_fill_catch(HttpServletRequest request) {
//		Gson gson=new Gson();
//		logger.info("补抓数据：当前时间"+data.format(new Date()));
//		List<QrtzCrawlerTable> rawler_fill = jobAndTriggerService.query_rawler_fill_catch();
//		return gson.toJson(rawler_fill);
//	}  
// 
//
//	@PostMapping(value="/crawler_failure_fill")//开始补抓数据
//	@ResponseBody
//	public String crawler_failure_fill(HttpServletRequest request) throws Exception{
//		Map<String, Object> map=getRequestParams(request);
//		String Platform= map.get("platform").toString();
//		String user_Id=map.get("user_Id").toString();
//		String dataSource=map.get("dataSource").toString();
//		String data_time=map.get("data_time").toString().replace(","," ");
//		String databases=map.get("databases").toString();
//		Gson gson=new Gson();
//		int jj=0;
//		ApplicationContext contexts= SpringContextUtil.getApplicationContext();
//		ExecutorService taskExecutor = Executors.newFixedThreadPool(Integer.valueOf(map.get("thread_sum").toString()));  //初始化线程池
//		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndGrabFailureGoodsData(user_Id,databases,Platform,null,Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据 
//		for(CrawKeywordsInfo accountInfo:list){
//			FillCatch_All_CrawlTask dataCrawlTask = (FillCatch_All_CrawlTask) contexts.getBean(FillCatch_All_CrawlTask.class);
//			dataCrawlTask.setDataType(Platform);
//			dataCrawlTask.setTimeDate(data_time);
//			dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
//			dataCrawlTask.setType(Fields.STYPE_1); 
//			dataCrawlTask.setAccountId(user_Id); 
//			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
//			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(dataSource)); 
//			if(Platform.equals(Fields.GUOMEI)){
//				dataCrawlTask.setSetUrl(Fields.GUOMEI_APP_URL+accountInfo.getCust_keyword_name()+".html"); 
//			}else if(Platform.equals(Fields.PLATFORM_KAOLA)){
//				dataCrawlTask.setSetUrl(Fields.KAOLA_APP_URL + accountInfo.getCust_keyword_name()+ ".html");
//			}else if(Platform.equals(Fields.PLATFORM_SUNING)){
//				dataCrawlTask.setSetUrl(Fields.SUNING_MOBILE_URL+accountInfo.getCust_keyword_name()+"__20_021_0210101_0_5__999.html?callback=wapData"); 
//			}else if(Platform.equals(Fields.PLATFORM_VIP)){
//				dataCrawlTask.setSetUrl(Fields.VIP_APP_URL + accountInfo.getCust_keyword_name()+ ".html?jump=1");
//			}else if(Platform.equals(Fields.PLATFORM_YHD)){
//				dataCrawlTask.setSetUrl(Fields.YHD_URL_APP+accountInfo.getCust_keyword_name()+".html");  
//			}else if(Platform.equals(Fields.PLATFORM_TAOBAO_EN)){
//				SimpleDateFormat data2=new SimpleDateFormat("HH");
//				String code=whetherShelves.CityAddressCode(data2);//获取城市code抓取库存
//				String url=Fields.TMALL_APP_2+"%7B%22itemNumId%22%3A%22"+accountInfo.getCust_keyword_name()+"%22%7D&qq-pf-to=pcqq.group&areaId="+code;
//				//String url=Fields.TMALL_APP+accountInfo.getCust_keyword_name()+"&ttid=2013@taobao_h5_1.0.0&areaId="+code+" ";
//				dataCrawlTask.setSetUrl(url); 
//				dataCrawlTask.setCode(code);
//			}else if(Platform.equals(Fields.PLATFORM_TMALL_EN)){
//				SimpleDateFormat data2=new SimpleDateFormat("HH");
//				String code=whetherShelves.CityAddressCode(data2);//获取城市code抓取库存
//				String url=Fields.TMALL_APP_2+"%7B%22itemNumId%22%3A%22"+accountInfo.getCust_keyword_name()+"%22%7D&qq-pf-to=pcqq.group&areaId="+code;
//				//String url=Fields.TMALL_APP+accountInfo.getCust_keyword_name()+"&ttid=2013@taobao_h5_1.0.0&areaId="+code+" ";
//				dataCrawlTask.setCode(code);
//				dataCrawlTask.setSetUrl(url); 
//			}else if(Platform.equals(Fields.PLATFORM_MiA_EN) || Platform.equals(Fields.PLATFORM_MiA_GLOBAL_EN)){
//				if(Platform.equals(Fields.PLATFORM_MiA_EN)){
//					dataCrawlTask.setSetUrl(Fields.MIYA_APP_URL + accountInfo.getCust_keyword_name()+ ".html");
//				}else{
//					dataCrawlTask.setSetUrl(Fields.MIYA_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ ".html");
//				}
//			}else if(Platform.equals(Fields.PLATFORM_JD)){
//				String dataCookie=crawlerPublicClassService.dataCookie(user_Id,Platform);//获取cookie
//				dataCrawlTask.setCookie(dataCookie); 
//				if(Validation.isEmpty(dataCookie)){
//					dataCrawlTask.setSetUrl(Fields.JD_URL_APP+accountInfo.getCust_keyword_name()+".html");
//				}else{
//					dataCrawlTask.setSetUrl(Fields.JD_URL_APP_INTERNATIONL+accountInfo.getCust_keyword_name()); 
//				}
//			}else if(Platform.equals(Fields.PLATFORM_JUMEI) || Platform.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//				
//				if(Platform.equals(Fields.PLATFORM_JUMEI)){
//					Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
//					if(m.matches()){
//						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL_2 + accountInfo.getCust_keyword_name()+ "&type=jumei_deal");
//					}else{
//						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL + accountInfo.getCust_keyword_name()+"&type=jumei_mall");
//					}
//				}else if(Platform.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//					Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
//					if(m.matches()){
//						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL_2 + accountInfo.getCust_keyword_name()+ "&type=global_deal"); 
//					}else{
//						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ "&type=global_mall"); 
//					}
//				}
//			}else if(Platform.equals(Fields.PLATFORM_AMAZON)){
//				dataCrawlTask.setSetUrl(Fields.PEPSI_AMAZON_URL+accountInfo.getCust_keyword_name()); 
//			}
//			taskExecutor.execute(dataCrawlTask);
//		}
//		taskExecutor.shutdown();
//		while(true){
//			if(((ExecutorService) taskExecutor).isTerminated()){
//				if(!Platform.equals(Fields.PLATFORM_AMAZON)){
//					grab_failure_commodity_prices(Platform,databases,user_Id,dataSource,data_time,Integer.valueOf(map.get("thread_sum").toString()));	
//				}
//				return gson.toJson(1);
//			}
//		}
//	}
//
//	public void grab_failure_commodity_prices(String Platform,String databases,String user_Id,String dataSource,String data_time,int thread_sum) throws InterruptedException{
//		int ii=0;
//		ExecutorService taskExecutor = Executors.newFixedThreadPool(thread_sum);  //初始化线程池 
//		ApplicationContext context = SpringContextUtil.getApplicationContext();
//		List<CommodityPrices>listPrice=crawlerPublicClassService.RecursionFailureGoods_Price(user_Id,databases,Platform,null,Fields.TABLE_CRAW_KEYWORDS_INF);
//		 long dataTime=System.currentTimeMillis();
//		if(listPrice.size()>0){
//			for(CommodityPrices accountInfo:listPrice){ ii++;	
//			FillCatch_All_CrawlTask dataCrawlTask = (FillCatch_All_CrawlTask) context.getBean(FillCatch_All_CrawlTask.class);
//			dataCrawlTask.setDataType(Platform);
//			dataCrawlTask.setTimeDate(data_time);
//			dataCrawlTask.setSum(""+ii+"/"+listPrice.size()+"");
//			accountInfo.setBatch_time(data_time);
//			accountInfo.setPlatform_name_en(Platform);
//			dataCrawlTask.setCommodityPrices(accountInfo);
//			dataCrawlTask.setType(Fields.STYPE_6);
//			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(dataSource)); 
//			dataCrawlTask.setAccountId(user_Id);
//			taskExecutor.execute(dataCrawlTask);  
//			if(Platform.equals(Fields.GUOMEI)){
//				String egoodsId[]=accountInfo.getEgoodsId().toString().split("-");
//				dataCrawlTask.setSetUrl(Fields.GUOMEI_PC_PRICE+egoodsId[0]+"/"+egoodsId[1]+"/N/21010100/210101001/1/null/flag/item/allStores?callback=allStores&_=1517206275418");
//			}else if(Platform.equals(Fields.PLATFORM_JD)){
//				String dataCookie=crawlerPublicClassService.dataCookie(user_Id,Platform);//获取cookie
//				dataCrawlTask.setCookie(dataCookie);
//			}else if(Platform.equals(Fields.PLATFORM_KAOLA)){
//				dataCrawlTask.setSetUrl(Fields.KAOLA_PC_URL_PRICE + accountInfo.getEgoodsId()+"&categoryId="+accountInfo.getPlatform_category()+"&t="+dataTime);
//			}else if(Platform.equals(Fields.PLATFORM_SUNING)){
//				dataCrawlTask.setSetUrl(Fields.SUNING_PC_URL+accountInfo.getEgoodsId()+"_000000000"+accountInfo.getEgoodsId()+"_0000000000_20_021_0210501_315587_1000267_9264_12117_Z001___R9000361_0.24_0_0010118558.html?callback=pcData&_="+dataTime+"");
//			}else if(Platform.equals(Fields.PLATFORM_VIP)){
//				 dataCrawlTask.setSetUrl(Fields.VIP_PC_URL+accountInfo.getEgoodsId()+ ".html");
//			}else if(Platform.equals(Fields.PLATFORM_YHD)){
//				dataCrawlTask.setSetUrl(Fields.EXTRAPARAM+accountInfo.getEgoodsId()+"&_="+dataTime);
//			}else if(Platform.equals(Fields.PLATFORM_JUMEI) || Platform.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//				 Matcher m=Pattern.compile(regex).matcher(accountInfo.getEgoodsId());  
//				  dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId() + ".html");
//				  if(Platform.equals(Fields.PLATFORM_JUMEI)){
//                      if(m.matches()){
//                      	 dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_2 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&callback=jQuery111207574128594781935_1512112116418&_=1512112116419");	//国内聚美特卖商品
//                      }else{
//                      	 dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_1 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery1112017022424268169556_1512110686059&_=1512110686060");//国内普通商品
//                      }
//                     }else if(Platform.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//                  	   if(m.matches()){
//                  		   dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_4 + accountInfo.getEgoodsId()+ ".html");//海外购极速免税店可以获取价格促销
//                  	   }else {
//                  		   dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_3 + accountInfo.getEgoodsId()+ ".html");//海外购
//                  	   }
//                     }else if(Platform.equals(Fields.PLATFORM_MiA_EN) || Platform.equals(Fields.PLATFORM_MiA_GLOBAL_EN)){
//                    	 dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL+accountInfo.getEgoodsId()+ ".html");
//                    	 if(Platform.equals(Fields.PLATFORM_MiA_EN)){
//                           	 dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId()+ ".html");
//                           }else{
//                           	 dataCrawlTask.setSetUrl(Fields.MIYA_PC_GLOBAL_URL + accountInfo.getEgoodsId()+ ".html");
//                           }
//                     }
//			    }
//			    taskExecutor.execute(dataCrawlTask);
//			}	
//		}
//		taskExecutor.shutdown();
//	} 
//}
