/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月10日 上午11:06:16 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.task.Vip_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：VipData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月10日 上午11:06:16   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月10日 上午11:06:16   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class VipData {
	private static Logger _log = Logger.getLogger(VipData.class);
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();  
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	public void atartCrawlerVip(List<QrtzCrawlerTable>listjobName,String tableName,String counts,int pageTop ){
		try {
			contexts= SpringContextUtil.getApplicationContext();  
			String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
			if(listjobName.size()>0){
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
				crawlerVipPriceMonitoring(time,listjobName,tableName,counts,pageTop);
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					_log.info("开始爬唯品会PC端价格当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
					TerminalCommodityPrices(time,listjobName,tableName,counts,pageTop);
				}
			}else{
				_log.info("当前唯品会用户不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());	
			}	
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}	 
	}

	/**  
	 * @Title: crawlerVipPriceMonitoring  
	 * @Description: TODO 开始解析vip数据
	 * @param @param listjobName
	 * @param @param message    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings({ "rawtypes", "unused" })
	public void crawlerVipPriceMonitoring(String time,List<QrtzCrawlerTable>listjobName,String tableName,String counts,int pageTop){
		int jj = 0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor); 
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,counts,pageTop);//开始解析数据 
		ArrayList<Future> futureList = new ArrayList<Future>();
		for (CrawKeywordsInfo accountInfo : list) { jj++;
		Vip_DataCrawlTask dataCrawlTask = (Vip_DataCrawlTask) contexts.getBean(Vip_DataCrawlTask.class);
		Map<String,Object>vipMap=new HashMap<String,Object>();
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setSetUrl(Fields.VIP_APP_URL.replace("EGOODSID", accountInfo.getCust_keyword_name())+"?jump=1");
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setVipMap(vipMap);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdown();
		while (true) {
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}
		try {
			crawAndParseInfoAndPricefailure(time,listjobName,tableName,counts,pageTop);//检查是否有失败的商品
		} catch (Exception e) {
			_log.info("解析失败信息失败"+e.toString());
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPricefailure(String time,List<QrtzCrawlerTable>listjobName,String tableName,String counts,int pageTop) throws InterruptedException {
		int jj = 0; int count = 0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		ArrayList<Future> futureList = new ArrayList<Future>();
		do {
			List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),tableName,pageTop);//开始解析数据
			for (CrawKeywordsInfo accountInfo : list) {
				jj++;
				Vip_DataCrawlTask dataCrawlTask = (Vip_DataCrawlTask) contexts.getBean(Vip_DataCrawlTask.class);
				Map<String,Object>vipMap=new HashMap<String,Object>();
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
				dataCrawlTask.setCrawKeywordsInfo(accountInfo);
				dataCrawlTask.setSetUrl(Fields.VIP_APP_URL.replace("EGOODSID", accountInfo.getCust_keyword_name())+"?jump=1");
				dataCrawlTask.setType(Fields.STYPE_1);
				dataCrawlTask.setVipMap(vipMap);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				futureList.add(future);
				_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			}
			jj = 0;
			taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}
			count++;
			if (count > 5 ||list!=null&&list.size()==0) {
				Thread.sleep(1000);
				return;
			}
		} while (true);
	}
	/**
	 * 获取PC商品价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void TerminalCommodityPrices(String time,List<QrtzCrawlerTable>listjobName,String tableName,String counts,int pageTop) {
		int ii = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CommodityPrices> listPrice = crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),tableName,pageTop);
		for (CommodityPrices accountInfo : listPrice) {
			ii++;
			Vip_DataCrawlTask dataCrawlTask = (Vip_DataCrawlTask) contexts.getBean(Vip_DataCrawlTask.class);
			Map<String,Object>vipMap=new HashMap<String,Object>();
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + ii + "/" + listPrice.size() + "");
			accountInfo.setBatch_time(time);
			accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
			dataCrawlTask.setCommodityPrices(accountInfo);
			dataCrawlTask.setType(Fields.STYPE_6); 
			dataCrawlTask.setVipMap(vipMap);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setSetUrl(Fields.VIP_PC_URL+accountInfo.getEgoodsId()+ ".html");
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdown();
		while (true) { 
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				break;
			}
		}
		ii = 0;
		try {// 递归补漏
			Thread.sleep(20000);
			vipRecursionFailureGoods(time,listjobName,tableName,pageTop);
		} catch (Exception e) {
			_log.info("递归补漏失败+===========" + e.getMessage() + "---------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
	}

	/**
	 * 递归检查获取没有成功抓取商品的价格
	 *
	 * @throws InterruptedException
	 * @throws ExecutionException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void vipRecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName,String tableName,int pageTop) throws InterruptedException, ExecutionException {
		int ii = 0; int count=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		do {
			ArrayList<Future> futureList = new ArrayList<Future>();
			List<CommodityPrices> listPrice = crawlerPublicClassService.RecursionFailureGoods(listjobName,listjobName.get(0).getDatabases(),tableName,pageTop);
			if (listPrice.size() > 0) {
				for (CommodityPrices accountInfo : listPrice) {
					ii++;
					Vip_DataCrawlTask dataCrawlTask = (Vip_DataCrawlTask) contexts.getBean(Vip_DataCrawlTask.class);
					Map<String,Object>vipMap=new HashMap<String,Object>();
					dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
					dataCrawlTask.setTimeDate(time);
					dataCrawlTask.setSum("" + ii + "/" + listPrice.size() + "");
					accountInfo.setBatch_time(time);
					dataCrawlTask.setVipMap(vipMap);
					dataCrawlTask.setIp(listjobName.get(0).getIP());
					accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
					dataCrawlTask.setCommodityPrices(accountInfo);
					dataCrawlTask.setType(Fields.STYPE_6);
					dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
					dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
					dataCrawlTask.setSetUrl(Fields.VIP_APP_URL + accountInfo.getEgoodsId() + ".html");
					dataCrawlTask.setStorage(listjobName.get(0).getStorage());
					dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
					Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
					futureList.add(future);
					_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				}
			}
			taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
						crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					}
					break;
				}
			}
			ii = 0;
			count++;
			if (count > 5 ||listPrice!=null&&listPrice.size()==0) {
				_log.info("递归补漏 第"+count+"次" + SimpleDate.SimpleDateFormatData().format(new Date()));
				Thread.sleep(5000); 
				return;
			}
		} while (true);	
	}
}
