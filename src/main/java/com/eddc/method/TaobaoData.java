/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月12日 上午11:32:58 
 */
package com.eddc.method;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.task.Taobao_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：selection_pepsi_crawler   
 * 类名称：TaobaoData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月12日 上午11:32:58   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月12日 上午11:32:58   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class TaobaoData {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private ApplicationContext contexts = SpringContextUtil.getApplicationContext(); 
	private static Logger _log = Logger.getLogger(TaobaoData.class);

	//开始爬虫
	public void crawlerTaobaoPriceMonitoring(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop) {
		_log.info(">>> Invoke Taobao_Job crawlerTaobaoPriceMonitoring method >>>");
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		List<Craw_keywords_delivery_place> List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName, listjobName.get(0).getDatabases());
		try {
			if (List_Code.size() > 0) {
				for (Craw_keywords_delivery_place delivery : List_Code) {
					crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getDatabases(), listjobName.get(0).getUser_Id(), tableName);//同步数据
					taobao_craw_item(listjobName, time, delivery.getDelivery_place_code(), tableName, count, pageTop); //开始抓取数据
				}
			} else {
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getDatabases(), listjobName.get(0).getUser_Id(), tableName);//同步数据
				taobao_craw_item(listjobName, time, Fields.CODE, tableName, count, pageTop); //开始抓取数据
			}

		} catch (Exception e) {
			_log.error(">>> Taobao_Job crawlerTaobaoPriceMonitoring fail,please check ! >>>");
			_log.error(e.getMessage());
		}
		_log.info(">>> Finish Taobao_Job crawlerTaobaoPriceMonitoring method >>>");
	}

	//开始爬数据
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void taobao_craw_item(List<QrtzCrawlerTable> listjobName, String time, String code, String tableName, String count, int pageTop) throws InterruptedException {
		_log.info(">>> Invoke Taobao_Job taobao_craw_item method >>>");
		int jj = 0;
		final long awaitTime = Integer.valueOf(listjobName.get(0).getDocker()) * 1000 * 60;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, pageTop);//开始解析数据
		for (CrawKeywordsInfo accountInfo : list) {
			jj++;
			//String url=Fields.TMALL_APP_3.replace("EGOODSID",accountInfo.getCust_keyword_name()+"&areaId="+code);
			String url=Fields.TMALL_APP_2.replace("EGOODSID",accountInfo.getCust_keyword_name()).replace("CODE", code);
			Taobao_DataCrawlTask dataCrawlTask = (Taobao_DataCrawlTask) SpringContextUtil.getApplicationContext().getBean(Taobao_DataCrawlTask.class);
			Map<String, Object> TaobaoMap = new HashMap<String, Object>();
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTaobaoMap(TaobaoMap);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setAccountinfo(accountInfo);
			dataCrawlTask.setSetUrl(url);
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setCode(code);
			dataCrawlTask.setPageTop(pageTop);
			dataCrawlTask.setClient(listjobName.get(0).getClient());
			dataCrawlTask.setPage(Integer.valueOf(Fields.STATUS_ON));
			dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			Future<Map<String, Object>> future = completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
			futureList.add(future);
			_log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
		}
		try {
			//taskExecutor.shutdown();
			_log.info(">>> Taobao_Job Future List prepare finished >>>");
			if (pageTop == Fields.STATUS_COUNT) {
				_log.info(">>> START COUNT = 0 >>>");
				while (true) {
					if (((ExecutorService) taskExecutor).isTerminated()) {
						_log.info(">>> TaskExecutor has finished >>>");
						taskExecutor.shutdownNow();
						_log.info(">>> TaskExecutor has shutdown >>>");

						_log.info(">>> Start batch insert crawler data >>>");
						// 超时的时候向线程池中所有的线程发出中断(interrupted)。
						if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
						}
						_log.info(">>> Finish batch insert crawler data >>>");

						_log.info(">>> Start check failure crawler info >>>");
						crawAndParseInfoAndPricefailure(time, code, listjobName, tableName, pageTop);//检查是否有失败的商品
						_log.info(">>> Finish check failure crawler info >>>");
					}
					break;
				}
			} else {
				if (!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)) {
					_log.info(">>> TaskExecutor timeout,shut down now ! >>>");
					taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。
				}else{
					taskExecutor.shutdownNow();  
				}
			}
		} catch (Exception e) {
			// 超时的时候向线程池中所有的线程发出中断(interrupted)。
			taskExecutor.shutdownNow();
			_log.error("taobao_craw_item fail,please check!");
			_log.error(e.getMessage());

		} finally {
			if (pageTop == Fields.STATUS_COUNT_1) {
				_log.info(">>> START COUNT = 1 >>>");
				_log.info(">>> Start batch insert crawler data >>>");
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				_log.info(">>> Finish batch insert crawler data >>>");

				_log.info(">>> Invoke updateCrawKeywordHistory >>>");
				crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
				_log.info(">>> Finish updateCrawKeywordHistory >>>");

				Thread.sleep(5000);
				_log.info(">>> Finish SynchronousData >>>");
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getDatabases(), listjobName.get(0).getUser_Id(), tableName);//同步数据
				_log.info(">>> Finish SynchronousData >>>");
			}
		}
		_log.info(">>> Finish invoke Taobao_Job taobao_craw_item method >>>");
	}

	///检查失败的item数据
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String time, String code, List<QrtzCrawlerTable> listjobName, String tableNname, int pageTop) throws InterruptedException {
		int jj = 0;
		int count = 0;
		List<CrawKeywordsInfo> list = new ArrayList<CrawKeywordsInfo>();
		ArrayList<Future> futureList = new ArrayList<Future>();
		do {
			list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName, listjobName.get(0).getDatabases(), tableNname, pageTop);//开始解析数据
			ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池
			for (CrawKeywordsInfo accountInfo : list) {
				jj++;
				String url = Fields.TMALL_APP_2 + "%7B%22itemNumId%22%3A%22" + accountInfo.getCust_keyword_name() + "%22%7D&qq-pf-to=pcqq.group&areaId=" + code;
				Taobao_DataCrawlTask dataCrawlTask = (Taobao_DataCrawlTask) contexts.getBean(Taobao_DataCrawlTask.class);
				Map<String, Object> TaobaoMap = new HashMap<String, Object>();
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setTaobaoMap(TaobaoMap);
				dataCrawlTask.setPageTop(pageTop);
				dataCrawlTask.setDataType(accountInfo.getPlatform_name());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
				dataCrawlTask.setAccountinfo(accountInfo);
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setSetUrl(url);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setType(Fields.STYPE_1);
				dataCrawlTask.setCode(code);
				dataCrawlTask.setPage(Integer.valueOf(Fields.STATUS_ON));
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
				dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
				Future<Map<String, Object>> future = taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
				futureList.add(future);
				_log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
			}
			_log.info(">>> Failure future List prepare finished >>>");
			while (true) {
				if (((ExecutorService) taskExecutor).isTerminated()) {
					taskExecutor.shutdownNow();
					_log.info(">>> Start batch insert failure crawler data >>>");
					crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
					_log.info(">>> Finish batch insert failure crawler data >>>");
					break;
				}
			}
			jj = 0;
			count++;
			Thread.sleep(5000);
			if (count > 4) {
				_log.info(">>> retry invoke Taobao_Job crawAndParseInfoAndPricefailure method : " + count + " >>>");
				return;
			}
		} while (true);
	}
}
