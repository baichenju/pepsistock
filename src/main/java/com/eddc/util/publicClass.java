package com.eddc.util;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
public class publicClass { 
	/** 
	 * 获取当前网络ip 
	 * @param request 
	 * @return 
	 */  
	public static String getIpAddr(HttpServletRequest request){  
		String ipAddress = request.getHeader("x-forwarded-for");  
		if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
			ipAddress = request.getHeader("Proxy-Client-IP");  
		}  
		if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
			ipAddress = request.getHeader("WL-Proxy-Client-IP");  
		}  
		if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
			ipAddress = request.getRemoteAddr();  
			if(ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")){  
				//根据网卡取本机配置的IP  
				InetAddress inet=null;  
				try {  
					inet = InetAddress.getLocalHost();  
				} catch (UnknownHostException e) {  
					e.printStackTrace();  
				}  
				ipAddress= inet.getHostAddress();  
			}  
		}  
		//对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割  
		if(ipAddress!=null && ipAddress.length()>15){ //"***.***.***.***".length() = 15  
			if(ipAddress.indexOf(",")>0){  
				ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));  
			}  
		}  
		return ipAddress;   
	}
	
	public static String Ipaddres(){
		InetAddress inet=null;  
		try {  
			inet = InetAddress.getLocalHost();  
		} catch (UnknownHostException e) {  
			e.printStackTrace();  
		}  
		String ipAddress= inet.getHostAddress();  
		return ipAddress;
	}
	
	public static String getLocalIP() {
		String IPString="";
		try {
			if (isWindowsOS()) {
				IPString= InetAddress.getLocalHost().getHostAddress();
			} else {
				IPString= getLinuxLocalIp();
			}
		} catch (Exception e) {
			IPString="127.0.0.1";
		}
		return IPString;

	}
	
	/**
     * 判断操作系统是否是Windows
     *
     * @return
     */
    public static boolean isWindowsOS() {
        boolean isWindowsOS = false;
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().indexOf("windows") > -1) {
            isWindowsOS = true;
        }
        return isWindowsOS;
    }
    /**
     * 获取Linux下的IP地址
     *
     * @return IP地址
     * @throws SocketException
     */
    private static String getLinuxLocalIp() throws SocketException {
        String ip = "";
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                String name = intf.getName();
                if (!name.contains("docker") && !name.contains("lo")) {
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            String ipaddress = inetAddress.getHostAddress().toString();
                            if (!ipaddress.contains("::") && !ipaddress.contains("0:0:") && !ipaddress.contains("fe80")) {
                                ip = ipaddress;
                                System.out.println(ipaddress);
                            }
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            System.out.println("获取ip地址异常");
            ip = "127.0.0.1";
            ex.printStackTrace();
        }
        System.out.println("IP:"+ip);
        return ip;
    }
	 public static Map<String,String>jumeiMap (){
	    	Map<String,String>jumeiMap=new HashMap<String, String>();
	    	jumeiMap.put("cookie","");
	    	jumeiMap.put("coding", "UTF-8");
	    	jumeiMap.put("urlRef", "item.jumei.com");
	    	jumeiMap.put("host", "item.jumei.com");
	    	jumeiMap.put("platform", Fields.PLATFORM_JUMEI);
	    	jumeiMap.put("ip","-");
	    	return jumeiMap;
	    }
		
	    public static Map<String,String>KaolaMap (){
	    	Map<String,String>Map=new HashMap<String, String>();
	    	Map.put("coding", "UTF-8");
	    	Map.put("urlRef", null);
	    	Map.put("cookie",null);
	    	Map.put("ip","-");
	    	Map.put("platform", Fields.PLATFORM_KAOLA);
	    	return Map;
	    }
	     
	    public static Map<String,String>lazadaMap (){
	    	Map<String,String>Map=new HashMap<String, String>();
	    	Map.put("cookie","");
	    	Map.put("coding", "UTF-8");
	    	Map.put("urlRef", "www.lazada.com.my");
	    	Map.put("platform", Fields.PLATFORM_LAZADA);
	    	Map.put("ip","-");
	    	return Map;
	    }
	    public static Map<String,String>sillaMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","JSESSIONID=jtlZ8ll0i37iq2JRa0O8myA27rOUsULZaEWYiPjKj30WkARp3Nwv!-1089477281; ");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", "www.shilladfs.com");
	    	map.put("platform", Fields.PLATFORM_SILLA);
	    	map.put("ip","-");
	    	return map;
	    }
	    public static Map<String,String>suningMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","cityId=9264; districtId=12113; SN_CITY=20_021_1000267_9264_01_12113_3_0; _snstyxuid=654260626164CBLF; _snsr=baidu%7Cbrand%7C%7Clogo%7C%25E8%258B%258F%25E5%25AE%2581*%3A*; authId=si590FAA6D8F49F4D1F97F6001806593BA; secureToken=8BC76544D7102B852B6C28DC2DA84DF9; _snms=150270379321633744; smhst=101923925|0000000000a101923918|0000000000a101822315|0000000000a625019371|0000000000a102668611|0000000000; _snma=1%7C150086118535495580%7C1500861185354%7C1502703813564%7C1502703822326%7C31%7C3; _snmc=1; _snmp=15027038222755593; _snmb=150270377280922319%7C1502703822438%7C1502703822330%7C8; _ga=GA1.2.721456464.1500861186; _gid=GA1.2.1016444502.1502703773");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", null);
	    	map.put("platform", Fields.PLATFORM_SUNING);
	    	map.put("ip","-");
	    	return map;
	    }
	    public static Map<String,String>tmallMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","cna=J9yBEjUYJ18CAYzOQzqzf2j1; hng=CN%7Czh-CN%7CCNY%7C156; thw=cn; miid=5366869721331323302; v=0; alitrackid=www.taobao.com; lastalitrackid=www.taobao.com; swfstore=142925; _tb_token_=30cc7b7958716; tk_trace=oTRxOWSBNwn9dPy4KVJVbutfzK5InlkjwbWpxHegXyGxPdWTLVRjn23RuZzZtB1ZgD6Khe0jl%2BAoo68rryovRBE2Yp933GccTPwH%2FTbWVnqEfudSt0ozZPG%2BkA1iKeVv2L5C1tkul3c1pEAfoOzBoBsNsJySRN5g7VG9h73waITUSDg5joDrKsiaccr%2B2B4QSpDs7e0NIAIKxBSdPu6Y6mEZrAaSW%2BAfEQ7Qd91oAa6mAMrWHWje%2BUfOxJQm8Oq1ox7SONNnVyHlQVcUCMyrpBvuags6gEofeGH3WZnnXNm4HjVgsp4Epd0mR%2BgRPkw%2FlNgu5DBvhfFSRMPOh1Z0XA%3D%3D; linezing_session=o7Hi416XJYuLSRZ7QZBFjwfP_1510712427612IkRq_8; uc3=sg2=UojQbRVHa5BKhStiSwLPQaAKPo%2B9DwJIRTX6k%2BWWdG8%3D&nk2=tsV8GJVVWb0%3D&id2=WvA07t216W%2BH&vt3=F8dBzLOSwUDJBK33wuQ%3D&lg2=UIHiLt3xD8xYTw%3D%3D; existShop=MTUxMDcxMjYwOQ%3D%3D; lgc=%5Cu8D75%5Cu65ED%5Cu4E1Czx; tracknick=%5Cu8D75%5Cu65ED%5Cu4E1Czx; cookie2=13903a637cf38429da6dab62a845e2ce; mt=np=&ci=-1_1; skt=d412064b8fc560a4; t=43b3fcd9469aab9535b7e1b70d7d4c25; _cc_=Vq8l%2BKCLiw%3D%3D; tg=0; whl=-1%260%260%261510717789907; JSESSIONID=A535233374D726F1B9FCD2C9BE6C2E5E; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0%26__ll%3D-1%26_ato%3D0; uc1=cart_m=0&cookie14=UoTde9HYNnKeww%3D%3D&lng=zh_CN&cookie16=WqG3DMC9UpAPBHGz5QBErFxlCA%3D%3D&existShop=false&cookie21=UtASsssmfaCONGki4KTH3w%3D%3D&tag=8&cookie15=U%2BGCWk%2F75gdr5Q%3D%3D&pas=0; isg=AiMjFrNCfQEMiTLEq5OZmScSsmcNsLSxParXsFWAEQL5lEO23ehHqgHO-FJh");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", "acs.m.taobao.com");
	    	map.put("platform", Fields.PLATFORM_TMALL_EN);
	    	map.put("ip","-");
	    	return map;
	    }
	    
	    public static Map<String,String>vipMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie",null);
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", null);
	    	map.put("platform", Fields.PLATFORM_VIP);
	    	map.put("ip","-");
	    	return map;
	    }
	    
	    public static Map<String,String>yhdMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508134896.1508137618.4; __jdb=81617359.1.15081208192201111174638|4.1508137618; __jdc=81617359");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", "www.yhd.com");
	    	map.put("platform", Fields.PLATFORM_YHD);
	    	map.put("ip","-");
	    	return map;
	    }
	    
	    public static Map<String,String>jdMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","JAMCookie=true;");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", null);
	    	map.put("host", "item.m.jd.com");
	    	map.put("platform", Fields.PLATFORM_JD);
	    	map.put("ip","-");
	    	return map;
	    }
	    public static Map<String,String>zaraMap (){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie",null);
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", null);
	    	map.put("host", "api.empathybroker.com");
	    	map.put("platform", Fields.ZARA);
	    	map.put("ip","-");
	    	return map;
	    }
	    public static Map<String,String>guomeiMap(){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("cookie","__clickidc=13335043916778900; __c_visitor=13335043916778900; atgregion=21010100%7C%E4%B8%8A%E6%B5%B7%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%84%E6%B5%A6%E5%8C%BA%E5%8D%8A%E6%B7%9E%E5%9B%AD%E8%B7%AF%E8%A1%97%E9%81%93%7C21010000%7C21000000%7C210101001; cartnum=0_0-1_0; _idusin=74848300764; isnew=638753565043.1516783413552; s_ev13=%5B%5B'sem_baidu_pinpai_logo'%2C'1517191505812'%5D%5D; asid=eb3dbf0ef372aa5464e31031d2dfede8; awaken=true; itemAdvertisingN=true; ufpd=6014f0f42484e911a6e9c449dc3daa04384977ac91f81785abfbfd84c9583a864368a01936a3160cd33fb312e7a09799fc9e207a6df52907fc9f5b4e569be1f3; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; compare=; proid120517atg=%5B%229140046570-1130051649%22%2C%229140046569-1130051643%22%2C%22A0006333570-pop8010627081%22%2C%229140046571-1130051656%22%2C%229140055865-1130523959%22%5D; s_cc=true; s_getNewRepeat=1517206275510-Repeat; s_sq=%5B%5BB%5D%5D; _smt_uid=5a683599.403fce14; _jzqco=%7C%7C%7C%7C%7C1.1631152474.1516778905172.1517199479642.1517206279671.1517199479642.1517206279671.0.0.0.22.22; __xsptplus194=194.7.1517206279.1517206279.1%232%7Cbzclk.baidu.com%7C%7C%7C%25E5%259B%25BD%25E7%25BE%258E%25E5%259C%25A8%25E7%25BA%25BF%7C%23%23Z3wzhyMBUeJsq35GotUN1Qk2YZUWYun0%23; uid=CjoWyFpu08CvfzNVD/QFAg==; DSESSIONID=5e155fb51705421dbdc086ca5367f282; s_ppv=-%2C35%2C15%2C2353; __gmv=1126275969063.1516778901361; __gma=ffb8de7.1126275969063.1516778901361.1517209037963.1517213662735.10; __gmb=ffb8de7.1.1126275969063|10.1517213662735; __gmz=ffb8de7|-|-|direct|-|-|-|1126275969063.1516778901361|dc-2|1517213662735; __gmc=ffb8de7; plasttime=1517213663; gm_sid=mfxgnn6gxnw3hyieyk7qc895g5n52g1nvy515172136623");
	    	map.put("coding", "UTF-8");
	    	map.put("urlRef", "https://item.m.gome.com.cn/product-9140046570-1130051648.html");
	    	map.put("platform", Fields.GUOMEI);
	    	map.put("ip","-");
	    	return map;
	    }
	    
	    public static Map<String,String>parameterMap(String setUrl,String accountId,String dataType,String timeDate,String IPPROXY){
	    	Map<String,String>map=new HashMap<String, String>();
	    	map.put("ip","-");
	    	map.put("url", setUrl);
	    	map.put("accountId", accountId);
	    	map.put("platform", dataType);
	    	map.put("timeDate", timeDate);
	    	map.put("platform_name", dataType); 
	    	map.put(Fields.IPPROXY, IPPROXY);
	    	map.put("goodsUrl", setUrl); 
	    	map.put("coding", "UTF-8");
	    	if(Fields.GUOMEI.equals(dataType)){
	    		map.put("cookie","__clickidc=13335043916778900; __c_visitor=13335043916778900; atgregion=21010100%7C%E4%B8%8A%E6%B5%B7%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%84%E6%B5%A6%E5%8C%BA%E5%8D%8A%E6%B7%9E%E5%9B%AD%E8%B7%AF%E8%A1%97%E9%81%93%7C21010000%7C21000000%7C210101001; cartnum=0_0-1_0; _idusin=74848300764; isnew=638753565043.1516783413552; s_ev13=%5B%5B'sem_baidu_pinpai_logo'%2C'1517191505812'%5D%5D; asid=eb3dbf0ef372aa5464e31031d2dfede8; awaken=true; itemAdvertisingN=true; ufpd=6014f0f42484e911a6e9c449dc3daa04384977ac91f81785abfbfd84c9583a864368a01936a3160cd33fb312e7a09799fc9e207a6df52907fc9f5b4e569be1f3; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; compare=; proid120517atg=%5B%229140046570-1130051649%22%2C%229140046569-1130051643%22%2C%22A0006333570-pop8010627081%22%2C%229140046571-1130051656%22%2C%229140055865-1130523959%22%5D; s_cc=true; s_getNewRepeat=1517206275510-Repeat; s_sq=%5B%5BB%5D%5D; _smt_uid=5a683599.403fce14; _jzqco=%7C%7C%7C%7C%7C1.1631152474.1516778905172.1517199479642.1517206279671.1517199479642.1517206279671.0.0.0.22.22; __xsptplus194=194.7.1517206279.1517206279.1%232%7Cbzclk.baidu.com%7C%7C%7C%25E5%259B%25BD%25E7%25BE%258E%25E5%259C%25A8%25E7%25BA%25BF%7C%23%23Z3wzhyMBUeJsq35GotUN1Qk2YZUWYun0%23; uid=CjoWyFpu08CvfzNVD/QFAg==; DSESSIONID=5e155fb51705421dbdc086ca5367f282; s_ppv=-%2C35%2C15%2C2353; __gmv=1126275969063.1516778901361; __gma=ffb8de7.1126275969063.1516778901361.1517209037963.1517213662735.10; __gmb=ffb8de7.1.1126275969063|10.1517213662735; __gmz=ffb8de7|-|-|direct|-|-|-|1126275969063.1516778901361|dc-2|1517213662735; __gmc=ffb8de7; plasttime=1517213663; gm_sid=mfxgnn6gxnw3hyieyk7qc895g5n52g1nvy515172136623");
	    		map.put("urlRef", "https://item.m.gome.com.cn/product-9140046570-1130051648.html");
	    	}else if(Fields.PLATFORM_YHD.equals(dataType)){
	    		map.put("cookie","__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508134896.1508137618.4; __jdb=81617359.1.15081208192201111174638|4.1508137618; __jdc=81617359");
	    		map.put("urlRef",null);
	    	}else if(Fields.PLATFORM_SUNING.equals(dataType)){
	    		map.put("cookie","cityId=9264; districtId=12113; SN_CITY=20_021_1000267_9264_01_12113_3_0; _snstyxuid=654260626164CBLF; _snsr=baidu%7Cbrand%7C%7Clogo%7C%25E8%258B%258F%25E5%25AE%2581*%3A*; authId=si590FAA6D8F49F4D1F97F6001806593BA; secureToken=8BC76544D7102B852B6C28DC2DA84DF9; _snms=150270379321633744; smhst=101923925|0000000000a101923918|0000000000a101822315|0000000000a625019371|0000000000a102668611|0000000000; _snma=1%7C150086118535495580%7C1500861185354%7C1502703813564%7C1502703822326%7C31%7C3; _snmc=1; _snmp=15027038222755593; _snmb=150270377280922319%7C1502703822438%7C1502703822330%7C8; _ga=GA1.2.721456464.1500861186; _gid=GA1.2.1016444502.1502703773");
	    		map.put("urlRef",null);
	    	}else if(Fields.PLATFORM_TMALL_EN.equals(dataType)){
	    		map.put("cookie","cookie2=1f316644c3a1a26b3d1064ae2008ef34; t=5781ecfa22aa1473a8e73ed72a4797cd; _tb_token_=b8bee76e173b; _m_h5_tk=b2d1751f2115335ca317f256e00c024f_1570598906206; _m_h5_tk_enc=35b3816c3210d78954e02d24bfe8feda; enc=5DN8n23GcMwHs3VhP5iZlAHcUb%2Ffd4rnpXfVoABdov726rdckTd1t0VwFL3YcUap5Sj3kXkUa2GhYduMnJgxtA%3D%3D; thw=cn; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0; whl=-1%260%260%260; mt=ci=0_0; cna=YzckFrJ0/0ICAYzOQzpsGili; v=0; l=cBMTdQ1gq4yIEuDzBOfZ-urza77tuIOf1oVzaNbMiICP_u5eAtrhWZBBUATwCnGALsMMR3yE78SHByYE8y4emU1n_MkpikEC.; isg=BHp6kQLa82jy_H8E_YV423TDy6CcK_4FU8s3JYRzJo3YdxqxbLtOFUCFwwGrfHad");
		    	map.put("urlRef", "acs.m.taobao.com");
	    	}else if(Fields.PLATFORM_TAOBAO_EN.equals(dataType)){
	    		map.put("cookie","cookie2=1f316644c3a1a26b3d1064ae2008ef34; t=5781ecfa22aa1473a8e73ed72a4797cd; _tb_token_=b8bee76e173b; _m_h5_tk=b2d1751f2115335ca317f256e00c024f_1570598906206; _m_h5_tk_enc=35b3816c3210d78954e02d24bfe8feda; enc=5DN8n23GcMwHs3VhP5iZlAHcUb%2Ffd4rnpXfVoABdov726rdckTd1t0VwFL3YcUap5Sj3kXkUa2GhYduMnJgxtA%3D%3D; thw=cn; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0; whl=-1%260%260%260; mt=ci=0_0; cna=YzckFrJ0/0ICAYzOQzpsGili; v=0; l=cBMTdQ1gq4yIEuDzBOfZ-urza77tuIOf1oVzaNbMiICP_u5eAtrhWZBBUATwCnGALsMMR3yE78SHByYE8y4emU1n_MkpikEC.; isg=BHp6kQLa82jy_H8E_YV423TDy6CcK_4FU8s3JYRzJo3YdxqxbLtOFUCFwwGrfHad");
		    	map.put("urlRef", "acs.m.taobao.com");
	    	}else if(Fields.PLATFORM_JD.equals(dataType)){
	    		map.put("cookie","shshshfpa=302dc358-c830-2c7f-b7bb-b8ba3bb417f7-1603179150; __jdu=16031635450441281671470; areaId=2; ipLoc-djd=2-2841-51980-0; shshshfpb=daMalnW82h60jM%20wdtGDr2Q%3D%3D; commonAddress=0; regionAddress=2%2C78%2C51978%2C0; unpl=V2_ZzNtbUJfRRYmARNSfU5ZVmIHFl0RUkVGcgoRAyhKDlJlBBReclRCFnQURlVnGl0UZwQZWEZcRhVFCEVkexhdBWMBFF5EUXMlRQtGZHopXAJmARJaQFNGEXQNQ1R4EFwCYQMXW0NncxJ1AXZkM10DUDlcRF5DV0sldQhCVH8ZbARXAxpYQ15EEncOQWQwd11IZwQTX0JQQRFwDEdRfhlfDGcEFF1HUUIldDhF; __jdv=76161171|kong|t_1001537277_|tuiguang|1972b8d77f4b4551b47b63ffbbcf3672|1603183541417; PCSYCityID=undefined_undefined_undefined_undefined; wxa_level=1; retina=0; cid=9; jxsid=16031852953174596200; webp=1; mba_muid=16031635450441281671470; visitkey=17187629239191604; autoOpenApp_downCloseDate_auto=1603185296780_21600000; 3AB9D23F7A4B3C9B=DHR7KQ256S7HZIJLC2ZUD5FWXAD2FEE2JUD4Q33KYOZZYFQQKFSIWXS2NVQMVUJVIJYINZ6U6WQ7WJ3DNQIFTU222U; sc_width=1920; __jdc=122270672; __jda=122270672.16031635450441281671470.1603163545.1603183541.1603189681.5; warehistory=\"885162,19037330024,12943831131,13354613071,\"; jxsid_s_u=https%3A//item.m.jd.com/product/885162.html; shshshfp=3d7c11cd67f3418c3b9c95d7c33c65e2; sk_history=885162%2C19037330024%2C; wq_addr=0%7C2_78_51978_0%7C%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_%7C%7C; jdAddrName=%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_; jdAddrId=2_78_51978_0; mitemAddrName=; mitemAddrId=2_78_51978_0; wq_logid=1603189750.1564841616; wqmnx1=MDEyNjM3NHNtY2Q1bTg5TS9pTjs7QWI3SGlrbzAxYS4zMlVCMlJJKikl; __jdb=122270672.3.16031635450441281671470|5.1603189681; mba_sid=16031897172306889263506457162.2; __jd_ref_cls=MDownLoadFloat_FloatShield; __wga=1603189751796.1603189717945.1603185296212.1603185296212.2.2; PPRD_P=UUID.16031635450441281671470-LOGID.1603189751833.1168568727; jxsid_s_t=1603189751897; shshshsID=3ae5fdf31d3e00f4746c97dc777a820a_3_1603189753069");   
	    		map.put("urlRef", null);
	    	}else if(Fields.PLATFORM_KAOLA.equals(dataType)){
	    		map.put("urlRef", null);
	    		map.put("cookie",null);
	    	}else if(Fields.PLATFORM_VIP.equals(dataType)){
	    		map.put("urlRef", null);
	    		map.put("cookie",null);
	    	}else if(Fields.PLATFORM_SILLA.equals(dataType)){
	    		map.put("cookie","JSESSIONID=jtlZ8ll0i37iq2JRa0O8myA27rOUsULZaEWYiPjKj30WkARp3Nwv!-1089477281; ");
	    		map.put("urlRef", null);
	    	}else  if(Fields.PLATFORM_JUMEI.equals(dataType)){
	    		map.put("urlRef", "item.jumei.com");
	    		map.put("cookie",null);
	    	}else  if(Fields.PLATFORM_MiA_EN.equals(dataType)){
	    		map.put("cookie",null);
	    		map.put("urlRef",null);
	    	}else  if(Fields.PLATFORM_LAZADA.equals(dataType)){
	    		map.put("cookie",null);
	    		map.put("urlRef",null);
	    	}else if(Fields.ZARA.equals(dataType)){
	    		map.put("cookie",null);
	    		map.put("urlRef",null);
	    	}else if(Fields.PLATFORM_AMAZON.equals(dataType)){
	    		map.put("urlRef",null);
	    		map.put("cookie", "x-wl-uid=1jhcryLY/bCwOcs5fK3QjULzEOUx6PcQsKUyAuOACFmOgePdlDg5XxJ9S/hqXxNEhQfxwzucKwg0=; csm-hit=s-8A3QM76PK12E68GSR80T|1521447069434; session-id=461-2660211-8068722; session-id-time=2082787201l; ubid-acbcn=460-7062327-5435929; session-token='EcIVWR555Oj4B24JrY5+wmf+TJzecF/FdlpcU2nvxuesUqqrZJjXbNzO9znGgsNByfhYTHyVGXWm9Lcit3ZWxC7Ue0fEdayI8P0Vo5JrbJdyrQVU2RrIBPxlseupR3M9qlH7iu4ovdhoxjSnXGGpmGa/j3poQWWLJvgOaXXj8mXq24j4/8OaqyXh2wEIWXKdbfVoxi3UA3S3UBRiQxWFlG6PZngA2pHFrJfGllVi/7QOxatUcKxx2g=='");
	    	}else if(Fields.PLATFORM_AMAZON_UN.equals(dataType)){
	    		map.put("urlRef",null);
	    		map.put("cookie", null);
	    	}else if(Fields.platform_1688.equals(dataType)){
	    		  map.put("coding", "GBK");
	    		  map.put("cookie",null);
	    		 // map.put("cookie","JSESSIONID=Ix7Z1QZ-4aeZSrNljFfPtRmVI7-kIwaxqQ-bC8; t=92a9269fa7c851e6faffd59634b64dc4; _tb_token_=ee3465e3e85be; cna=SI1xEwAXFkUCAcuc19M2MehE; ali_ab=140.206.67.58.1525325651306.7; UM_distinctid=163247eb99b39e-0f9d340aa08399-1373565-100200-163247eb99c2c4; _csrf_token=1525327015360; lid=%E8%B5%B5%E6%97%AD%E4%B8%9Czx; ali_apache_tracktmp=c_w_signed=Y; LoginUmid=CBQFUPP41RnBgSFsixDaG8X6%2FB9gQet%2B6rFnlcnxnN%2FElUbKolxKOQ%3D%3D; userID=qPW6JmBcTrrsTuvZDFBFAS%2Bm6PbsAcjcwlRS%2Fx6ke0c6sOlEpJKl9g%3D%3D; userIDNum=HlUKJc7MGasDBPJBu4sTUQ%3D%3D; last_mid=b2b-900013774f1f5f; __last_loginid__=%E8%B5%B5%E6%97%AD%E4%B8%9Czx; _cn_slid_=APCQGJZFAs; ali_beacon_id=203.156.215.211.1525332392215.615209.0; _alizs_area_info_=1001; _alizs_user_type_=purchaser; _alizs_cate_info_=7%252C728; ali_apache_id=10.176.103.152.1525338991383.275029.3; ctoken=KMcOWu1fYKSI88w7Y5Oczealot; cn_m_s.sig=7MLuIxRTCVx2weRF5q3W8pqK7b9OWypw285zHorJsak; ali_apache_track.sig=d4VU5iUXojh4EUuSkgUvEn5iZdM7T7Os4M13Hhmp-dc; cookie2.sig=cXKLa6rxY1waEwRuzICd8id1_eKG-zSL-5w4mpafCDQ; __cn_logon__.sig=U1K8PEk3J_sjpFmup-PbQXymGVFBuEtz5lgzYQNXiQM; tbsnid.sig=TpoE9MI_ply652-dGFaFaGk08DMdHeDbMW7uEOizgYM; __cn_logon_id__.sig=ZbmgFi8lH6_zvGkFGQCM-dWmEQ2uRJhrk12iPkeoFf4; cn_tmp.sig=BCX4BGAznI37yL8ofw8fS1YZqluLfMwkvqNtsTpmLgg; h_keys='%u667a%u80fd%u624b%u73af#%u667a%u80fd%u624b%u673a#KEYWORD#%u79fb%u52a8%u7535%u6e90#%u97f3%u7bb1'; ad_prefer='2018/05/03 22:07:38'; _tmp_ck_0='HYP1z9lZIkhm7qevFkKi2I%2BdZNCGoq0DvtZE8q2T7MtCU2gtDZ11BIGRSxGT9%2B2CGmdyGwES%2F5ypxb8RJ7sdXb%2BckHgxc9c9R40Uv%2BoBisYh8V4HlhLy%2FVc4DKB2uRciCT2JeXf5I5PfhC2V9yFStNu8%2BR%2Bnmr%2FO63xOvMxR6Bj9ULTiXcSQyf%2F0bafRsdbBoIrBLOPWdmM%2FAWJ37Wux60HeMuAOOvH41DCDcIJ02uBkUJ3rwZZ4O%2BeRtww8IkqMtsjR%2FfsValWgIBSWxvxzJ47ww%2BdbmATLMJGdJ2UUzbjROLk87GJwOdAuAaHQDA4M8jJ4T8SOw5q0mjYGFJhHxgisux0hbBWercoz5doAelsfGK2%2B1ijLoAdhTvd3rY5zPwrOKGN67TU%2Fk0Gk25jLurBQsFclcSb%2BTuuZBMs61F5Rd4TEFERKeJGjWS%2F7PMMSUdHSmk%2BMmQkxvZbvZ%2BzinsxxBBfVPLCccrDjmieja5ydeFlQVxES9g%3D%3D'; webp=1; __cn_logon__=false; ali-ss=eyJtZW1iZXJJZCI6bnVsbCwidXNlcklkIjpudWxsLCJsb2dpbklkIjpudWxsLCJzaWQiOm51bGwsImVjb2RlIjpudWxsLCJsb2dpblN0YXR1c1JldE1zZyI6bnVsbCwibG9naW5NZXNzYWdlRXJyb3IiOm51bGwsImxvZ2luRXJyb3JVc2VyTmFtZSI6bnVsbCwiY2hlY2tjb2RlIjpudWxsLCJzZWNyZXQiOiIzcnZPT2M2azZ0YlUyTC1ZbjFrRDQ0Y1YiLCJrb2EtZmxhc2giOnt9LCJfZXhwaXJlIjoxNTI1NDg1NTExMjYyLCJfbWF4QWdlIjo4NjQwMDAwMH0=; alicnweb=touch_tb_at%3D1525399659879%7ChomeIdttS%3D05074113413947949406728081773798878085%7ChomeIdttSAction%3Dtrue%7Clastlogonid%3D%25E8%25B5%25B5%25E6%2597%25AD%25E4%25B8%259Czx%7Cshow_inter_tips%3Dfalse; _m_h5_tk=a3574894823aa10d194d61126f7a3bf0_1525401755561; _m_h5_tk_enc=abeff2af19c9c0d9a7dc8dd3f089bc92; isg=BLq60CIqtN4hOjgzhB46v4qWC-Acwz2nBxCE4cSzZs0Xt1vxrPuOVYDRA0VrJ7bd");
	    	}else if(Fields.PLATFORM_JD_ZC.equals(dataType)){
	    		  map.put("coding", "utf-8");
	    		  map.put("cookie","_ga=GA1.3.1348533502.1525677902; _gid=GA1.3.1347410754.1525677902; __jdv=122270672|baidu|-|organic|not set|1525677905481; __jdu=15256779054801302799491; sec_flag=3ad1f024066193b2ab724fe4297643df; sec_addr=c0a803e2; __jdc=238784459; recentbrowse=ada5d925426842beb0c0f15233b66646; 3AB9D23F7A4B3C9B=QRF72YA5KMGSHVZBS6USLB36YVK7TTSB6VQWZQS3FOPUF2OSNETZISFC55RYGIASVKCYCUB4VOAHF5YO3JOYABF4CQ; __jda=238784459.15256779054801302799491.1525677905.1525684257.1525743805.3; _jrda=3");
	    		  map.put("urlRef", "https://z.jd.com/bigger/search.html");
	    	}else if(Fields.PLATFORM_TAOBAO_ZC.equals(dataType)){
	    		 map.put("coding", "GBK");
	    		  map.put("cookie",null);
	    		  map.put("urlRef", "https://www.taobao.com/markets/hi/list?spm=a215p.128754.1.6.627f4ed6KHrAen");
	    	}else if(Fields.platform_INDEX_1688.equals(dataType)){
	    		 map.put("coding", "UTF-8");
	    		  map.put("cookie","JSESSIONID=XK0ZEOm-8iXZHZOyg9JkwDart7-Mjt0drQ-fKy42; cookie2=1b58e1153a0a066fc95e9533acb38859; t=ef85024be9b247a4238bdb3f887f4d6c; _tb_token_=5ebe873ee3be5; __cn_logon__=false; _alizs_area_info_=1001; _alizs_user_type_=purchaser; ali_beacon_id=203.156.215.211.1525937658674.586830.4; cna=++N6E5BIQ2oCAYzOQzr3wXEq; UM_distinctid=16349634a868a-0d050b79fc308b-1373565-100200-16349634a874d8; _csrf_token=1525944634956; _tmp_ck_0='UskWF3HRLh2I9vpDRuYdIu2MWWBMN7qc5yrLZOtIMz8aFmkSkPjMr4DtDcuKjaWQKPTxbH8WRxClRLMDf0A5B3JUGnGfA18BnwQivN5xuI5BYaiUgXAO3SrqsTWoIkl258LgQM0bap7eM5kfWbzJxDEn4dPKYlhNZUlwF0uhHgg7OqHXv90WnYGPqb8oonYZTMk%2B6lxPw4wPgivmEjn0nvWksx6VQU15qpPKEWxrGh1YFmX9YcVBVLj2jQ%2F%2FcpgSeskTt9nLq8cBQHEMKwjARCmEkJ9AlE8OMG1XNwb14LOi1SmL%2FJJbjf3LQsWAhU5woWKbrNKWDMRSJHMWyApxipPLdoZL2PCFtmIGFp72Rc5BVAm9QlVDWRamVdLWB%2FIqytEnnSS%2Fb%2F8%3D'; ali_ab=203.156.215.211.1526012098233.4; _alizs_cate_info_=7%252C1046688; alicnweb=touch_tb_at%3D1526021170999; isg=BHp6kDvz9ADiLXhqAHJWFa-Ky6Bcg_3n1pDyQYRzio3adxuxbLqSFQHBwwOrZ3ad");
	    	}
	    	return map;
	    }
	    
	    public static List<String>location(){
	    	List<String>list=new ArrayList<String>();
	    	list.add(Fields.LOCATION_HELAN);
	    	list.add(Fields. LOCATION_AOZHOU);
	    	list.add(Fields. LOCATION_RIBEN);
	    	list.add(Fields. LOCATION_XINXILAN);
	    	list.add(Fields. LOCATION_DEGUO);
	    	list.add(Fields. LOCATION_MEIGUO);
	    	list.add(Fields. LOCATION_YINGGUO);
	    	list.add(Fields. LOCATION_FAGUO);
	    	list.add(Fields. LOCATION_AGENTING);
	    	list.add(Fields. LOCATION_ZHILI);
	    	list.add(Fields. LOCATION_HANGUO); 
	    	list.add(Fields. LOCATION_ZHONGGUO);
	    	list.add(Fields. LOCATION_DANMAI);
	    	list.add(Fields.LOCATION_AODALIY);
	    	list.add(Fields. LOCATION_AOZHOU);
	    	list.add(Fields. LOCATION_RUIDIAN);
	    	list.add(Fields.LOCATION_AIERLAN);
			return list;
	    }
	    //IP库随机分配
	    public static String IpDataSource(String dataSource){
	    	String IP="";
	    	if(dataSource.toString().contains(",")){
	    		String data[]=dataSource.split(",");
	    		int index = (int) (Math.random() * data.length);
	    		IP = data[index];
	    	}else{
	    		IP=dataSource;
	    	}
	    	return IP;
	    }
	    
	   public static String jdCookie(String regionAddress){
		String cookie="mobilev=html5; __jdv=122270672|direct|-|none|-|1522118955694; PCSYCityID=2; unpl=V2_ZzNtbUAAF0dzAEEAfB0LA2IDQQ9KUEJAIV1EAH5JVQ1gBhQNclRCFXwUR1ZnGlUUZwEZWEtcQB1FCHZXfBpaAmEBFl5yBBNNIEwEACtaDlwJARtURlZDFXY4dld7KVwEVwMWXkFXQxx9DEJTcxxfBmcGGlpDX0AlRQ92ZEsbVANuAhVtQ2dCJTdcGlB9EFQHZk4SWUFUQxV8AEJQfBFZBmQDF1VFVksWRQl2Vw%3d%3d; CCC_SE=ADC_ym4wXAfZE2Tl3rUgbtr6wszfnRI8FRIyMD08w7XftnS0rfwQeGZGayUY726IbwJ89OyeiKrZgP52fckZUTJj2I39XDw0SQJ0AGNdoyY5VCRWGTMYSvNodJx6Tj3zlyTQfOOoDpmN4a6PW9OtATH6YoKkekouqAaarPTNvmLYeAZFVETjizfSeC4LASwZPE3zyclz3tWm51q5RraVBKV2Cz5SfmSf5c%2bWIHFFk8J2Eorqq9cqKhEPkeEg7UO50FUrS32Da3SlkGxaWwWtC9ok15ui1w95PGF1goZ00UEnPCHUm%2fvY5oXdANhVa9r0jiJLSDoXtQBRe5QbUiJ8zCagY6aRbbxPSoj4JTByY05rvR3Mw76OUQGnuzMmyXXGyn%2bbjpLvYkdkD7y37ylZXU8tK3S7I5A6IKTisRnPDYse3qLcciMayHd%2f7KNkXGIiezVCDQiMx9BoZFhRc9MeFOVAgijb2ZGhlGOPmv13La1FA4uJmgTLfdQ4J0pC5hCmCqlgqafXtUgRJn8j%2fg0WR8yIfkfJrQTzZ3hlNBpUA8J36ZY%3d; mt_xid=V2_52007VwMWUV5YUlMXTR1bDWIAEVJYUFVbF0opW1JnUBFVXllOD0geGkAAMwBATg0PW14DTUtfBDQDEFZcXgZcL0oYXAx7AhBOXlBDWhhCG1QOYgUiUG1YYloXQB9eBGAHEmJYUQ%3D%3D; ipLoc-djd=3-51035-39620-0; areaId=REGIONADDRESS; mba_muid=1522118955694221496092; mba_sid=15223147237607271958672399781.1; sid=a2d7baf3b896a64d76d402901dacf505; __jda=122270672.1522118955694221496092.1522118955.1522208609.1522312687.6; __jdb=122270672.49.1522118955694221496092|6.1522312687; __jdc=122270672; __jdu=1522118955694221496092; 3AB9D23F7A4B3C9B=LUIO5ONLVFCACBKQNZUE2BNQDSNZX7AWDQVO6IUEXM4NPBL5KN2M3NMFMPOQV2JHM5DKKND7VGVP5EQHQ25OTW2X6Q";
		//String cookie="JAMCookie=true; __jdv=122270672|direct|-|none|-|1521034595186; 3AB9D23F7A4B3C9B=VZQOD4ZFW2G7ZAURD3QFTJ4Z7WA5WE3GKEBCMW2ZD7RZUH5KLGXCEK35SGQLRTKXRN5LNNSXEDIKJCB3RD52IFI4GA; abtest=20180315143049300_12; mobilev=html5; __jdu=15211042922782016018046; USER_FLAG_CHECK=6eb8fb6ca1f74a0213a30efb6db27b22; autoOpenApp_downCloseDate_auto=1521183437573_21600000; M_Identification=643e8172d7339357_213a8f1a4ebd0ac836fc8815f18c7ccd; M_Identification_abtest=20180315163325118_09594197; warehistory='3029684,6707875,'; __jda=122270672.15210345951861174872005.1521034595.1521102802.1521183437.4; __jdc=122270672; mba_muid=15210345951861174872005; sid=af945030598c3e6de5bb223334287466; regionAddress=REGIONADDRESS;"; 
		cookie=cookie.replace("REGIONADDRESS", regionAddress);
		return cookie;
	   }
}

