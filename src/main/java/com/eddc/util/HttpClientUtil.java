/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年10月5日 下午2:36:26 
 */
package com.eddc.util;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.springframework.stereotype.Component;
/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：HttpClientUtil   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年10月5日 下午2:36:26   
* 修改人：jack.zhao   
* 修改时间：2018年10月5日 下午2:36:26   
* 修改备注：   
* @version    
*    
*/
@Component
public class HttpClientUtil { 
 
	//读取超时
	private final static int SOCKET_TIMEOUT = 80000;
	//连接超时
	private final static int CONNECTION_TIMEOUT = 80000;
	// 每个HOST的最大连接数量
	private final static int MAX_CONN_PRE_HOST = 100;
	// 连接池的最大连接数
	private final static int MAX_CONN = 1000;
	// 连接池
	private final static HttpConnectionManager httpConnectionManager;
	//每个路由最大连接数 
	public final static int MAX_ROUTE_CONNECTIONS = 400;  
	static {
		httpConnectionManager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = httpConnectionManager.getParams();
		params.setConnectionTimeout(CONNECTION_TIMEOUT);
		params.setSoTimeout(SOCKET_TIMEOUT);
		params.setDefaultMaxConnectionsPerHost(MAX_CONN_PRE_HOST);
		params.setMaxTotalConnections(MAX_CONN);

	}
	
	public  static HttpClient getClient(){
		return new HttpClient(httpConnectionManager);
	}
}