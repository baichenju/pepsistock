package com.eddc.util;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.stereotype.Component;
import com.enterprise.support.utility.Validation;

/**
 * @author jack
 * @date 2017年10月30日 下午5:28:15
 * @description http请求工具类
 */
@Component
public class ClientHttpDataUtil {
	private final static int MAX_TIMEOUT =10000;
	private static Logger logger=Logger.getLogger(ClientHttpUtil.class);
	private final static ThreadLocal<HttpClient> clients = new ThreadLocal<HttpClient>();
	public  HttpClient getCurrentHttpClient()  {
		HttpClient client = clients.get();
		if (null == client) {
			client = HttpClientUtil.getClient();
			clients.set(client);
		}
		return client;
	}
	public  Map<String,String>getMap(Map<String,String>mapMessage) throws Exception {
		int stats=0; String result = null;
		Map<String,String>map=new HashMap<String, String>();
		if (Validation.isEmpty(mapMessage.get("url"))) {
			throw new Exception("参数异常、URL不能为空");
		} 	
		String charset=charsetData(mapMessage);
		HttpClient client=clientData(charset,mapMessage);	
		HttpMethod method=httpGetData(mapMessage,charset);
		try {
			try {
				stats = client.executeMethod(method);
				if (stats == HttpStatus.SC_OK) {
					Header header = method.getResponseHeader("Content-Encoding");
					if (header != null && !Validation.isEmpty(header.getValue()) && header.getValue().contains("gzip") ) {//
						InputStream input = new GZIPInputStream(method.getResponseBodyAsStream());
						result = IOUtils.toString(input, charset);
						IOUtils.closeQuietly(input);
					}else{
						result = method.getResponseBodyAsString();
					} 
				}else if (stats==HttpStatus.SC_NOT_FOUND) {
					Header header = method.getResponseHeader("Content-Encoding");
					if (header != null && !Validation.isEmpty(header.getValue()) && header.getValue().contains("gzip")) {
						InputStream input = new GZIPInputStream(method.getResponseBodyAsStream());
						result = IOUtils.toString(input, charset);
						IOUtils.closeQuietly(input);
					} else {
						result = method.getResponseBodyAsString();
					}
				}else if(Validation.isEmpty(stats)) {
					result=null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("请求数据超时：stats 等于"+stats+"-----------------------"+e);
				result=null;
			}
		} catch (Exception e) {
			method.abort();
			logger.error("请求数据失败", e);
		} finally {
			method.releaseConnection();
		}
		map.put("result", result);
		return map;
	}
	//post 请求
	@SuppressWarnings("unchecked")
	public static String post( Map<String, Object> map,Map<String,String>PostData)throws Exception {
		String ret = null;
		CloseableHttpClient closeHttpClient = HttpClients.createDefault();  
		CloseableHttpResponse httpResponse = null;  
		RequestConfig defaultRequestConfig = RequestConfig.custom()
				.setSocketTimeout(MAX_TIMEOUT)
				.setConnectTimeout(MAX_TIMEOUT)
				.setConnectionRequestTimeout(MAX_TIMEOUT)
				.setCircularRedirectsAllowed(false).build();
		// 建立一个NameValuePair数组，用于存储欲传递的参数
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		if (map != null) {// 添加参数
			for (String key : map.keySet()) {
				String value = "";
				if(map.get(key) instanceof Integer){
					value=(Integer)map.get(key)+"";
				}else if(map.get(key) instanceof String){
					value=(String)map.get(key);
				}else if(map.get(key) instanceof List){
					List<String> list = (List<String>) map.get(key);
					for(String s : list){
						params.add(new BasicNameValuePair(key, s));
					}
				}else{
					continue;
				}
				params.add(new BasicNameValuePair(key, value));
			}
		}

		HttpPost post=httpPostData(PostData);
		post.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
		try {
			post.setConfig(defaultRequestConfig);
			httpResponse = closeHttpClient.execute(post);  
			HttpEntity httpEntity = httpResponse.getEntity();  
			ret = EntityUtils.toString(httpEntity);  
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("POST " + PostData.get("url") + " failed!");
		} finally {
			post.abort();
		}
		return ret;
	}


	public HttpClient clientData(String charset,Map<String,String>mapMessage){
		HttpClient client = getCurrentHttpClient();
		client.getParams().setContentCharset(charset);
		client.getParams().setSoTimeout(MAX_TIMEOUT);
		client.getParams().setConnectionManagerTimeout(MAX_TIMEOUT);
		client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, charset);
		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(200, true));
		client.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
		client.getParams().setIntParameter("http.socket.timeout", MAX_TIMEOUT);
		if(!Validation.isEmpty(mapMessage.get("ip"))){
			client.getHostConfiguration().setProxy(mapMessage.get("ip"), Integer.parseInt(mapMessage.get("port")));
		}		
		return client;	
	}
	public HttpMethod httpGetData(Map<String,String>mapMessage,String charset){
		HttpMethod httpGet = new GetMethod(mapMessage.get("url"));
		httpGet.getParams().setParameter("http.socket.timeout", MAX_TIMEOUT);
		httpGet.setRequestHeader("Host",host(mapMessage));
		httpGet.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		httpGet.setRequestHeader("Connection", "keep-alive");
		httpGet.setRequestHeader("Content-Type", "application/x-javascript;charset="+charset);
		httpGet.setRequestHeader("Accept", "*/*");
		httpGet.setRequestHeader("Cache-Control", "max-age=0");
		httpGet.setRequestHeader("Pragma", "no-cache");
		httpGet.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		httpGet.setRequestHeader("Accept-Encoding", "gzip, deflate, br");//zip, deflate, sdch, br
		httpGet.setRequestHeader("Cache-Control", "no-cache");
		httpGet.setRequestHeader("Upgrade-insecure-Requests", "1");
		httpGet.setRequestHeader("User-Agent",HttpCrawlerUtil.CrawlerAttribute());
		httpGet.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
		if (!Validation.isEmpty(mapMessage.get("cookie")) || !Validation.isEmpty(mapMessage.get("Cookie"))) {
			httpGet.setRequestHeader("Cookie", mapMessage.get("cookie"));
		}
		if (!Validation.isEmpty(mapMessage.get("urlRef"))) {
			httpGet.setRequestHeader("Referer", mapMessage.get("urlRef"));
		}

		return httpGet;
	}
	public static  HttpPost httpPostData(Map<String,String>PostData){
		HttpPost post = new HttpPost(PostData.get("url"));
		post.setHeader("Host",host(PostData));
		post.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
		post.setHeader("User-Agent",HttpCrawlerUtil.CrawlerAttribute());
		post.setHeader("Connection", "keep-alive");
		post.setHeader("Content-Type", "application/x-javascript;charset="+PostData.get("coding"));
		post.setHeader("Accept", "*/*");
		post.setHeader("Pragma", "no-cache");
		post.setHeader("X-Requested-With", "XMLHttpRequest");
		post.setHeader("Cache-Control", "no-cache");
		post.setHeader("Accept-Encoding", "gzip, deflate");
		if(!Validation.isEmpty(PostData.get("cookie"))){
			post.setHeader("Cookie", PostData.get("cookie"));
		}
		if(!Validation.isEmpty(PostData.get("urlRef"))){
			post.setHeader("Referer", PostData.get("urlRef"));
		}
		return post;
	}
	
	public   String  getJsoupResult4G(Map <String,String>mapLog){
		String user_Agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";
		String priceString="";
		String HOST=""; 
		String spitString[]=mapLog.get("url").split("/");
		if(!Validation.isEmpty(mapLog.get("url"))){
			HOST=spitString[2];
		}
		try {//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(mapLog.get("ip"),Integer.valueOf(mapLog.get("port"))));
			Connection con = Jsoup.connect(mapLog.get("url"));
			con.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
			con.header("Accept-Encoding", "gzip, deflate, br");
			con.header("Accept-Language", "zh-CN,zh;q=0.8");
			con.header("Cache-Control", "no-cache");
			con.header("Connection","keep-alive");
			con.header("Pragma","no-cache");
			con.header("Pragma","no-cache");
			con.header("Upgrade-Insecure-Requests","1");
			con.header("x-requested-with","XMLHttpRequest");
			con.header("Host", HOST);
			con.header("User-Agent",user_Agent).ignoreContentType(true).timeout(50000).maxBodySize(0);
			if(Validation.isNotEmpty(mapLog.get("urlRef"))){
				con.header("Referer",mapLog.get("urlRef"));	
			}
			if(Validation.isNotEmpty(mapLog.get("cookie"))){
				con.header("Cookie",mapLog.get("cookie").toString());	
			}
			Element body =con.get().body();
			priceString=body.text(); 
		} catch (IOException e) {
			priceString="failure"; 
		}
		return priceString;
	}


	public static String host(Map<String,String>mapMessage){
		String HOST=null;
		String spitString[]=mapMessage.get("url").split("/");
		if(!Validation.isEmpty(mapMessage.get("url"))){
			HOST=spitString[2];
		}
		return HOST;
	}
	public String charsetData(Map<String,String>mapMessage){
		String  charset="";
		try{
			charset=mapMessage.get("coding");
			if (Validation.isEmpty(charset)) {
				charset=Fields.UTF8;
			}
		}catch(Exception e){
			charset=Fields.UTF8;
		}
		return charset;
	}

}
