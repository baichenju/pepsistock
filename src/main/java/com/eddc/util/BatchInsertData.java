/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年6月30日 下午7:07:06 
 */
package com.eddc.util;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eddc.framework.TargetDataSource;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**   
 *    
 * 项目名称：selection_sibrary_crawler   
 * 类名称：BatchInsertData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年6月30日 下午7:07:06   
 * 修改人：jack.zhao   
 * 修改时间：2018年6月30日 下午7:07:06   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class BatchInsertData {
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	private String dataDsNameSpace = "com.eddc.mapper.CrawlerPublicClassMapper";
	private static Logger logger = Logger.getLogger(BatchInsertData.class);
	@TargetDataSource
	public void insertIntoData(List<Map<String,Object>> insertItems,String database,String tableName){
		SqlSession session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		Map<String,Object> params = Maps.newHashMap();
		logger.info(">>> Start inserting data , tableName : + " + tableName +" >>>");
		try { 
			if (!insertItems.isEmpty()) {
				//这里把数据分成每1000条执行一次，可根据实际情况进行调整
				int count = insertItems.size() / 20;
				int sum = insertItems.size() % 20;
				for (int i = 0; i <= count; i++) {
					logger.info(">>> Current batch is " + i + " >>>");
					List<Map<String,Object>> subList = Lists.newArrayList();
					if (i == count) {
						if(sum != 0){
							subList = insertItems.subList(i * 20, 20 * i + sum);
						}else {
							continue;
						}
					} else {
						subList = insertItems.subList(i * 20, 20 * (i + 1));
					}
					params.put("table_name", tableName);
					params.put("fields", subList.get(0));
					params.put("list", subList);
					session.insert(dataDsNameSpace+".insertDatas", params);
					session.commit();
					session.clearCache();// 清理缓存，防止溢出
					logger.info(">>> Insert current batch : " + i+1 + " data is complete >>>");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.rollback();
			logger.error(">>> Rollback,insert data the failure please check! >>>"+tableName+">>>>>>>>>>");
			logger.error(e.getMessage());
		}finally {
			logger.info(">>> Insert data session close >>>");
			session.close();
		}  

	}
	
	@TargetDataSource
	public void insertIntoData2(List<Map<String,Object>> insertItems,String database,String tableName) throws Exception {
		SqlSession batchSqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		Map<String,Object> params = Maps.newHashMap();
		try {
			int batchCount = 1000;// 每批commit的个数
			int batchLastIndex = batchCount;// 每批最后一个的下标
			for (int index = 0; index < insertItems.size(); index++) {
				List<Map<String,Object>> subList = Lists.newArrayList();
				if (batchLastIndex >= insertItems.size()) {
					batchLastIndex = insertItems.size();
					subList = insertItems.subList(index, batchLastIndex);
					params.put("table_name", tableName);
					params.put("fields", subList.get(0));
					params.put("list", subList);
					batchSqlSession.insert(dataDsNameSpace+".insertDatas",params);
					batchSqlSession.commit();
					System.out.println("index:" + index+ " batchLastIndex:" + batchLastIndex);
					break;// 数据插入完毕，退出循环
				} else {
					subList = insertItems.subList(index, batchLastIndex);
					params.put("table_name", tableName);
					params.put("fields", subList.get(0));
					params.put("list", subList);
					batchSqlSession.insert(dataDsNameSpace+".insertDatas",params);
					index = batchLastIndex;// 设置下一批下标
					batchLastIndex = index + (batchCount - 1);
				}
			}
			batchSqlSession.commit();
		}catch(Exception e){ 
			e.printStackTrace();
			batchSqlSession.rollback();
			logger.error(">>> Rollback,insert data the failure please check! >>>");
			logger.error(e.getMessage());
		}finally {
			batchSqlSession.close();
		}
	}
}
