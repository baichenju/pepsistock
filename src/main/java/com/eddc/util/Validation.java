package com.eddc.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Validation implements Serializable {

    public static Boolean isNull(Object object) {
        return object == null ? true : false;
    }

    public static Boolean isEmpty(Integer number) {
        return (isNull(number) || number == 0 || number < 1) ? true : false;
    }

    public static Boolean isEmpty(Long number) {
        return (isNull(number) || number == 0 || number < 1) ? true : false;
    }

    public static Boolean isEmpty(String string) {
        return (isNull(string) || string.equalsIgnoreCase("")) ? true : false;
    }

    public static Boolean isEmpty(List list) {
        return (isNull(list) || list.size() < 1) ? true : false;
    }

    public static Boolean isEmpty(Object[] objects) {
        return (isNull(objects) || objects.length < 1) ? true : false;
    }

    public static Boolean isEmpty(Collection objects) {
        return (isNull(objects) || objects.size() < 1) ? true : false;
    }

    public static Boolean isEmpty(Object object, String... equals) {
        if (isNull(object)) {
            return true;
        } else if (object.getClass().getSimpleName().equalsIgnoreCase("String")) {
            return isEmpty((String) object) ? true : equals.length == 0 ? false : ((String) object).equalsIgnoreCase(equals[0]);
        } else if (object.getClass().getSimpleName().equalsIgnoreCase("Long")) {
            return isEmpty((Long) object);
        } else if (object.getClass().getSimpleName().equalsIgnoreCase("List")) {
            return isEmpty((List) object);
        } else if (object.getClass().getSimpleName().equalsIgnoreCase("Integer")) {
            return isEmpty((Integer) object);
        } else if (object instanceof Object[]) {
            return isEmpty((Object[]) object);
        } else if (object instanceof Collection) {
            return isEmpty((Collection) object);
        } else if (object instanceof Map) {
            return ((Map) object).isEmpty();
        }
        return equals.length == 0 ? false : object.toString().equalsIgnoreCase(equals[0]);
    }


    public static Boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }

    /**
     * 验证字符串str是否只包含英文,数字和"-_"
     *
     * @param str 待验证字符串
     * @return true 是只包含英文,数字和"-_" false	不是只包含英文,数字和"-_"
     */
    public static boolean onlyCharAndNumber(String str) {
        return Pattern.compile("^[a-zA-Z0-9]+([-_][a-zA-Z0-9]+)*$").matcher(str).find();
    }


    /**
     * 验证字符串str是否只包含英文,数字,中文,空格和"-_"
     *
     * @param str 待验证字符串
     * @return true 是只包含英文,数字,中文和"-_" false	不是只包含英文,数字,中文和"-_"
     */
    public static boolean onlyCharAndNumberAndCh(String str) {
        return Pattern.compile("^[a-zA-Z0-9\u4e00-\u9fa5\\s]+([-_][a-zA-Z0-9\u4e00-\u9fa5\\s]+)*$").matcher(str).find();
    }

    /**
     * 验证字符串email是否符合email格式
     *
     * @param email 待验证邮件地址
     * @return true 符合 false	不符合
     */
    public static boolean isEmail(String email) {
        return isEmpty(email) ? false : Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*").matcher(email).find();
    }

    /**
     * 验证字串是否手机号
     *
     * @param mobile 待验证手机号
     * @return true 符合 false	不符合
     */
    public static boolean mobile(String mobile) {
        return isEmpty(mobile) ? false : Pattern.compile("^1[0-9]{10}$", Pattern.CASE_INSENSITIVE).matcher(mobile).find();
    }

    /**
     * <b>功能：</b>验证字串图片文件是否属于限定的某中类型格式
     *
     * @param filepath 待验证图片名称
     * @return true 符合 false	不符合
     */
    public static boolean isImagefile(String filepath) {
        return isEmpty(filepath) ? false : Pattern.compile("\\.(?:GIF|JPG|JPEG|BMP|PNG)$", Pattern.CASE_INSENSITIVE).matcher(filepath).find();
    }


    /**
     * <b>功能：</b>验证字符串date是否符合Date格式
     *
     * @param date 待验证邮件地址
     * @return true 符合 false	不符合
     */
    public static boolean isDate(String date) {
        return isEmpty(date) ? false : Pattern.compile("^((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$").matcher(date).find();
    }

    /**
     * 验证字符串tags是否符合 Tags标签 正确格式 (aaa ddd ccc)
     *
     * @param tags
     * @return true|false 符合标签格式 true 否则false
     */
    public static boolean tags(String tags) {
        return isEmpty(tags) ? false : tags.matches("^\\s*(([a-z0-9A-Z\\u4e00-\\u9fa5]{2,8})\\s+){0,2}([a-z0-9A-Z\\u4e00-\\u9fa5]{2,8}\\s*)?$");
    }

    /**
     * 查找字符串email的邮箱名称
     *
     * @param email
     * @return 邮箱 用户名
     */
    public static String findEmailName(String email) {
        return email.substring(0, email.indexOf("@"));
    }

    /**
     * 正则验证
     *
     * @param value
     * @param regex
     * @return
     */
    public static boolean matches(Object value, String regex) {
        return isEmpty(value) || isEmpty(regex) ? false : String.valueOf(value).matches(regex);
    }

}
