package com.eddc.util;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.sf.json.JSONException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.stereotype.Component;
import com.enterprise.support.utility.Validation;

/**
 * @author jack
 * @date 2017年10月30日 下午5:28:15
 * @description http请求工具类
 */
@Component
public class ClientHttpUtil {
	private final static int MAX_TIMEOUT = 90000;
	private static Logger logger=Logger.getLogger(ClientHttpUtil.class);
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  Map<String,String>getMap(Map<String,String>mapMessage) throws Exception {
		CookieStore cookieStore = null;  CloseableHttpResponse response = null;CloseableHttpResponse responses = null;
		String HOST=null;String result = null;
		Map<String,String>map=new HashMap<String, String>();
		String userName=ResourceBundle.getBundle("docker").getString("userName_data");
		String password=ResourceBundle.getBundle("docker").getString("password_data");
		try {
			String proxyHost =mapMessage.get("ip");
			int proxyPort =Integer.valueOf(mapMessage.get("port"));
			String spitString[]=mapMessage.get("url").split("/");
			if(!Validation.isEmpty(mapMessage.get("url"))){HOST=spitString[2];}
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider.setCredentials( new AuthScope(proxyHost,proxyPort ),  new UsernamePasswordCredentials(userName,password));
			ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
			LayeredConnectionSocketFactory sslsf = SSLConnectionSocketFactory.getSocketFactory();
			// 采用绕过验证的方式处理https请求
			SSLContext sslcontext = createIgnoreVerifySSL();
			// 设置协议http和https对应的处理socket链接工厂的对象
			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
					.register("http", PlainConnectionSocketFactory.INSTANCE)
					.register("https", new SSLConnectionSocketFactory(sslcontext)).build();
			PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
			connManager.setMaxTotal(600);
			connManager.setDefaultMaxPerRoute(300);		
			//HttpClients.custom().setConnectionManager(connManager);      
//			Registry registry = RegistryBuilder.create().register("http", plainsf).register("https", sslsf).build();
			HttpHost proxy = new HttpHost(proxyHost, proxyPort);
     		RequestConfig config = RequestConfig.custom().setSocketTimeout(MAX_TIMEOUT).setConnectTimeout(MAX_TIMEOUT).setProxy(proxy).build();
//			PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
//			cm.setMaxTotal(600);
//			cm.setDefaultMaxPerRoute(300);		
			CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(connManager).setDefaultCredentialsProvider(credsProvider).setDefaultCookieStore(cookieStore).build();
			String charset=mapMessage.get("coding");
			if (Validation.isEmpty(charset)) {charset = "UTF-8";}
			HttpGet httpGet=httpGetData(mapMessage,charset,HOST);
			httpGet.setConfig(config);
			try {
				SSLSocketFactory.getSocketFactory().setHostnameVerifier(new AllowAllHostnameVerifier());
				response = httpclient.execute(httpGet);//请求数据
			} catch (ClientProtocolException e) {//重定向url进行请求数据
				if(mapMessage.get("platform").equalsIgnoreCase(Fields.PLATFORM_JD)){
					try {
						HttpClientContext context = HttpClientContext.create();
						HttpGet httpGets = new HttpGet(mapMessage.get("url"));
						SSLSocketFactory.getSocketFactory().setHostnameVerifier(new AllowAllHostnameVerifier());
						response = httpclient.execute(httpGets, context);
						List<URI>redirectLocations = context.getRedirectLocations();
						httpGet.setHeader("host",redirectLocations.get(0).toASCIIString());
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
			HttpEntity entity = response.getEntity();
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				result = EntityUtils.toString(entity,charset);
				if(result.contains(Fields.MESS_CODE)){//处理乱码
					SSLSocketFactory.getSocketFactory().setHostnameVerifier(new AllowAllHostnameVerifier());
					responses= httpclient.execute(httpGet);//请求数据
					result = EntityUtils.toString(responses.getEntity(),Fields.GBK);	
					EntityUtils.consume(responses.getEntity());
					responses.close();	
				}
				EntityUtils.consume(entity);
				response.close();	
				logger.info("----->> HttpClient request SUCCESS status code : "+response.getStatusLine().getStatusCode());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND){
				logger.info("----->> HttpClient request fail status code 404 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN){
				logger.info("----->> HttpClient request fail status code 403 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED){
				logger.info("----->> HttpClient request fail status code 401 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST){
				logger.info("----->> HttpClient request fail status code 400 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_PAYMENT_REQUIRED){
				logger.info("----->> HttpClient request fail status code 402 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR){
				logger.info("----->> HttpClient request fail status code 500 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_IMPLEMENTED){
				logger.info("----->> HttpClient request fail status code 501 : " +mapMessage.get("url").toString());	
			}else if(response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_GATEWAY){
				logger.info("----->> HttpClient request fail status code 502 : " +mapMessage.get("url").toString());	
			}else if(Validation.isEmpty(result)) {
				logger.info("----->> HttpClient request fail status code : "+response.getStatusLine().getStatusCode()+mapMessage.get("url").toString());	
				result=null;
			}else{
				logger.info("----->> HttpClient request fail status code : "+response.getStatusLine().getStatusCode()+mapMessage.get("url").toString());	
			}

		} catch (Exception e) {
			logger.error("请求数据失败>>>>>>>>>>>>"+response.getStatusLine().getStatusCode()+">>>>>>>>>>>>>>",e);
		}
		map.put("result", result);
		return map;
	}
	/**  
	 * @Title: httpGetData  
	 * @Description: TODO(封装HttpGet)  
	 * @param @param mapMessage
	 * @param @return    设定文件  
	 * @return HttpGet    返回类型  
	 * @throws  
	 */  
	public HttpGet httpGetData(Map<String,String>mapMessage,String charset,String host){
		HttpGet httpGet = new HttpGet(mapMessage.get("url"));
		httpGet.setHeader("Host",host);
		httpGet.setHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		httpGet.setHeader("Connection", "keep-alive");
		httpGet.setHeader("Content-Type", "application/x-javascript;charset="+charset);
		httpGet.setHeader("Accept", "*/*");
		httpGet.setHeader("Cache-Control", "max-age=0");
		httpGet.setHeader("Pragma", "no-cache");
		httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
		httpGet.setHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		httpGet.setHeader("Cache-Control", "no-cache");
		httpGet.setHeader("Upgrade-insecure-Requests", "1");
		httpGet.setHeader("User-Agent",HttpCrawlerUtil.CrawlerAttribute());
		if (!Validation.isEmpty(mapMessage.get("cookie")) || !Validation.isEmpty(mapMessage.get("Cookie"))) {
			httpGet.setHeader("Cookie", mapMessage.get("cookie"));
		}
		if (!Validation.isEmpty(mapMessage.get("urlRef"))) {
			httpGet.setHeader("Referer", mapMessage.get("urlRef"));
		}
		return httpGet;
	}
	//post 请求
	@SuppressWarnings("unchecked")
	public static String post( Map<String, Object> map,Map<String,String>PostData)throws Exception {
		String ret = null;
		CloseableHttpClient closeHttpClient = HttpClients.createDefault();  
		CloseableHttpResponse httpResponse = null;  
		RequestConfig defaultRequestConfig = RequestConfig.custom()
				.setSocketTimeout(MAX_TIMEOUT)
				.setConnectTimeout(MAX_TIMEOUT)
				.setConnectionRequestTimeout(MAX_TIMEOUT)
				.setCircularRedirectsAllowed(false).build();
		// 建立一个NameValuePair数组，用于存储欲传递的参数
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		if (map != null) {// 添加参数
			for (String key : map.keySet()) {
				String value = "";
				if(map.get(key) instanceof Integer){
					value=(Integer)map.get(key)+"";
				}else if(map.get(key) instanceof String){
					value=(String)map.get(key);
				}else if(map.get(key) instanceof List){
					List<String> list = (List<String>) map.get(key);
					for(String s : list){
						params.add(new BasicNameValuePair(key, s));
					}
				}else{
					continue;
				}
				params.add(new BasicNameValuePair(key, value));
			}
		}
		HttpPost post = new HttpPost(PostData.get("url"));
		post.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
		String HOST=null;
		String spitString[]=PostData.get("url").split("/");
		if(!Validation.isEmpty(PostData.get("url"))){
			HOST=spitString[2];
		}
		try {
			post.setHeader("Host",HOST);
			post.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
			post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
			post.setHeader("Connection", "keep-alive");
			post.setHeader("Content-Type", "application/x-javascript;charset="+PostData.get("coding"));
			post.setHeader("Accept", "*/*");
			//post.setHeader("Origin", PostData.get("origin"));
			post.setHeader("Pragma", "no-cache");
			post.setHeader("X-Requested-With", "XMLHttpRequest");
			post.setHeader("Cache-Control", "no-cache");
			post.setHeader("Accept-Encoding", "gzip, deflate");
			if(!Validation.isEmpty(PostData.get("cookie"))){
				post.setHeader("Cookie", PostData.get("cookie"));
			}
			if(!Validation.isEmpty(PostData.get("urlRef"))){
				post.setHeader("Referer", PostData.get("urlRef"));
			}
			post.setConfig(defaultRequestConfig);
			httpResponse = closeHttpClient.execute(post);  
			HttpEntity httpEntity = httpResponse.getEntity();  
			ret = EntityUtils.toString(httpEntity);  
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("POST " + PostData.get("url") + " failed!");
		} finally {
			post.abort();
		}
		return ret;
	}

	public Document httpGet(String url,String cookie) throws IOException{
		//获取请求连接
		Connection con = Jsoup.connect(url);
		//请求头设置，特别是cookie设置
		con.header("Accept", "text/html, application/xhtml+xml, */*"); 
		con.header("Content-Type", "application/x-www-form-urlencoded");
		con.header("Accept-Encoding", "gzip, deflate, sdch, br");
		con.header("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0))"); 
		con.header("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3").timeout(1000);//Response ss .ignoreContentType(true).execute()  ss.body();
		con.header("Cookie", "cna=J9yBEjUYJ18CAYzOQzqzf2j1; hng=CN%7Czh-CN%7CCNY%7C156; thw=cn; miid=5366869721331323302; v=0; alitrackid=www.taobao.com; lastalitrackid=www.taobao.com; swfstore=142925; _tb_token_=30cc7b7958716; tk_trace=oTRxOWSBNwn9dPy4KVJVbutfzK5InlkjwbWpxHegXyGxPdWTLVRjn23RuZzZtB1ZgD6Khe0jl%2BAoo68rryovRBE2Yp933GccTPwH%2FTbWVnqEfudSt0ozZPG%2BkA1iKeVv2L5C1tkul3c1pEAfoOzBoBsNsJySRN5g7VG9h73waITUSDg5joDrKsiaccr%2B2B4QSpDs7e0NIAIKxBSdPu6Y6mEZrAaSW%2BAfEQ7Qd91oAa6mAMrWHWje%2BUfOxJQm8Oq1ox7SONNnVyHlQVcUCMyrpBvuags6gEofeGH3WZnnXNm4HjVgsp4Epd0mR%2BgRPkw%2FlNgu5DBvhfFSRMPOh1Z0XA%3D%3D; linezing_session=o7Hi416XJYuLSRZ7QZBFjwfP_1510712427612IkRq_8; uc3=sg2=UojQbRVHa5BKhStiSwLPQaAKPo%2B9DwJIRTX6k%2BWWdG8%3D&nk2=tsV8GJVVWb0%3D&id2=WvA07t216W%2BH&vt3=F8dBzLOSwUDJBK33wuQ%3D&lg2=UIHiLt3xD8xYTw%3D%3D; existShop=MTUxMDcxMjYwOQ%3D%3D; lgc=%5Cu8D75%5Cu65ED%5Cu4E1Czx; tracknick=%5Cu8D75%5Cu65ED%5Cu4E1Czx; cookie2=13903a637cf38429da6dab62a845e2ce; mt=np=&ci=-1_1; skt=d412064b8fc560a4; t=43b3fcd9469aab9535b7e1b70d7d4c25; _cc_=Vq8l%2BKCLiw%3D%3D; tg=0; whl=-1%260%260%261510717789907; uc1=cart_m=0&cookie14=UoTde9HZLmsEAA%3D%3D&lng=zh_CN&cookie16=WqG3DMC9UpAPBHGz5QBErFxlCA%3D%3D&existShop=false&cookie21=WqG3DMC9Edo1TBf%2BcZ0sSw%3D%3D&tag=8&cookie15=U%2BGCWk%2F75gdr5Q%3D%3D&pas=0; JSESSIONID=955ABD7D28CA82C3210021B93D72C738; isg=ApycKxeQKqSZ1d2JyK6uXNRnbbqOvUPUDr94PXadoAdqwTxLniUQzxJzVx_C; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0%26__ll%3D-1%26_ato%3D0");
		Document doc=con.get();  
		//解析请求结果
		return doc;
	}

	public   String  getJsoupResultMessage(Map <String,String>mapLog){
		String user_Agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";
		String priceString="";
		Document doc;
		String HOST="";
		String spitString[]=mapLog.get("url").split("/");
		if(!Validation.isEmpty(mapLog.get("url"))){
			HOST=spitString[2];
		}
		try {
			doc = Jsoup.connect(mapLog.get("url")).
					header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8").
					header("Accept-Encoding", "gzip, deflate, br").
					header("Accept-Language", "zh-CN,zh;q=0.8").
					header("Cache-Control", "no-cache").
					header("Connection","keep-alive").
					header("Pragma","no-cache").
					header("Cookie","cna=b4k/E3W94EgCAcuc19MXE4AN; hng=CN%7Czh-CN%7CCNY%7C156; uc1=cookie14=UoTePMZMF%2BtTdQ%3D%3D&lng=zh_CN&cookie16=VFC%2FuZ9az08KUQ56dCrZDlbNdA%3D%3D&existShop=false&cookie21=U%2BGCWk%2F7owY3i1vB1W2BgQ%3D%3D&tag=8&cookie15=URm48syIIVrSKA%3D%3D&pas=0; uc3=nk2=tsV8GJVVWb0%3D&id2=WvA07t216W%2BH&vt3=F8dBz4KEvh%2Bq2JeD6qY%3D&lg2=VT5L2FSpMGV7TQ%3D%3D; tracknick=%5Cu8D75%5Cu65ED%5Cu4E1Czx; ck1=; lgc=%5Cu8D75%5Cu65ED%5Cu4E1Czx; cookie2=18f5e1658d274efb49360e85ef350fad; t=5d663418df1edef4fee5c9957e27cbf2; skt=f26281b0971e1c6d; _tb_token_=c03ee31833d04; swfstore=215406; whl=-1%260%260%260; x=__ll%3D-1%26_ato%3D0; enc=iZ5mpmCMdp0bPp8rAZognJM52jI9drDfL%2BGqIVX48DcrRGNrS6gtZsS7aj%2BHO12opgFFNNY%2FJDvEh%2FPUxMr5ew%3D%3D; sm4=310100; _m_h5_tk=b78d7e42dbfd9ef797eb660a144b62b4_1522145135637; _m_h5_tk_enc=f7a77fc456a0911d982e3661fb035e20; pnm_cku822=; cq=ccp%3D1; otherx=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0; isg=BA4OxeJC2MI4WWzBgLnbxoipX-QQJ9FrKFWqUzhTBpHem671oB8imbQS18f3g8qh").
					header("Pragma","no-cache").
					header("Upgrade-Insecure-Requests","1").
					header("Referer",mapLog.get("urlRef")).
					header("x-requested-with","XMLHttpRequest").
					header("Host", HOST).
					userAgent(user_Agent).
					ignoreContentType(true).
					timeout(50000).get();
			if(doc.toString().contains("sec.taobao.com")){
				System.out.println("数据没有抓取到");
			}else{
				priceString=doc.toString();
			} 
		} catch (IOException e) {
			priceString="failure"; 
		}
		return priceString;
	}
	public static  String  getJsoupResult(String url, String ref,String Host){
		String user_Agent="Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.15)";
		String priceString="";
		Document doc;
		try {
			doc = Jsoup.connect(url).
					header("Accept", "*/*").
					header("Accept-Charset", "UTF-8,*;q=0.5").
					header("Accept-Encoding", "gzip, deflate, sdch, br").
					header("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6").
					header("Cache-Control", "no-cache").
					header("Connection","keep-alive").
					header("Pragma","no-cache").
					//header("Cookie",Cookie).
					header("Host", Host).
					header("Referer", ref).
					userAgent(user_Agent).
					ignoreContentType(true).
					timeout(50000).get();
			if(doc.toString().contains("sec.taobao.com")){
				System.out.println("数据没有抓取到");
			}else{
				priceString=doc.toString();
			} 
		} catch (IOException e) {
			priceString="failure"; 
		}
		return priceString;
	}

	public  String getJsonObj(String src) {
		InputStreamReader reader = null;
		BufferedReader in = null;
		try { 
			URL url = new URL(src);
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(1000);
			reader = new InputStreamReader(connection.getInputStream(), "utf-8");
			in = new BufferedReader(reader);
			String line = null;        //每行内容
			int lineFlag = 0;        //标记: 判断有没有数据
			StringBuffer content = new StringBuffer();
			while ((line = in.readLine()) != null) {
				//String temp = line.substring(line.indexOf("{"), line.lastIndexOf("}") + 1);
				content.append(line.toString());
				lineFlag++;
			}
			if(lineFlag!=0 ){
				//JSONObject jsonObject = JSONObject.fromObject(content.toString());	
				return content.toString();
			}else{
				return null;
			}
		} catch (SocketTimeoutException e) {
			logger.info("连接超时!!!"+src);
			return null;
		} catch (JSONException e) {
			logger.info("网站响应不是json格式，无法转化成JSONObject!!!");
			return null;
		} catch (Exception e) {
			logger.info("连接网址不对或读取流出现异常!!!");
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					logger.info("关闭流出现异常!!!");
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.info("关闭流出现异常!!!");
				}
			}
		}
	}
	/**
	 * 获取URL HOST 地址
	 * @param url
	 * @return
	 */
	public static String getHost(String url) {
		url = url.substring(7);
		System.out.println(url.substring(0, url.indexOf("/") == -1 ? url.length() : url.indexOf("/")));
		return url.substring(0, url.indexOf("/") == -1 ? url.length() : url.indexOf("/"));
	}

	public static String get(String host, Map<String, String> params) {
		int SOCKET_TIMEOUT = 10000; // 10S
		String GET = "GET";
		try {
			// 设置SSLContext
			SSLContext sslcontext = SSLContext.getInstance("TLS");
			sslcontext.init(null, new TrustManager[] { myX509TrustManager }, null);
			String sendUrl = getUrlWithQueryString(host, params);
			URL uri = new URL(sendUrl); // 创建URL对象
			HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
			if (conn instanceof HttpsURLConnection) {
				((HttpsURLConnection) conn).setSSLSocketFactory(sslcontext.getSocketFactory());
			}
			conn.setConnectTimeout(SOCKET_TIMEOUT); // 设置相应超时
			conn.setRequestMethod(GET);
			int statusCode = conn.getResponseCode();
			if (statusCode != HttpURLConnection.HTTP_OK) {
				System.out.println("Http错误码：" + statusCode);
			}
			// 读取服务器的数据
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null) {
				builder.append(line);
			}
			String text = builder.toString();
			close(br); // 关闭数据流
			close(is); // 关闭数据流
			conn.disconnect(); // 断开连接
			return text;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getUrlWithQueryString(String url, Map<String, String> params) {
		if (params == null) {
			return url;
		}

		StringBuilder builder = new StringBuilder(url);
		if (url.contains("?")) {
			builder.append("&");
		} else {
			builder.append("?");
		}
		int i = 0;
		for (String key : params.keySet()) {
			String value = params.get(key);
			if (value == null) { // 过滤空的key
				continue;
			}
			if (i != 0) {
				builder.append('&');
			}

			builder.append(key);
			builder.append('=');
			builder.append(encode(value));
			i++;
		}
		return builder.toString();
	}

	protected static void close(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 对输入的字符串进行URL编码, 即转换为%20这种形式
	 * 
	 * @param input 原文
	 * @return URL编码. 如果编码失败, 则返回原文
	 */
	public static String encode(String input) {
		if (input == null) {
			return "";
		}

		try {
			return URLEncoder.encode(input, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return input;
	}
	
	/**
	 * 获取SSL套接字对象 重点重点：设置tls协议的版本
	 * 
	 * @return
	 */
	public static SSLContext createIgnoreVerifySSL() {
		SSLContext sslContext = null;// 创建套接字对象
		try {
			sslContext = SSLContext.getInstance("TLSv1.2");//指定TLS版本
		} catch (NoSuchAlgorithmException e) {
			//SourceCode.getInstance().output("创建套接字失败！", e);
		}
		// 实现X509TrustManager接口，用于绕过验证
		X509TrustManager trustManager = new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
					String paramString) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
					String paramString) throws CertificateException {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
		try {
			sslContext.init(null, new TrustManager[] { trustManager }, null);//初始化sslContext对象
		} catch (KeyManagementException e) {
			//SourceCode.getInstance().output("初始化套接字失败！", e);
		}
		return sslContext;
	}

	private static TrustManager myX509TrustManager = new X509TrustManager() {

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}
	};
}
