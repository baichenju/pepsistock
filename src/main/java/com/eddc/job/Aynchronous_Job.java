/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年8月28日 下午2:18:08 
 */
package com.eddc.job;

import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.eddc.service.CrawlerPublicClassService;
import com.eddc.util.Fields;
import com.eddc.util.SpringContextUtil;

/**   
*    
* 项目名称：selection_pepsi_crawler   
* 类名称：Aynchronous_Job   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年8月28日 下午2:18:08   
* 修改人：jack.zhao   
* 修改时间：2018年8月28日 下午2:18:08   
* 修改备注：   
* @version    
*    
*/
@Component
@Configuration //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling // 2.开启定时任务
public class Aynchronous_Job {
	@Scheduled(cron="0 0 1 * * ?")
	public void aynchronousData() throws InterruptedException, ExecutionException{
		ApplicationContext contexts = SpringContextUtil.getApplicationContext();
		CrawlerPublicClassService publicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
		String satabases=ResourceBundle.getBundle("docker").getString("satabases");
		publicClassService.SynchronousDataAll(Fields.TABLE_CRAW_KEYWORDS_INF,satabases);
	} 
}
