package com.eddc.job;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Keywords_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Keywords_Job implements Job {
	private static Logger _log = Logger.getLogger(Keywords_Job.class);
	private String tableName=Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext(); 
	public Keywords_Job(){}
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		_log.info(jobName+"__关键词搜索 Job执行时间: " + new Date());
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		try {
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.GUOMEI);
			try {
				if(listjobName.size()>0){
					//crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
					crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
					crawlerKeywords(listjobName,time);  
				}else{
					_log.info("当前天猫用户"+listjobName.get(0).getUser_Id()+"不存在请打开爬虫状态！爬虫结束------"+new Date());	
				}
			} catch (Exception e) {
				_log.info("搜索关键词查询启动失败-----"+e.toString());
			} 
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}

	}
	public  void crawlerKeywords(List<QrtzCrawlerTable>listjobName,String time) throws UnsupportedEncodingException{
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);//开始解析数据 
		int sum=0;
		for (CrawKeywordsInfo accountInfo : list) {
			sum=accountInfo.getCrawling_status();  
			Keywords_DataCrawlTask dataCrawlTask = (Keywords_DataCrawlTask) contexts.getBean(Keywords_DataCrawlTask.class);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setType(Fields.STYPE_1); 
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			dataCrawlTask.setSetUrl(accountInfo.getCust_keyword_url());	
			dataCrawlTask.setStatus(sum);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setSearch_price(accountInfo.getSearch_price_range());
			dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
			taskExecutor.execute(dataCrawlTask);
		}
		taskExecutor.shutdown();  
		while(true){  
			if(((ExecutorService) taskExecutor).isTerminated()){
				_log.info("关键词搜索结束-----"+SimpleDate.SimpleDateTime().format(new Date()));
				break;  
			}	
		}  
	} 
}

