package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Suning_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Suning_Job  implements Job, Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger _log = Logger.getLogger(Suning_Job.class);
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;

	public Suning_Job(){};
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		_log.info(jobName+"__苏宁 Job执行时间: " + new Date());
		try {
			//jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			//crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.PLATFORM_SUNING_CN);	
			atartCrawlerSuning(listjobName,Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON);
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}
	}
	public void atartCrawlerSuning(List<QrtzCrawlerTable>listjobName,String tableName,String count) throws InterruptedException{
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
		crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);

		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		if(List_Code.size()>0){
			for(Craw_keywords_delivery_place delivery:List_Code){
				_log.info("当前商品抓取地区是----------===="+delivery.getDelivery_place_name()+"==========="+SimpleDate.SimpleDateFormatData().format(new Date()));
				if(listjobName.size()>0){
					_log.info("开始同步苏宁数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
					crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
					crawAndParseInfoAndPriceSuning(listjobName,time,tableName,count,delivery.getDelivery_place_code()); //开始抓取数据
					if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
						_log.info("开始抓取苏宁PC端价格数据当前用户是--------"+listjobName.get(0).getUser_Id()+"抓取时间------"+SimpleDate.SimpleDateFormatData().format(new Date()));
						CommodityPricesSuningData(listjobName,time,tableName);//获取商品价格
					}
				}else{
					_log.info("当前用户苏宁不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());
					return ;
				}
			}
		}else{
			if(listjobName.size()>0){
				_log.info("开始同步苏宁数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
				crawAndParseInfoAndPriceSuning(listjobName,time,tableName,count,null); //开始抓取数据
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					_log.info("开始抓取苏宁PC端价格数据当前用户是--------"+listjobName.get(0).getUser_Id()+"抓取时间------"+SimpleDate.SimpleDateFormatData().format(new Date()));
					CommodityPricesSuningData(listjobName,time,tableName);//获取商品价格
				}
			}else{
				_log.info("当前用户苏宁不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());
				return ;
			}
		}

	}

	//商品详情
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPriceSuning(List<QrtzCrawlerTable>listjobName,String time,String tableName,String count,String code) throws InterruptedException{
		int jj=0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,count,Fields.STATUS_COUNT);//开始解析数据 
		for(CrawKeywordsInfo accountInfo:list){ jj++;
		Map<String,Object>suningData=new HashMap<String,Object>();
		Suning_DataCrawlTask dataCrawlTask= contexts.getBean(Suning_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSuningData(suningData);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setCodeCookice(code);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setSetUrl(Fields.SUNING_MOBILE_URL+accountInfo.getCust_keyword_name()+"__20_021_0210101_0_5__999.html?callback=wapData"); 
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while(true){
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				_log.info("信息爬虫结束开始检查是否有漏抓数据-----"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawAndParseInfoAndPricefailure(time,listjobName,tableName,code);//检查是否有失败的商品
				break;
			}	

		} 	
	}

	///检查失败的item数据
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String time,List<QrtzCrawlerTable>listjobName,String tableName,String code) throws InterruptedException{
		int jj=0; int count=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		do{  
			ArrayList<Future> futureList = new ArrayList<Future>();
			CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),tableName,Fields.STATUS_COUNT);//开始解析数据 
			for(CrawKeywordsInfo accountInfo:list){ jj++;
			Map<String,Object>suningData=new HashMap<String,Object>();
			Suning_DataCrawlTask dataCrawlTask= contexts.getBean(Suning_DataCrawlTask.class);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTimeDate(time); 
			dataCrawlTask.setSuningData(suningData);
			dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
			dataCrawlTask.setType(Fields.STYPE_1); 
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
			dataCrawlTask.setCrawKeywordsInfo(accountInfo); 
			dataCrawlTask.setCodeCookice(code);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			dataCrawlTask.setSetUrl(Fields.SUNING_MOBILE_URL+accountInfo.getCust_keyword_name()+"__20_021_0210101_0_5__999.html?callback=wapData"); 
			Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			}
			//taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			jj=0;
			count++;
			Thread.sleep(5000); 
			if (count > 5) {
				Thread.sleep(5000); 
				return;
			}
		}while (true);
	}
	//商品价格PC端
	@SuppressWarnings("rawtypes")
	public void CommodityPricesSuningData(List<QrtzCrawlerTable>listjobName,String time,String tableName){
		_log.info("-----------------开始抓取商品街价格"+SimpleDate.SimpleDateFormatData().format(new Date()) +"-----------------------------------------------");
		int ii=0;
		long dataTime=System.currentTimeMillis();
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CommodityPrices>listPrice=crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),tableName,Fields.STATUS_COUNT);
		for(CommodityPrices accountInfo:listPrice){ ii++;	
		Map<String,Object>suningData=new HashMap<String,Object>();
		Suning_DataCrawlTask dataCrawlTask= contexts.getBean(Suning_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setSuningData(suningData);
		dataCrawlTask.setTimeDate(time);
		accountInfo.setBatch_time(time);
		dataCrawlTask.setSum(""+ii+"/"+listPrice.size()+"");
		accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
		dataCrawlTask.setCommodityPrices(accountInfo);
		dataCrawlTask.setType(Fields.STYPE_6); 
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setSetUrl(Fields.SUNING_PC_URL+accountInfo.getEgoodsId()+"_000000000"+accountInfo.getEgoodsId()+"_0000000000_20_021_0210501_315587_1000267_9264_12117_Z001___R9000361_0.24_0_0010118558.html?callback=pcData&_="+dataTime+"");
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while(true){
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				break;
			}
		}
		ii=0;
		try {// 递归补漏
			Thread.sleep(20000); 
			RecursionFailureGoods(time,listjobName,tableName);
		} catch (Exception e) {
			_log.info("递归补漏失败+==========="+e.getMessage()+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
	}
	/**
	 * 递归检查获取没有成功抓取商品的价格
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("rawtypes")
	public void RecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName,String tableName) throws InterruptedException{
		_log.info("-----------------开始抓取商品街价格"+SimpleDate.SimpleDateFormatData().format(new Date()) +"-----------------------------------------------");
		int ii=0;int count=0;
		long dataTime=System.currentTimeMillis();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		do{
			ArrayList<Future> futureList = new ArrayList<Future>();
			List<CommodityPrices>listPriceSuning=crawlerPublicClassService.RecursionFailureGoods(listjobName,listjobName.get(0).getDatabases(),tableName,Fields.STATUS_COUNT);
			if(listPriceSuning.size()>0){
				for(CommodityPrices accountInfo:listPriceSuning){ ii++;	
				Map<String,Object>suningData=new HashMap<String,Object>();
				Suning_DataCrawlTask dataCrawlTask=contexts.getBean(Suning_DataCrawlTask.class);
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSuningData(suningData);
				dataCrawlTask.setSum(""+ii+"/"+listPriceSuning.size()+"");
				accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
				dataCrawlTask.setCommodityPrices(accountInfo);
				dataCrawlTask.setType(Fields.STYPE_6);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setSetUrl(Fields.SUNING_PC_URL+accountInfo.getEgoodsId()+"_000000000"+accountInfo.getEgoodsId()+"_0000000000_20_021_0210501_315587_1000267_9264_12117_Z001___R9000361_0.24_0_0010118558.html?callback=pcData&_="+dataTime+"");
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				futureList.add(future);
				_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				}	
			}
			//taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			ii=0;
			count++;
			if (count > 8) { 
				Thread.sleep(5000); 
				_log.info("递归检查获取没有成功抓取商品的价格----第："+count+"次循环------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				return;
			} 
		}while (true);
	} 
}
