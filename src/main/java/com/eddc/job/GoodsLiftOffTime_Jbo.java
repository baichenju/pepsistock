/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年6月21日 下午2:18:37 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Price_PackageVO;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.GoodsLiftOffTimeTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;

/**   
 *    
 * 项目名称：selection_sibrary_crawler   
 * 类名称：GoodsLiftOffTime_Jbo   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年6月21日 下午2:18:37   
 * 修改人：jack.zhao   
 * 修改时间：2018年6月21日 下午2:18:37   
 * 修改备注：   
 * @version    
 *    
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class GoodsLiftOffTime_Jbo extends Amazon_Job implements  Job,Serializable { 
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(CrowdFunding_Job.class); 
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private JobAndTriggerService jobAndTriggerService;  
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		goodsLiftOffTimeData(jobName);

	}
	public void goodsLiftOffTimeData(String jobName){
		try {
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.GUOMEI);
			if(jobName.contains("ALL")){//上下架时间
				crawAndParseInfoAndGoodsLiftOffTime(listjobName);
			}else{
				crawCommentNumber(listjobName);//京东评论
			}

		} catch (Exception e) {
			logger.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndGoodsLiftOffTime (List<QrtzCrawlerTable>listjobName) throws InterruptedException{
		int jj=0; 
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		ApplicationContext contexts= SpringContextUtil.getApplicationContext();  
		crawlerPublicClassService.restoreData(listjobName.get(0).getDatabases(),listjobName.get(0).getDatabases());//去重
		List<Craw_goods_Info>list=crawlerPublicClassService.goodsLiftoffTimeData(listjobName,listjobName.get(0).getDatabases());//开始解析数据 
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		for(Craw_goods_Info accountInfo:list){ jj++;
		GoodsLiftOffTimeTask dataCrawlTask = (GoodsLiftOffTimeTask) contexts.getBean(GoodsLiftOffTimeTask.class);	
		Map<String,Object>goodsMap=new HashMap<String,Object>();
		accountInfo.setCust_account_id(listjobName.get(0).getUser_Id());
		dataCrawlTask.setDataType(accountInfo.getPlatform_name_en());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		accountInfo.setBatch_time(time);
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.COUNT_01); 
		dataCrawlTask.setCrawGoodsiInfo(accountInfo);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setPage(Fields.STATUS_COUNT_1);
		dataCrawlTask.setGoodsMap(goodsMap);
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
		Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		try {
			while(true){ 
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow();  
					break;
				}	
			}
		} catch (Exception e) {
			taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public void crawCommentNumber (List<QrtzCrawlerTable>listjobName) throws InterruptedException{
		int jj=0;  
		ApplicationContext contexts= SpringContextUtil.getApplicationContext();  
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		//crawlerPublicClassService.commentDeweighting(listjobName.get(0).getDatabases(),listjobName.get(0).getDatabases());//去重
		List<Price_PackageVO>comVo=new ArrayList<Price_PackageVO>();
		List<Craw_goods_Info>list=crawlerPublicClassService.getProductReviews(listjobName,listjobName.get(0).getDatabases());//开始解析数据 
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		int totalSize = list.size(); //总记录数  
		int pageSize = 30; //每页N条  
		int totalPage = totalSize/pageSize; //共N页  
		int totalPagesurplus = totalSize % pageSize; //共N页  
		for(int i=0;i<totalPage;i++){
			Price_PackageVO vo=new Price_PackageVO();
			StringBuilder  egoodId =new StringBuilder ();
			StringBuilder dataEgoodsId =new StringBuilder();
			StringBuilder keyword =new StringBuilder();
			for(int j=0;j<pageSize;j++){
				if(StringUtil.isEmpty(egoodId.toString())){
					egoodId.append(list.get(i*pageSize+j).getEgoodsId());
					keyword.append(list.get(i*pageSize+j).getCust_keyword_id());
					dataEgoodsId.append("'"+list.get(i*pageSize+j).getEgoodsId()+"'");
				}else{
					egoodId.append(","+list.get(i*pageSize+j).getEgoodsId());
					keyword.append(","+list.get(i*pageSize+j).getCust_keyword_id());
					dataEgoodsId.append(","+"'"+list.get(i*pageSize+j).getEgoodsId()+"'");
				}
			}
			vo.setDataEgoodsId(dataEgoodsId.toString());
			vo.setEgoodsId(egoodId.toString());
			vo.setKeyword(keyword.toString());
			comVo.add(vo); 
		}
		Price_PackageVO vos=new Price_PackageVO();
		StringBuilder  egoodIds =new StringBuilder ();
		StringBuilder dataEgoodsIds =new StringBuilder();
		StringBuilder keyword =new StringBuilder();
		for(int k=0;k<totalPagesurplus;k++){
			if(StringUtil.isEmpty(egoodIds.toString())){
				egoodIds.append(list.get(totalPage*pageSize+k).getEgoodsId());
				keyword.append(list.get(totalPage*pageSize+k).getCust_keyword_id());
				dataEgoodsIds.append("'"+list.get(totalPage*pageSize+k).getEgoodsId()+"'");
			}else{
				egoodIds.append(","+list.get(totalPage*pageSize+k).getEgoodsId());
				keyword.append(","+list.get(totalPage*pageSize+k).getCust_keyword_id());
				dataEgoodsIds.append(","+"'"+list.get(totalPage*pageSize+k).getEgoodsId()+"'");
			}	
		}
		if(totalPagesurplus>0){
			vos.setDataEgoodsId(dataEgoodsIds.toString());
			vos.setEgoodsId(egoodIds.toString());
			vos.setKeyword(keyword.toString());
			comVo.add(vos);	
		}
		for(Price_PackageVO vo :comVo){
			GoodsLiftOffTimeTask dataCrawlTask = (GoodsLiftOffTimeTask) contexts.getBean(GoodsLiftOffTimeTask.class);
			Map<String,Object>goodsMap=new HashMap<String,Object>();
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
			dataCrawlTask.setDataEgoodsId(vo.getDataEgoodsId());
			dataCrawlTask.setKeyworId(vo.getKeyword());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
			dataCrawlTask.setType(Fields.COUNT_2); 
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setGoodsMap(goodsMap);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
			dataCrawlTask.setSetUrl(Fields.JD_URL_COMMENT.replace("COMMENT",vo.getEgoodsId()));
			completion.submit(dataCrawlTask);
			//Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			//futureList.add(future);
			logger.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		try {
			while(true){ 
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow();  
					break;
				}	
			}
		} catch (Exception e) {
			taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
		}
	  }
}

