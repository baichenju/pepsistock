package com.eddc.job;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import com.eddc.method.YhdData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Yhd_Job extends CrowdFunding_Job implements Job, Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger _log = Logger.getLogger(Yhd_Job.class);
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private YhdData yhdData;
	private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts = Fields.STATUS_ON;
	public Yhd_Job(){};
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		int pageTop = 0;
		if (jobName.contains(Fields.SEARCH)) {
			tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
			counts = Fields.STATUS_OFF;
			pageTop = 1;
		}
		_log.info(jobName+"开始执行一号店------ Job执行时间: " + new Date());
		try {
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.PLATFORM_JD_CN);
			yhdData.atartCrawlerYhd(listjobName,tableName,counts,pageTop);
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());	
		}

	}
}
