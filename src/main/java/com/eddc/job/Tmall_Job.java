package com.eddc.job;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import com.eddc.method.TmallData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.redis.JedisService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;

@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Tmall_Job extends Taobao_Job implements Job, Serializable {
    private static Logger _log = Logger.getLogger(Tmall_Job.class);
    private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
    private String counts = Fields.STATUS_ON;
    @Autowired
    private JobAndTriggerService jobAndTriggerService;
    @Autowired
    private CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    private TmallData tmallData;
    public Tmall_Job() {
    }
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().toString();
        jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
        int pageTop = 0;
        _log.info(">>> [" + jobName + "] job start >>>");
        _log.info(">>> Invoke Tmall_Job execute method >>>");
        try {
            String Ip = publicClass.getLocalIP();
            List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.PLATFORM_TMALL_EN);
            if (listjobName.size() > 0) {
                if (jobName.contains(Fields.SEARCH)) {
                    tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
                    counts = Fields.STATUS_OFF;
                    pageTop = 1;
                }
                //crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
                tmallData.crawlerTmallPriceMonitoring(listjobName, tableName, counts, pageTop);//启动爬虫
            } else {
                _log.info(">>> Tmall_Job : listjobName is null,please check!");
            }
            _log.info(">>> [" + jobName + "] finish >>>");
        } catch (Exception e) {
            _log.error(">>> Tmall_Job execute fail,please check! >>>");
            _log.error(e.getMessage());
        }
    }

    
}
