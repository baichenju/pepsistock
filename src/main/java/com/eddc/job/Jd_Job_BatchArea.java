package com.eddc.job;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Jd_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.Validation;
import com.eddc.util.publicClass;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.context.ApplicationContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Jd_Job_BatchArea implements Job {
    private static Logger _log = Logger.getLogger(Jd_Job_BatchArea.class);
    private  ApplicationContext context = SpringContextUtil.getApplicationContext();
    private String message = "";
    int count = 0;
    public Jd_Job_BatchArea(){}
    //Dyson 北上广成都 武汉城市编码
    public static String[] dysonCityCodes = {"1_72_2799_0", "2_2813_51976_0", "19_1601_3633_0", "22_1930_50946_0", "17_1381_3583_0"};
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().toString();
        jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
        _log.info(jobName + "___________开始执行京东------ Job执行时间: " + new Date());
        try {
            for (String cityCode : dysonCityCodes) {
                crawlerJDPriceMonitoring(cityCode, jobName);
            }
        } catch (Exception e) {
            _log.info("获取JobAndTriggerService对象为空------" + new Date() + e.toString());
        }

    }

    public void crawlerJDPriceMonitoring(String cityCode, String jobName) throws InterruptedException {
        JobAndTriggerService jobAndTriggerService = (JobAndTriggerService) context.getBean(JobAndTriggerService.class);
        CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
        String Ip = publicClass.getLocalIP();
        List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.PLATFORM_JD_CN);
        String time = SimpleDate.SimpleDateFormatData().format(new Date());
        if (listjobName.size() > 0) {
        	_log.info(jobName + "------开始抓取京东数据当前用户是" + listjobName.get(0).getUser_Id() + "------" + new Date());
        	crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
        	crawAndParseInfoAndPriceJD(cityCode, listjobName, time, message);//启动爬虫
        	//CommodityPricesJDData(listjobName, time, message);//获取商品价格
        } else {
            _log.info("当前用户不存在！爬虫结束------" + new Date());
        }
    }

    //商品详情
    @SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPriceJD(String cityCode, List<QrtzCrawlerTable> listjobName, String time, String messageId) throws InterruptedException {
        _log.info("开始爬去京东数据-----" + SimpleDate.SimpleDateFormatData().format(new Date()));
        int jj = 0;
        ArrayList<Future> futureList = new ArrayList<Future>();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
        CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
        String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
        List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),listjobName.get(0).getPlatform(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据 
        for (CrawKeywordsInfo accountInfo : list) {
            jj++;
            Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) context.getBean(Jd_DataCrawlTask.class);
            dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
            dataCrawlTask.setTimeDate(time);
            dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
            dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
            dataCrawlTask.setType(Fields.STYPE_1);
            dataCrawlTask.setStorage(listjobName.get(0).getStorage());
            dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
            dataCrawlTask.setCrawKeywordsInfo(accountInfo);
            dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
            dataCrawlTask.setCookie(dataCookie);
            dataCrawlTask.setProxy(listjobName.get(0).getIP());
            dataCrawlTask.setCityCode(cityCode);
            if (Validation.isEmpty(dataCookie)) {
                dataCrawlTask.setSetUrl(Fields.JD_URL_APP + accountInfo.getCust_keyword_name() + ".html");
            } else {
                dataCrawlTask.setSetUrl(Fields.JD_URL_APP_INTERNATIONL + accountInfo.getCust_keyword_name());
            }
            Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
        }
        //taskExecutor.shutdown();
        while (true) {
            if (((ExecutorService) taskExecutor).isTerminated()) {
            	taskExecutor.shutdownNow();  
                _log.info("信息爬虫结束开始检查是否有漏抓数据-----" + SimpleDate.SimpleDateFormatData().format(new Date()));
                crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
                crawAndParseInfoAndPricefailure(cityCode, time, listjobName);//检查是否有失败的商品
                break;
            }

        }
    }

    ///检查失败的item数据
    @SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String cityCode, String time, List<QrtzCrawlerTable> listjobName) throws InterruptedException {
        int jj = 0;
        int count = 0;
        ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池
        CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
        String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
        do {
        	ArrayList<Future> futureList = new ArrayList<Future>();
            List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据
            for (CrawKeywordsInfo accountInfo : list) {
                jj++;
                Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) context.getBean(Jd_DataCrawlTask.class);
                dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
                dataCrawlTask.setTimeDate(time);
                dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
                dataCrawlTask.setType(Fields.STYPE_1);
                dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
                dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
                dataCrawlTask.setCrawKeywordsInfo(accountInfo);
                dataCrawlTask.setStorage(listjobName.get(0).getStorage());
                dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
                dataCrawlTask.setCookie(dataCookie);
                dataCrawlTask.setProxy(listjobName.get(0).getIP());
                dataCrawlTask.setCityCode(cityCode);
                if (Validation.isEmpty(dataCookie)) {
                    dataCrawlTask.setSetUrl(Fields.JD_URL_APP + accountInfo.getCust_keyword_name() + ".html");
                } else {
                    dataCrawlTask.setSetUrl(Fields.JD_URL_APP_INTERNATIONL + accountInfo.getCust_keyword_name());
                }
                Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
        		futureList.add(future);
            }
            //taskExecutor.shutdown();
            while (true) {
                if (((ExecutorService) taskExecutor).isTerminated()) {
                	 taskExecutor.shutdownNow();  
                	 crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
                	 crawAndParseInfoAndPricefailure(cityCode, time, listjobName);//检查是否有失败的商品
                    break;
                }
            }
            jj = 0;
            count++;
            Thread.sleep(5000);
            if (count > 5) {
                Thread.sleep(5000);
                return;
            }
        } while (true);
    }

    //商品价格PC端
    @SuppressWarnings("rawtypes")
	public void CommodityPricesJDData(List<QrtzCrawlerTable> listjobName, String time) {
        _log.info("-----------------开始抓取商品街价格" + SimpleDate.SimpleDateFormatData().format(new Date()) + "-----------------------------------------------");
        int ii = 0;
        CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
        ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
        List<CommodityPrices> listPriceJd = crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
        String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
        ArrayList<Future> futureList = new ArrayList<Future>();
        for (CommodityPrices accountInfo : listPriceJd) {
            ii++;
            Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) context.getBean(Jd_DataCrawlTask.class);
            accountInfo.setBatch_time(time);
            dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
            dataCrawlTask.setTimeDate(time);
            dataCrawlTask.setSum("" + ii + "/" + listPriceJd.size() + "");
            accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
            dataCrawlTask.setCommodityPricesJD(accountInfo);
            dataCrawlTask.setType(Fields.STYPE_6);
            dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
            dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
            dataCrawlTask.setCookie(dataCookie);
            dataCrawlTask.setStorage(listjobName.get(0).getStorage());
            dataCrawlTask.setProxy(listjobName.get(0).getIP());
            dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
            Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
    		futureList.add(future);
        }
      //  taskExecutor.shutdown();
        while (true) {
            if (taskExecutor.isTerminated()) {
                try {// 递归补漏
                	 taskExecutor.shutdownNow();  
                	 crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
                    RecursionFailureGoods(time, listjobName);
                } catch (Exception e) {
                    _log.info("递归补漏失败+===========" + e.getMessage() + "---------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
                }
                break;
            }
        }
        ii = 0;

    }

    /**
     * 递归检查获取没有成功抓取商品的价格
     *
     * @throws InterruptedException
     */
    @SuppressWarnings("rawtypes")
	public void RecursionFailureGoods(String time, List<QrtzCrawlerTable> listjobName) throws InterruptedException {
        _log.info("-----------------开始抓取商品街价格" + SimpleDate.SimpleDateFormatData().format(new Date()) + "-----------------------------------------------");
        int ii = 0;
        int count = 0;
        ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池
        CrawlerPublicClassService crawlerPublicClassService = (CrawlerPublicClassService) context.getBean(CrawlerPublicClassService.class);
        String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
        do {
            List<CommodityPrices> listPriceJd = crawlerPublicClassService.RecursionFailureGoods(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
            ArrayList<Future> futureList = new ArrayList<Future>();
            if (listPriceJd.size() > 0) {
                for (CommodityPrices accountInfo : listPriceJd) {
                    ii++;
                    Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) context.getBean(Jd_DataCrawlTask.class);
                    dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
                    dataCrawlTask.setTimeDate(time);
                    dataCrawlTask.setSum("" + ii + "/" + listPriceJd.size() + "");
                    accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
                    dataCrawlTask.setCommodityPricesJD(accountInfo);
                    dataCrawlTask.setType(Fields.STYPE_6);
                    dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
                    dataCrawlTask.setCookie(dataCookie);
                    dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
                    dataCrawlTask.setProxy(listjobName.get(0).getIP());
                    dataCrawlTask.setStorage(listjobName.get(0).getStorage());
                    dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
                    Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
            		futureList.add(future);
                }
            }
            //taskExecutor.shutdown();
            while (true) {
                if (taskExecutor.isTerminated()) {
                	 taskExecutor.shutdownNow();  
                	 crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
                    break;
                }
            }
            ii = 0;
            count++;
            if (count > 8) {
                Thread.sleep(5000);
                return;
            }
        } while (true);
    }
}
