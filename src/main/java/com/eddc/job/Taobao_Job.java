package com.eddc.job;
import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import com.eddc.method.TaobaoData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@SuppressWarnings("serial")
public class Taobao_Job extends Jd_Job implements Job, Serializable {
	private static Logger _log = Logger.getLogger(Taobao_Job.class);
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private TaobaoData taobaoData;
	private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts = Fields.STATUS_ON;
	public Taobao_Job() {
	} ;

	//开始调用定时任务
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getJobDetail().getKey().toString();
		jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
		int pageTop = 0;
		_log.info(">>> [" + jobName + "] job start >>>");
		_log.info(">>> Invoke Taobao_Job execute method >>>");
		try {
			String Ip = publicClass.getLocalIP();
			List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.PLATFORM_TAOBAO_CN);
			if (jobName.contains(Fields.SEARCH)) {
				tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY;
				counts = Fields.STATUS_OFF;
				pageTop = 1;
			}
			if (listjobName.size() > 0) {
				try {
					if (jobName.contains(Fields.SEARCH)) {
						crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
					}
				} catch (Exception e) {
					_log.info("Failed to start crawler>>>>>>>>>>>>>>>>>");
				}
				taobaoData.crawlerTaobaoPriceMonitoring(listjobName, tableName, counts, pageTop);//启动爬虫
			} else {
				_log.info(">>> Taobao_Job : listjobName is null,please check!");
			}
			_log.info(">>> [" + jobName + "] finish >>>");
		} catch (Exception e) {
			_log.error(">>> Taobao_Job execute fail,please check! >>>");
			_log.error(e.getMessage());
		}
	}


}
