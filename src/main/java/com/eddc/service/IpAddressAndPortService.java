package com.eddc.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.framework.TargetDataSource;
import com.eddc.mapper.IpAddressAndPortMapper;
import com.eddc.model.Cookies;
import com.eddc.model.Craw_IP_Monitoring;
import com.eddc.model.IpAddresPort;
import com.eddc.util.Fields;
import com.eddc.util.Validation;

@Service
public class IpAddressAndPortService {
	@Autowired
	private IpAddressAndPortMapper ipAddressAndPortMapper;
	private static Logger logger=Logger.getLogger(IpAddressAndPortService.class);
	@TargetDataSource
	public List<String> getValidIpAddressAndPort(String platform,String ipProxy) { 
		List<String> list = new ArrayList<String>();
		String  ipContent="";
		if(ipProxy.contains(Fields.LAOA)){
			ipContent = IpAddressAndPortLaoA(); 
		}else{
			if(platform.equalsIgnoreCase(Fields.PLATFORM_AMAZON) || platform.equalsIgnoreCase(Fields.PLATFORM_AMAZON_UN) ){//ymx
				platform="ymx";
			}
			ipContent = IpAddressAndPort(platform); 
		}
		if (!Validation.isEmpty(ipContent)) {
			String[] ipAdd = ipContent.split(":");
			list.add(ipAdd[0]);
			list.add(ipAdd[1]); 
			list.add(ipAdd[2]); 
			return list;  
		}else{ 
			return null;
		}	
	} 
	public String IpAddressAndPort(String platform){
		try {
			List<IpAddresPort> ipList =new ArrayList<IpAddresPort>();
			ipList = ipAddressAndPortMapper.queryForList(platform);
//			if(ipList.size()>0){
//				ipAddressAndPortMapper.IpUpdate(ipList.get(0).getId(),platform);	
//				return ipList.get(0).getIpAddress()+":"+ipList.get(0).getNumber();
//			}else{
//				ipAddressAndPortMapper.IpUpdate_status(platform);
//				return IpAddressAndPort(platform)+":"+ipList.get(0).getNumber();
//			}
			return IpAddressAndPort(platform)+":"+ipList.get(0).getNumber();
		} catch (Exception e) {
			logger.info("请求IP失败"+e.getMessage());
			return "210.22.86.58:8080"+":0";
		}
	}
	public String IpAddressAndPortLaoA(){
		List<Cookies> ipList =new ArrayList<Cookies>();
		try {
			ipList = ipAddressAndPortMapper.queryForListLaoA();
			logger.info(ipList+"-----------------");
		  } catch (Exception e) {
		} 
		return ipList.get(0).getIp()+":"+ipList.get(0).getCookie();
	}
	//监控代理IP可用数量
	public void Inset_Craw_IP_monitoring(List<String> list,Map <String,String>mapLog){
		Craw_IP_Monitoring  monitoring=new Craw_IP_Monitoring();
		monitoring.setCust_account_id(Integer.valueOf(mapLog.get("accountId")));
		monitoring.setIp_library(mapLog.get(Fields.IPPROXY));
		if(!Validation.isEmpty(list)){
			monitoring.setiP_number(Integer.valueOf(list.get(2)));
		}else{
			monitoring.setiP_number(0);
		}
		monitoring.setPlatform(mapLog.get("platform"));
		ipAddressAndPortMapper.Inset_Craw_IP_monitoring(monitoring);
	}
}
