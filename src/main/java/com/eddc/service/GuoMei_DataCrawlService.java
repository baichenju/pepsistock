package com.eddc.service;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.CommodityPrices;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.enterprise.support.utility.Validation;
/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：GuoMei_DataCrawlService   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2017年11月22日 上午10:36:33   
* 修改人：jack.zhao   
* 修改时间：2017年11月22日 上午10:36:33   
* 修改备注：   
* @version    
*    
*/
@Service
public class GuoMei_DataCrawlService {
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	private static Logger logger = LoggerFactory.getLogger(GuoMei_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	/**  
	* @Title: GuoMeiParseItemPage  
	* @Description: TODO(解析页面商品的详细信息)  
	* @param @return
	* @return Map<String,String>    返回类型  
	* @throws  
	*/  
	public Map<String,String>GuoMeiParseItemPage(String itemPage ,Map<String,String>message,String database){
		Map<String,String>map=new HashMap<String, String>();
		String pictureUrl="";//商品图片URL
		String goodsName = ""; // 商品名称
		String stock="";//库存数量
		String platform_sellername="";//店铺
		String delivery_place="";//交货地点
		String selller_location="";//销售地点
		String venderId="";
		String currentPrice="";
		String  shopid="";
		String pricepromotion="";
		String PROPRIETARY =Fields.PROPRIETARY;//判断自营非自营
		String postageFree=Fields.DONTPACK_MAIL; //是否包邮
		StringBuffer brff=new StringBuffer();
		String egoodsId[]=message.get("egoodsId").split("-");
		try {
			 Document item = Jsoup.parse(itemPage.toString());
			   if(item.toString().contains("自营")){
				   PROPRIETARY=Fields.YES_PROPRIETARY;
			   }
			   Document docs =Jsoup.parse(item.toString());
			   goodsName=docs.getElementsByClass("title_text").text();//商品名称
			   pictureUrl=docs.getElementsByClass("swiper-wrapper see_details").get(0).getElementsByClass("swiper-slide").get(0).getElementsByTag("img").attr("src").toString();
			   if(docs.toString().contains(Fields.DEONSTATUS)){
				   String onStatus=docs.getElementById("onStatus").attr("data-href").toString();
				   platform_sellername=StringHelper.nullToString(StringHelper.getResultByReg(onStatus,"&companyName=([^,]+)&companyType")).replaceAll("\"", "");
				   shopid=StringHelper.nullToString(StringHelper.getResultByReg(onStatus,"companyID=([^,]+)&pageNameIndex")).replaceAll("\"", "");
			   }else if(docs.toString().contains(Fields.SHOP_NAME_ELLISPIS)){
				   platform_sellername=docs.getElementsByClass("shop_name ellipsis").html();
				   shopid=StringHelper.nullToString(StringHelper.getResultByReg(docs.toString(),"\"id\":\"([^,\"]+)\"")).replaceAll("\"", ""); 
			   }else {
				   platform_sellername=Fields.GOME_PROPRIETARY;
			   }
			   //获取库存详情以及商品价格
			   message.put("coding", "UTF-8");
			   message.put("cookie", "uid=CjoWOFpoNZRjf8a8CqpxAg==; __clickidc=13335043916778900; __c_visitor=13335043916778900; atgregion=21010100%7C%E4%B8%8A%E6%B5%B7%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%84%E6%B5%A6%E5%8C%BA%E5%8D%8A%E6%B7%9E%E5%9B%AD%E8%B7%AF%E8%A1%97%E9%81%93%7C21010000%7C21000000%7C210101001; cartnum=0_0-1_0; _idusin=74848300764; isnew=638753565043.1516783413552; DSESSIONID=4f18f58a27e74aaca00746f4d13187c5; __xsptplusUT_194=1; s_ev13=%5B%5B'sem_baidu_pinpai_logo'%2C'1517191505812'%5D%5D; asid=eb3dbf0ef372aa5464e31031d2dfede8; compare=; proid120517atg=%5B%229140046570-1130051648%22%2C%229140046571-1130051656%22%2C%229140055865-1130523959%22%5D; s_cc=true; gpv_p6=%E5%95%86%E5%93%81; __xsptplus194=194.4.1517191494.1517191513.3%232%7Cbzclk.baidu.com%7C%7C%7C%25E5%259B%25BD%25E7%25BE%258E%25E5%259C%25A8%25E7%25BA%25BF%7C%23%23ptlmYlZBohRqjPG8GzU0qtcVxgTeHgbR%23; _smt_uid=5a683599.403fce14; _jzqco=%7C%7C%7C%7C%7C1.1631152474.1516778905172.1516839477430.1517191514390.1516839477430.1517191514390.0.0.0.15.15; awaken=true; itemAdvertisingN=true; http_referer=%2F%2Fu.m.gome.com.cn%2Fmy_store_shopper.html%3Fstoreid%3DA007%26guideNo%3D00125157%26r%3D1517191553; ufpd=6014f0f42484e911a6e9c449dc3daa04384977ac91f81785abfbfd84c9583a864368a01936a3160cd33fb312e7a09799fc9e207a6df52907fc9f5b4e569be1f3; s_ppv=-%2C40%2C16%2C2253; gpv_pn=no%20value; gpv_p22=no%20value; s_getNewRepeat=1517191562338-Repeat; s_sq=gome-prd%3D%2526pid%253Dhttps%25253A%25252F%25252Fsearch.gome.com.cn%25252Fsearch%25253Fquestion%25253Diphone8%252526searchType%25253Dgoods%2526oid%253Dhttps%25253A%25252F%25252Fitem.gome.com.cn%25252F9140046570-1130051648.html%25253Fintcmp%25253Dsearch-9000000700-1_1_1%2526ot%253DA; __gma=ffb8de7.1126275969063.1516778901361.1516839472664.1517191490369.4; __gmv=1126275969063.1516778901361; __gmz=ffb8de7|sem_baidu_pinpai_logo|-|sem|-|18KIAeGJC80*|-|1126275969063.1516778901361|dc-1|1517191490370; __gmb=ffb8de7.7.1126275969063|4.1517191490369; __gmc=ffb8de7; plasttime=1517191604; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; gm_sid=6f68wlp7u65ou4i0aouxupnjpyzcs6wxl3515171915183");
			   message.put("url", Fields.GUOMEI_APP_STOCK+egoodsId[0]+"&skuID="+egoodsId[1]+"&shopId=&shopType=&provinceId=21000000&cityId=21010000&districtId=21011100&townId=210111002&modelId=&stid=&mid=&isFirst=Y&isPresent=0&ajax=1&_=1517191614144");
			   message.put("urlRef",Fields.GUOMEI_APP_URL+message.get("egoodsId")+".html");
			   String stockMessage=httpClientService.getJsoupResultMessage(message);
			   if(Validation.isEmpty(stockMessage) ||  stockMessage.toString().contains(Fields.SERVICEBUSY) ){
				   stockMessage=httpClientService.GoodsProduct(message,database);
			   }
			   if(!Validation.isEmpty(stockMessage)){
				   String strs=stockMessage.substring(stockMessage.indexOf("skuPrice"), stockMessage.lastIndexOf("}")+1);
				   currentPrice=StringHelper.nullToString(StringHelper.getResultByReg(strs,"skuPrice\":([^,]+)")).replaceAll("\"", "");
				   stock=StringHelper.nullToString(StringHelper.getResultByReg(strs,"\"stockDesc\":([^,]+)")).replaceAll("\"", "");
				   stock=UnicodeMessage(stock);
				   String temp = stockMessage.substring(stockMessage.indexOf("freight"), stockMessage.lastIndexOf("\"shop")-1);
				   postageFree=StringHelper.nullToString(StringHelper.getResultByReg(temp,"\"price\":([^,]+)")).replaceAll("\"", "");
			       postageFree=UnicodeMessage(postageFree);
				   if(postageFree.contains(Fields.FREE_SHIPPING)){
					   postageFree=Fields.PACK_MAIL; //是否包邮  
				   }
				   String  rebate=stockMessage.substring(stockMessage.indexOf("rebate"), stockMessage.lastIndexOf("businessType")-2);
				   rebate="{\""+rebate+"}";
			       JSONObject jsonsq = JSONObject.parseObject(rebate);
			       pricepromotion=jsonsq.getJSONObject("rebate").getJSONObject("buy").getString("desc").toString();
			   }
			   
			 //促销
			   message.put("cookie", "uid=CjoWOFpoNZRjf8a8CqpxAg==; __clickidc=13335043916778900; __c_visitor=13335043916778900; atgregion=21010100%7C%E4%B8%8A%E6%B5%B7%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%84%E6%B5%A6%E5%8C%BA%E5%8D%8A%E6%B7%9E%E5%9B%AD%E8%B7%AF%E8%A1%97%E9%81%93%7C21010000%7C21000000%7C210101001; cartnum=0_0-1_0; _idusin=74848300764; isnew=638753565043.1516783413552; DSESSIONID=4f18f58a27e74aaca00746f4d13187c5; s_ev13=%5B%5B'sem_baidu_pinpai_logo'%2C'1517191505812'%5D%5D; asid=eb3dbf0ef372aa5464e31031d2dfede8; compare=; proid120517atg=%5B%229140046570-1130051648%22%2C%229140046571-1130051656%22%2C%229140055865-1130523959%22%5D; s_cc=true; __xsptplus194=194.4.1517191494.1517191513.3%232%7Cbzclk.baidu.com%7C%7C%7C%25E5%259B%25BD%25E7%25BE%258E%25E5%259C%25A8%25E7%25BA%25BF%7C%23%23ptlmYlZBohRqjPG8GzU0qtcVxgTeHgbR%23; _smt_uid=5a683599.403fce14; _jzqco=%7C%7C%7C%7C%7C1.1631152474.1516778905172.1516839477430.1517191514390.1516839477430.1517191514390.0.0.0.15.15; awaken=true; itemAdvertisingN=true; http_referer=%2F%2Fu.m.gome.com.cn%2Fmy_store_shopper.html%3Fstoreid%3DA007%26guideNo%3D00125157%26r%3D1517191553; ufpd=6014f0f42484e911a6e9c449dc3daa04384977ac91f81785abfbfd84c9583a864368a01936a3160cd33fb312e7a09799fc9e207a6df52907fc9f5b4e569be1f3; s_ppv=-%2C40%2C16%2C2253; s_getNewRepeat=1517191562338-Repeat; s_sq=gome-prd%3D%2526pid%253Dhttps%25253A%25252F%25252Fsearch.gome.com.cn%25252Fsearch%25253Fquestion%25253Diphone8%252526searchType%25253Dgoods%2526oid%253Dhttps%25253A%25252F%25252Fitem.gome.com.cn%25252F9140046570-1130051648.html%25253Fintcmp%25253Dsearch-9000000700-1_1_1%2526ot%253DA; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; __gma=ffb8de7.1126275969063.1516778901361.1517191490369.1517193747817.5; __gmv=1126275969063.1516778901361; __gmz=ffb8de7|search.gome.com.cn|-|referrer|-|-|-|1126275969063.1516778901361|dc-2|1517193747818; __gmb=ffb8de7.2.1126275969063|5.1517193747817; __gmc=ffb8de7; plasttime=1517193748; gm_sid=6f68wlp7u65ou4i0aouxupnjpyzcs6wxl3515171915183");
			   message.put("url", Fields.GUOMEI_APP_PROMOTION+egoodsId[0]+"&skuID="+egoodsId[1]+"&skuPrice="+currentPrice+"&shopId="+shopid+"&isShop=1&isBbc=Y&natural=1&istlp=0&_=1517205734093");
			   message.put("urlRef",Fields.GUOMEI_APP_URL+message.get("egoodsId")+".html");
			   String promotionMessage= httpClientService.getJsoupResultMessage(message);
			   //String promotionMessage=httpClientService.GoodsProduct(message);
			   if(Validation.isEmpty(promotionMessage) || promotionMessage.toString().contains(Fields.SERVICEBUSY)){
				   promotionMessage=httpClientService.GoodsProduct(message,database);//requestData
			   }
			   if(!Validation.isEmpty(promotionMessage)){
				   String data=promotionMessage.substring(promotionMessage.indexOf("{"), promotionMessage.lastIndexOf("}")+1);
				   JSONObject currPriceJson = JSONObject.parseObject(data);
				   if(currPriceJson.getJSONObject("ticket").toString().contains(Fields.ENTRYLIST)){
					   JSONArray jsons= currPriceJson.getJSONObject("ticket").getJSONArray("entrylist");
					   for(int i=0;i<jsons.size();i++){
						   JSONObject jsonPromotion =JSONObject.parseObject(jsons.toArray()[i].toString());
						   if(Validation.isEmpty(pricepromotion)){
							   pricepromotion=jsonPromotion.getString("desc");
						   }else{
							   pricepromotion+="&&"+jsonPromotion.getString("desc");
						   } 
					   }   
				   }
				  
			   } 
		} catch (Exception e) {
			e.printStackTrace();
          logger.info("解析国美店数据失败！------"+e.toString()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date() ));
		} 
		
		map.put("originalPrice", currentPrice);//原价
		map.put("currentPrice", currentPrice);//现价
		map.put("shopName", platform_sellername);//店铺名称
		map.put("region",selller_location);//卖家位置
		map.put("platform_shoptype", PROPRIETARY);//平台商店类型
		map.put("sellerName", platform_sellername);//卖家店铺名称
		map.put("picturl", pictureUrl);//图片url 
		map.put("goodsName", goodsName);//商品名称
		map.put("delivery_place", delivery_place);//交易地址
		map.put("inventory", stock);//商品库存
		map.put("transactNum", null);//月销
		map.put("postageFree",postageFree);//是不包邮
		map.put("subCatId", venderId);
		map.put("rateNum", null);//商品总评论数
		map.put("promotion",brff.toString());//促销
		map.put("skuId", message.get("egoodsId"));//商品sku
		map.put("promotion", pricepromotion);//促销
		map.put("goodsUrl",Fields.GUOMEI_PC_URL+message.get("egoodsId")+".html");
		map.put("channel",Fields.CLIENT_MOBILE);
		map.put("message", null);   
		map.put("deposit", null);
		map.put("coupons", null);
		map.put("sale_qty", null);
		map.put("reserve_num", null);
		map.put("shopid", shopid);//商品Id
		map.put("sellerId", null);
		return map;
	}
    /**
     * @throws SQLException   
    * @Title: dataCrawGuoMeiPriceP_PC  
    * @Description: TODO 获取失败的商品价格促销
    * @param     设定文件  
    * @return void    返回类型  
    * @throws  
    */  
	public Map<String,Object> dataCrawGuoMeiPriceP_PC(Map<String ,String>map,CommodityPrices priceMessage,String ip,String database) throws SQLException{
		String PcPromotion=""; 
		Map<String,Object>mapData=new HashMap<String, Object>();
		String egoodsId[]=priceMessage.getEgoodsId().split("-");
		map.put("url",Fields.GUOMEI_PC_PRICE+egoodsId[0]+"/"+egoodsId[1]+"/N/21010100/210101001/1/null/flag/item/allStores?callback=allStores&_=1517219148067");
		map.put("cookie", "__clickidc=13335043916778900; __c_visitor=13335043916778900; atgregion=21010100%7C%E4%B8%8A%E6%B5%B7%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%84%E6%B5%A6%E5%8C%BA%E5%8D%8A%E6%B7%9E%E5%9B%AD%E8%B7%AF%E8%A1%97%E9%81%93%7C21010000%7C21000000%7C210101001; cartnum=0_0-1_0; _idusin=74848300764; s_ev13=%5B%5B'sem_baidu_pinpai_logo'%2C'1517191505812'%5D%5D; asid=eb3dbf0ef372aa5464e31031d2dfede8; awaken=true; ufpd=6014f0f42484e911a6e9c449dc3daa04384977ac91f81785abfbfd84c9583a864368a01936a3160cd33fb312e7a09799fc9e207a6df52907fc9f5b4e569be1f3; gps_provinceid=21000000; gps_cityid=21010000; gps_districtid=21011100; gps_townid=210111002; uid=CjoWyFpu08CvfzNVD/QFAg==; DSESSIONID=33f98239abb346d692aa1ed9a34f1f23; __xsptplusUT_194=1; s_cc=true; gpv_p6=%E5%95%86%E5%93%81; gpv_pn=%E5%95%86%E5%93%81%3AApple%20iPhone%208%20Plus%2064G%20%E6%B7%B1%E7%A9%BA%E7%81%B0%20%E7%A7%BB%E5%8A%A8%E8%81%94%E9%80%9A%E7%94%B5%E4%BF%A14G%E6%89%8B%E6%9C%BA; gpv_p22=no%20value; s_getNewRepeat=1517219128437-Repeat; s_sq=%5B%5BB%5D%5D; __gma=ffb8de7.1126275969063.1516778901361.1517216504433.1517219119250.12; __gmv=1126275969063.1516778901361; __gmz=ffb8de7|www.gome.com.cn|-|referrer|-|18KIAeGJC80*|-|1126275969063.1516778901361|dc-1|1517219119250; __gmb=ffb8de7.3.1126275969063|12.1517219119250; __gmc=ffb8de7; _jzqco=%7C%7C%7C%7C%7C1.1631152474.1516778905172.1517219124486.1517219132799.1517219124486.1517219132799.0.0.0.24.24; __xsptplus194=194.8.1517219124.1517219133.2%232%7Cbzclk.baidu.com%7C%7C%7C%25E5%259B%25BD%25E7%25BE%258E%25E5%259C%25A8%25E7%25BA%25BF%7C%23%23MiK87sqDzMQ3-qXxv04bddkmPHd-Gu9R%23; _smt_uid=5a683599.403fce14; s_ppv=-%2C20%2C16%2C862; compare=; proid120517atg=%5B%229140046570-1130051649%22%2C%229140046569-1130051643%22%2C%22A0006333570-pop8010627081%22%2C%229140046571-1130051656%22%2C%229140055865-1130523959%22%5D");
		map.put("coding", "UTF-8");
		map.put("urlRef", Fields.GUOMEI_PC_URL+priceMessage.getEgoodsId()+".html");
		int count=0; String itemPrice=""; String originalPrice="";String currentPrice="";String priceDetails=""; 
		try {
			while (Validation.isEmpty(itemPrice)) {
				if (count > 8) {
					break;
				}
				itemPrice=httpClientService.getJsoupResultMessage(map);
				if(Validation.isEmpty(itemPrice) ||itemPrice.contains(Fields.ILLEAL)){
					 itemPrice=httpClientService.interfaceSwitch(ip, map,database);//请求数据	
				} 
			}
		} catch (Exception e) {
			logger.info("查询PC端价格请求数据失败"+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
		}

		if(!Validation.isEmpty(itemPrice) ||itemPrice.contains(Fields.ILLEAL)){//非法的外部请求!
			  currentPrice=StringHelper.nullToString(StringHelper.getResultByReg(itemPrice,"\"salePrice\":([^}]+)")).replaceAll("\"", "");
			  originalPrice=StringHelper.nullToString(StringHelper.getResultByReg(itemPrice,"\"lineation\":([^,]+)")).replaceAll("\"", "");
			  if(Validation.isEmpty(originalPrice)){
				  originalPrice=currentPrice;
			  }
		}
		//促销
		try {
			map.put("url", Fields.GUOMEI_PC_PROMOTION+egoodsId[0]+"/"+egoodsId[1]+"/null/21010000/flag/item/userStores?callback=userStores&_=1517220099422");
			priceDetails=httpClientService.getJsoupResultMessage(map);
			if(Validation.isEmpty(priceDetails) || priceDetails.toString().contains(Fields.ILLEAL)){
				priceDetails=httpClientService.GoodsProductDetailsData(map);
					
			} 
		} catch (Exception e) {
			logger.info("请求促销数据失败------"+e.toString()+""+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		if(!Validation.isEmpty(priceDetails) || priceDetails.toString().contains(Fields.ILLEAL)){
			String result=priceDetails.substring(priceDetails.indexOf("{"), priceDetails.lastIndexOf("}")+1);
			JSONObject message = JSONObject.parseObject(result);
			JSONArray promotionResult= message.getJSONObject("result").getJSONArray("promotionResult");
			if(promotionResult.size()>0){
				 for(int i=0;i<promotionResult.size();i++){
			   		 JSONObject jsonPromotion = JSONObject.parseObject(promotionResult.toArray()[i].toString()); 
			   			if(Validation.isEmpty(PcPromotion)){
				   			PcPromotion=Fields.FULL+jsonPromotion.getString("fullAmount")+Fields.REDUCTION_OF+jsonPromotion.getString("ticketAmount");
				   		}else{
				   			PcPromotion+="&&"+Fields.FULL+jsonPromotion.getString("fullAmount")+Fields.REDUCTION_OF+jsonPromotion.getString("ticketAmount"); 
			   		 }
			   	 } 
			 }else{  
				 if(message.getJSONObject("result").getString("rebate").equals("0.00")){
					 PcPromotion=""; 
				 }else{
					 PcPromotion=Fields.REBATE+message.getJSONObject("result").getString("rebate"); 
				 }
			 }
		}

		map.put("originalPrice", originalPrice);//原价
		map.put("currentPrice", currentPrice);//现价
		map.put("promotion", PcPromotion);
		map.put("channel",priceMessage.getChannel());
		map.put("goodsId", priceMessage.getGoodsid());
		map.put("keywordId", priceMessage.getCust_keyword_id());
		map.put("SKUid",priceMessage.getEgoodsId());
		mapData.putAll(map);
		Craw_goods_Price_Info  price=crawlerPublicClassService.commdityPricesData(priceMessage,mapData);//插入PC端价格
		mapData.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
		return mapData;
	}
	public static String UnicodeMessage(String unicode){
		String brand[]=unicode.split("\\\\u");
		StringBuffer st=new StringBuffer();
		int data=0;
		for(int i=0;i<brand.length;i++){
			if(i!=0){
				try {
					if(brand[i].length()==4){
						data = Integer.parseInt(brand[i], 16);
						st.append((char) data); 
					}else{
						String title=String.valueOf(brand[i].substring(4).toString());
						data=Integer.parseInt(brand[i].substring(0,4),16);
						st.append(title); 
						st.append((char) data); 

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		return st.toString();
		
	  }	 
}
