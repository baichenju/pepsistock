package com.eddc.service;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.enterprise.support.utility.Validation;
import com.github.pagehelper.util.StringUtil;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Yhd_DataCrawlService   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月22日 上午10:36:33   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月22日 上午10:36:33   
 * 修改备注：   
 * @version    
 *    
 */
@Service
public class Yhd_DataCrawlService {
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	private static Logger logger = LoggerFactory.getLogger(Yhd_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	/**  
	 * @Title: YhdParseItemPage  
	 * @Description: TODO(解析页面商品的详细信息)  
	 * @param @return
	 * @return Map<String,String>    返回类型  
	 * @throws  
	 */  
	public Map<String,String>YhdParseItemPage(String itemPage ,Map<String,String>message,String database){
		Map<String,String>map=new HashMap<String, String>();
		String pictureUrl="";//商品图片URL
		String goodsName = ""; // 商品名称
		String isYiHaoDian =Fields.YES_PROPRIETARY;//判断自营非自营
		String stock_url="";//库存url
		String  stockDetails="";
		String stock="";//库存数量
		String platform_sellername="";//店铺
		String provinceName="";//省
		String cityName="";//市
		String delivery_place="";//交货地点
		String selller_location="";//销售地点
		String venderId="";
		String StockStateName="";
		String originalPrice="";
		String currentPrice="";
		String  shopid="";
		String pricepromotion="";
		String url="";
		StringBuffer brff=new StringBuffer();
		long dateTime=System.currentTimeMillis();
		Document doc = Jsoup.parse(itemPage.toString());
		stock_url = StringHelper.getResultByReg(doc.toString(), "jd_item_stock\":\"(.+?)\",");
		venderId=StringHelper.getResultByReg(doc.toString(), "venderId:([^,]+)");
		venderId=StringHelper.getResultByReg(venderId,"([0-9]+)");
		shopid=StringHelper.getResultByReg(doc.toString(),"category:(.+?)\"]");
		shopid=shopid.replace("[","").replaceAll("\"", "");
		goodsName=StringHelper.getResultByReg(doc.toString(), "pName:([^,]+)").replaceAll("\"", "");//商品名称
		pictureUrl=StringHelper.getResultByReg(doc.toString(), "image:([^,]+)").replace("[", "");//商品url
		pictureUrl="//img11.360buyimg.com/n1/"+pictureUrl.replaceAll("\"", "").trim();
		if(StringUtil.isNotEmpty(message.get("cookie"))){ 
			String area=StringHelper.getResultByReg(message.get("cookie").toString(),"yhd_location=([^,]+);").split(";")[0].toString();
			url="http:"+stock_url+"&skuId="+message.get("egoodsId")+"&venderId="+venderId+"&cat="+shopid+"&fqsp=0&area="+area+"&buyNum=1&callback=loadStockCallBack&_="+dateTime+"";	 
		}else{
			url="http:"+stock_url+"&skuId="+message.get("egoodsId")+"&venderId="+venderId+"&cat="+shopid+"&fqsp=0&area=2_2817_51973_0&buyNum=1&callback=loadStockCallBack&_="+dateTime+"";
		}
		message.put("url",url); 
		message.put("coding", "GBK");
		message.put("urlRef",Fields.YHD_URL_APP+message.get("egoodsId")+".html");
		//获取库存 店铺 是否有货
		try { 
			stockDetails=httpClientService.interfaceSwitch(message.get("ip"), message,database);//请求数据
			//stockDetails=httpClientService.GoodsProduct(message);
			if(!Validation.isEmpty(stockDetails)){
				String temp = stockDetails.substring(stockDetails.indexOf("{"), stockDetails.lastIndexOf("}") + 1);
				JSONObject jsonObject = JSONObject.parseObject(temp);
				StockStateName=jsonObject.getJSONObject("stock").get("StockStateName").toString();//是否有货
				provinceName=jsonObject.getJSONObject("stock").getJSONObject("area").get("provinceName").toString();//省，直辖市
				cityName=jsonObject.getJSONObject("stock").getJSONObject("area").getString("cityName");//市
				originalPrice=jsonObject.getJSONObject("stock").getJSONObject("jdPrice").getString("m");//原价
				currentPrice=jsonObject.getJSONObject("stock").getJSONObject("jdPrice").getString("p");//现价
				delivery_place=provinceName+" "+cityName;//交货地点
				selller_location=provinceName;//销售地点
				if(Fields.SPOT.equals(StockStateName)){ 
					StockStateName=Fields.IN_STOCK;
				}else{
					StockStateName=Fields.IS_NOT_STOCK;
				}
				try{
					if(jsonObject.getJSONObject("stock").toString().contains("self_D")){
						platform_sellername=jsonObject.getJSONObject("stock").getJSONObject("self_D").get("vender").toString();//店铺	
					}else{
						platform_sellername=jsonObject.getJSONObject("stock").getJSONObject("D").get("vender").toString();//店铺
					}
				}catch (Exception e) { 
					platform_sellername=Fields.YES_PROPRIETARY; 
				}
			}
			if(!platform_sellername.contains(Fields.FLAGSHIP_STORE)){
				isYiHaoDian=Fields.PROPRIETARY;
			}
			if(message.get("promotion_status").toString().equals(Fields.STATUS_ON)){//1代表 抓取促销 0代表不抓取
				//促销请求
				message.put("url",Fields.APP_PROMOTION+message.get("egoodsId")+"&params.venderId="+venderId+"&params.area=2_2817_51973_0&params.channelId=1");//促销
				message.put("coding", "UTF-8");
				try {
					pricepromotion=httpClientService.interfaceSwitch(message.get("ip"), message,database);//请求数据
					if(Validation.isEmpty(pricepromotion)){
						pricepromotion=httpClientService.GoodsProductItem(message);
					}
				} catch (Exception e) {
					logger.info("一号店请求APP促销失败"+e.toString()+"====="+SimpleDate.SimpleDateFormatData().format(new Date()));
				} 
				logger.info(pricepromotion+"------APP促销---------------------");
				if(pricepromotion.toString().contains("content")){
					String[] str = pricepromotion.split("content");
					for(int i=1;i<str.length;i++){
						String name=StringHelper.getResultByReg("content"+str[i], "name\":\"(.+?)\",");
						if(i+1==str.length){ 
							brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\",")));	
						}else{
							brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\","))+"&&");
						}
					}
				}
			}
		} catch (Exception e) {
			logger.info("解析一号店数据失败！------"+e.toString()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date() ));

		}
		map.put("originalPrice", originalPrice);//原价
		map.put("currentPrice", currentPrice);//现价
		map.put("shopName", platform_sellername);//店铺名称
		map.put("region",selller_location);//卖家位置
		map.put("platform_shoptype", isYiHaoDian);//平台商店类型
		map.put("sellerName", platform_sellername);//卖家店铺名称
		map.put("picturl", pictureUrl);//图片url 
		map.put("goodsName", goodsName);//商品名称
		map.put("delivery_place", delivery_place);//交易地址
		map.put("inventory", stock);//商品库存
		map.put("postageFree",Fields.DONTPACK_MAIL);//是不包邮
		map.put("subCatId", venderId);
		map.put("inventory", StockStateName);
		map.put("promotion",brff.toString());//促销
		map.put("skuId", message.get("egoodsId"));//商品sku
		map.put("channel",Fields.CLIENT_MOBILE); 
		map.put("goodsUrl", Fields.YHD_URL_PC+message.get("egoodsId")+".html"); 
		map.put("shopid", shopid);//商品Id
		map.put("message", null);   
		map.put("deposit", null);
		map.put("coupons", null);
		map.put("sale_qty", null);
		map.put("reserve_num", null);
		map.put("rateNum", null);//商品总评论数
		map.put("sellerId", null);
		map.put("transactNum", null);//月销
		return map;
	}

	/**
	 * @param dataCrawyhdPromotion_PC
	 * @return 解析PC端商品促销
	 * @throws Exception 
	 */
	public String dataCrawyhdPromotion_PC(Map<String ,String>map,CrawKeywordsInfo priceMessage) throws Exception{
		String priceDetails=""; 
		StringBuffer brff=new StringBuffer();
		map.put("cookie", "__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508130181.1508134896.3; __jdb=81617359.4.15081208192201111174638|3.1508134896; __jdc=81617359");
		map.put("urlRef", Fields.YHD_URL_PC+priceMessage.getCust_keyword_name()+".html");
		map.put("coding", "UTF-8");
		map.put("url",Fields.PROMOTION+priceMessage.getCust_keyword_name()+"&params.venderId="+map.get("subCatId")+"&params.area=2_2817_51973_0");//促销
		try {
			priceDetails=httpClientService.requestData(map);
			if(Validation.isEmpty(priceDetails)){
				priceDetails=httpClientService.GoodsProductDetailsData(map);	
			} 
		} catch (Exception e) {
			logger.info("请求促销数据失败------"+e.toString()+""+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		if(!Validation.isEmpty(priceDetails)){
			String[] str = priceDetails.split("content");
			for(int i=1;i<str.length;i++){
				String name=StringHelper.getResultByReg("content"+str[i], "name\":\"(.+?)\",");
				if(i+1==str.length){ 
					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\",")));	
				}else{
					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\","))+"&&");
				}
			}	
		}
		return brff.toString(); 
	}
	/**
	 * @throws SQLException   
	 * @Title: dataCrawYHDPriceP_PC  
	 * @Description: TODO 获取失败的商品价格促销
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	public Map<String,Object>  dataCrawYHDPriceP_PC(Map<String ,String>map,CommodityPrices priceMessage) throws SQLException{
		StringBuffer brff=new StringBuffer();
		Map<String,Object>mapData=new HashMap<String, Object>();
		map.put("url", Fields.EXTRAPARAM+priceMessage.getEgoodsId()+"&_=1511318259452");
		map.put("cookie", "__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz001_0_2c4cce5564014487a6fb235ff6a6ebde|1513314731212; cart_cookie_uuid=9b3f1f5a-df44-4898-b679-1d3cc3e83bd5; guid=18XT5KNVVTJYECAJR7HVU6W539FR9DE2ZDY6; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_sid=15136502255078814372853512444.3; mba_muid=1513314731211228338240; test=1; unpl=V2_ZzNtbUdVQRx8WkBULxkPBWJWQF8RXhRGIQpHUnpODAVkUxpfclRCFXMURldnGVgUZwcZXENcRhFFCHZXchBYAWcCGllyBBNNIEwHDCRSBUE3XHwNRAAXEHYIQAEvSg4ANAISVUtVRBB2WBRcKxpfUGcLR21yVEMldQl2VHoZVQFmBxRZQWdzEkU4dlR6EVgGYzMTbUNnAUEpDUFXch5eSGcCElRGVkcTcQt2VUsa; cart_num=0; __jda=81617359.1513314731211228338240.1513314731.1513590915.1513650225.8; __jdb=81617359.4.1513314731211228338240|8.1513650225; __jdc=81617359");
		map.put("coding", "UTF-8");
		map.put("urlRef", Fields.YHD_URL_PC+priceMessage.getEgoodsId()+".html");
		int count=0; String itemPrice=""; String originalPrice="";String currentPrice="";String priceDetails=""; 
		try {
			while (Validation.isEmpty(itemPrice)) {
				if (count > 8) {
					break;
				}
				itemPrice=httpClientService.requestData(map);
				if(Validation.isEmpty(itemPrice)){
					itemPrice=httpClientService.GoodsProductDetailsData(map);
				} 
			}
		} catch (Exception e) {
			logger.info("查询PC端价格请求数据失败"+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
		}

		if(!Validation.isEmpty(itemPrice)){
			String temp = itemPrice.substring(itemPrice.indexOf("{"), itemPrice.lastIndexOf("}") + 1);	
			JSONObject jsonObject = JSONObject.parseObject(temp);
			com.alibaba.fastjson.JSONArray jsonList = jsonObject.getJSONArray("data");
			originalPrice=JSONObject.parseObject(jsonList.toArray()[0].toString()).get("m").toString();
			currentPrice=JSONObject.parseObject(jsonList.toArray()[0].toString()).get("p").toString();
		}

		map.put("url",Fields.PROMOTION+priceMessage.getEgoodsId()+"&params.venderId="+priceMessage.getPlatform_category()+"&params.area=2_2817_51973_0");//促销
		try {
			priceDetails=httpClientService.requestData(map);
			if(Validation.isEmpty(priceDetails)){
				priceDetails=httpClientService.GoodsProductDetailsData(map);	
			} 
		} catch (Exception e) {
			logger.info("请求促销数据失败------"+e.toString()+""+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		if(!Validation.isEmpty(priceDetails)){
			String[] str = priceDetails.split("content");
			for(int i=1;i<str.length;i++){
				String name=StringHelper.getResultByReg("content"+str[i], "name\":\"(.+?)\",");
				if(i+1==str.length){ 
					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\",")));	
				}else{
					brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\","))+"&&");
				}
			}	
		}
		if(StringUtil.isNotEmpty(currentPrice)){
			map.put("originalPrice", originalPrice);//原价
			map.put("currentPrice", currentPrice);//现价
			map.put("promotion", brff.toString());
			map.put("channel",priceMessage.getChannel());
			map.put("goodsId", priceMessage.getGoodsid());
			map.put("keywordId", priceMessage.getCust_keyword_id());
			map.put("SKUid",priceMessage.getEgoodsId());
			mapData.putAll(map);
			crawlerPublicClassService.commdityPricesData(priceMessage,mapData);//插入PC端价格
		}
		return mapData;
	}

	/**  
	 * @Title: Yhd_DataCrawlPrice_APP  
	 * @Description: TODO(解析商品的App端价格)  
	 * @param @param map
	 * @param @param commodityPrices    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	public void dataCrawYHDPriceP_App(Map<String ,String>map,CommodityPrices priceMessage) throws Exception{
		String priceDetails=""; String price="";
		Map<String,Object>mapData=new HashMap<String, Object>();
		StringBuffer brff=new StringBuffer();
		map.put("urlRef", Fields.YHD_URL_APP+priceMessage.getEgoodsId()+".html");
		map.put("cookie", "__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508120819.1508130181.2; __jdb=81617359.21.15081208192201111174638|2.1508130181; __jdc=81617359; mba_muid=15081208192201111174638; mba_sid=1508131376821626930059243537.2; test=1");
		try {
			priceDetails=httpClientService.requestData(map);
			if(Validation.isEmpty(priceDetails)){
				priceDetails=httpClientService.GoodsProductDetailsData(map);
			}
		} catch (Exception e) { 
			logger.info("一号店请求APP价格失败"+e.toString()+"====="+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		try {
			if(!Validation.isEmpty(priceDetails)){
				String data_price=priceDetails.substring(priceDetails.indexOf("{"),priceDetails.lastIndexOf("}") + 1);
				JSONObject jsonObject = JSONObject.parseObject(data_price);
				com.alibaba.fastjson.JSONArray jsonList = jsonObject.getJSONArray("data");
				price=JSONObject.parseObject(jsonList.toArray()[0].toString()).get("p").toString();
				map.put("url",Fields.APP_PROMOTION+priceMessage.getEgoodsId()+"&params.venderId="+priceMessage.getPlatform_category()+"&params.area=2_2817_51973_0&params.channelId=1");//促销
				try {
					priceDetails=httpClientService.requestData(map);
					if(Validation.isEmpty(priceDetails)){
						priceDetails=httpClientService.requestData(map);
					}
				} catch (Exception e) {
					logger.info("一号店请求APP促销失败"+e.toString()+"====="+SimpleDate.SimpleDateFormatData().format(new Date()));
				}
				String[] str = priceDetails.split("content");
				for(int i=1;i<str.length;i++){
					String name=StringHelper.getResultByReg("content"+str[i], "name\":\"(.+?)\",");
					if(i+1==str.length){ 
						brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\",")));	
					}else{
						brff.append(name+":"+(StringHelper.getResultByReg("content"+str[i], "content\":\"(.+?)\","))+"&&");
					}

				} 
			}
			map.put("promotion", brff.toString());
			map.put("originalPrice", price);
			map.put("currentPrice", price);
			map.put("channel",Fields.CLIENT_MOBILE);
			map.put("goodsId", priceMessage.getGoodsid());
			map.put("keywordId", priceMessage.getCust_keyword_id());
			map.put("SKUid",priceMessage.getEgoodsId());
			mapData.putAll(map);
			crawlerPublicClassService.commdityPricesData(priceMessage,mapData);//插入APP端价格
		} catch (Exception e) {
			logger.info("一号店APP端价格解析数据失败"+e.toString());
		}
	}
}
