/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.service 
 * @author: jack.zhao   
 * @date: 2018年5月25日 下午4:15:31 
 */
package com.eddc.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eddc.framework.TargetDataSource;
import com.eddc.job.Taobao_Job;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_InfoVO;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.model.Craw_goods_bsr_Info;
import com.eddc.model.Craw_goods_crowdfunding_price_Info;
import com.eddc.service.BatchFileUploadService.ExecupteHp;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.google.common.collect.Lists;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：BatchExecutionCrawlService   
 * 类描述： 多线程批量插入 
 * 创建人：jack.zhao   
 * 创建时间：2018年5月25日 下午4:15:31   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月25日 下午4:15:31   
 * 修改备注：   
 * @version    
 *    
 */
@Service
public class BatchExecutionCrawlService extends Thread{
	@Autowired
	BatchInsertData batchInsertData;
	private static Logger log = Logger.getLogger(BatchExecutionCrawlService.class); 
	@SuppressWarnings({ "rawtypes"})
	@TargetDataSource
	public void insertCrawlData(ArrayList<Future> futureList,String database){
		ExecutorService taskExecutor = Executors.newFixedThreadPool(1);
		ExecupteHp hpRunnable = new ExecupteHp(futureList,database);
		taskExecutor.execute(hpRunnable);
		taskExecutor.shutdown();
	}
	class ExecupteHp implements Runnable{
		@SuppressWarnings("rawtypes")
		private ArrayList<Future> futureList;
		private String database;
		@SuppressWarnings("rawtypes")
		public ExecupteHp(ArrayList<Future> futureList,String database) {
			this.futureList=futureList;
			this.database=database;
		}
		@SuppressWarnings("unchecked")
		public void run() {
			Future<Map<String, Object>> futures=null;
			String tableName=""; String tableNamePirce=""; String tableName_info=""; String bsr="";
			List<Map<String,Object>> insert = Lists.newArrayList();
			List<Map<String,Object>> insertprice = Lists.newArrayList();
			List<Map<String,Object>> insert_info = Lists.newArrayList();
			List<Map<String,Object>> insert_infoBsr = Lists.newArrayList();
			if(null!=futureList && futureList.size()>0){
				for(int i = 0;i<futureList.size();i++){
					futures=futureList.get(i); 
					Map<String,Object> insertItem=new HashMap<String,Object>();
					Map<String,Object> insertItemData=new HashMap<String,Object>();
					Map<String,Object> insertItemDataBsr=new HashMap<String,Object>();
					try {
						String dataMessage=futures.get().toString();
						if(dataMessage.contains(Fields.TABLE_CRAW_GOODS_INFO)){
							Craw_goods_InfoVO info=(Craw_goods_InfoVO) futures.get().get(Fields.TABLE_CRAW_GOODS_INFO);	
							tableName_info= Fields.TABLE_CRAW_GOODS_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insert_info.add(insertItem);
						}
						if(dataMessage.contains(Fields.TABLE_CRAW_GOODS_PRICE_INFO)){
							Craw_goods_Price_Info price=(Craw_goods_Price_Info) futures.get().get(Fields.TABLE_CRAW_GOODS_PRICE_INFO);
							tableNamePirce=Fields.TABLE_CRAW_GOODS_PRICE_INFO;
							insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(price);
							insertprice.add(insertItemData);
						}
						//处理tmall商品价格
						if(dataMessage.contains("goods_info_price")){
							List<Craw_goods_Price_Info>list=(List<Craw_goods_Price_Info>)futures.get().get("goods_info_price");
							tableNamePirce=Fields.TABLE_CRAW_GOODS_PRICE_INFO;
							for(int is=0;is<list.size();is++){
								Craw_goods_Price_Info info=list.get(is);
								if(futures.get().get("client").toString().equalsIgnoreCase("ALL")){
									info.setChannel(Fields.CLIENT_MOBILE);
									insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
									insertprice.add(insertItemData);
									info.setChannel(Fields.CLIENT_PC);
									insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
									insertprice.add(insertItemData);
								}else if(futures.get().get("client").toString().equalsIgnoreCase("MOBILE")){
									info.setChannel(Fields.CLIENT_MOBILE);
									insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
									insertprice.add(insertItemData);
								}	
							}
						}

						if(dataMessage.contains(Fields.TABLE_CRAW_VENDOR_INFO)){
							Craw_goods_Vendor_InfoVO info=(Craw_goods_Vendor_InfoVO) futures.get().get(Fields.TABLE_CRAW_VENDOR_INFO);
							tableName=Fields.TABLE_CRAW_VENDOR_INFO;
							insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insert.add(insertItemData);
						}
						if(dataMessage.contains("Vendor_priceList")){//1688价格
							List<Craw_goods_Vendor_price_Info>list=(List<Craw_goods_Vendor_price_Info>)futures.get().get("Vendor_priceList");
							tableNamePirce=Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
							for(int is=0;is<list.size();is++){
								Craw_goods_Vendor_price_Info info=list.get(is);
								insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insertprice.add(insertItemData);
							}
						}

						if(dataMessage.contains("list_taobaozc")){
							List<Craw_goods_crowdfunding_price_Info>list=(List<Craw_goods_crowdfunding_price_Info>) futures.get().get("list_taobaozc");
							List<Craw_goods_crowdfunding_price_Info>listjd=(List<Craw_goods_crowdfunding_price_Info>) futures.get().get("list_jdzc");
							tableName=Fields.TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO;
							for(int is=0;is<list.size();is++){
								Craw_goods_crowdfunding_price_Info info=list.get(is);
								insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insert.add(insertItemData);
							}
							for(int iss=0;iss<listjd.size();iss++){
								Craw_goods_crowdfunding_price_Info info=listjd.get(iss);
								insertItemData = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								insert.add(insertItemData);
							}
						}

						if(dataMessage.contains(Fields.CRAW_GOODS_BSR_INFO)){//评论数
							Craw_goods_bsr_Info info=(Craw_goods_bsr_Info) futures.get().get(Fields.CRAW_GOODS_BSR_INFO);
							bsr=Fields.CRAW_GOODS_BSR_INFO;
							insertItemDataBsr = BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insert_infoBsr.add(insertItemDataBsr);
						}
						

					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				log.info(">>> Start do real insert >>>");
				
				try {
					if(insert_info.size()>0){
						batchInsertData.insertIntoData(insert_info,database,tableName_info);
						if(insert.size()>0){
							batchInsertData.insertIntoData(insert,database,tableName);
						}
						if(insertprice.size()>0){
							batchInsertData.insertIntoData(insertprice,database,tableNamePirce);
						}
						if(insert_infoBsr.size()>0){
							batchInsertData.insertIntoData(insert_infoBsr,database,tableNamePirce);
						}
					}
				} catch (Exception e) {
						e.printStackTrace();
				}
				log.info(">>> Finish do real insert >>>");
			}else{
				log.info("insertCrawlData data list is empty,please check!");
			}
		}
	}
}
