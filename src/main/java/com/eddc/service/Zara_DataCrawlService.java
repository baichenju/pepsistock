package com.eddc.service;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.craw_customerweb_goods_info;
@Service
public class Zara_DataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	private static Logger logger = LoggerFactory.getLogger(Zara_DataCrawlService.class);  
	/**
	 * @throws InterruptedException 
	 * @throws UnsupportedEncodingException   
	 * @Title: zaraParseItemPage  
	 * @Description: 解析页面详情
	 * @param @param item
	 * @param @param message
	 * @param @return    设定文件  
	 * @return Map<String,String>    返回类型  
	 * @throws  
	 */  
	public void zaraParseItemPage(String item ,Map<String,String>message) throws UnsupportedEncodingException, InterruptedException{
		List<craw_customerweb_goods_info>list1=new ArrayList<craw_customerweb_goods_info>();
		List<craw_customerweb_goods_info>list2=new ArrayList<craw_customerweb_goods_info>();
		List<craw_customerweb_goods_info>list3=new ArrayList<craw_customerweb_goods_info>();
		List<craw_customerweb_goods_info>list4=new ArrayList<craw_customerweb_goods_info>();
		List<craw_customerweb_goods_info>list5=new ArrayList<craw_customerweb_goods_info>();
		List<craw_customerweb_goods_info>list6=new ArrayList<craw_customerweb_goods_info>();
		String price="";
		String temp = item.substring(item.indexOf("{"), item.lastIndexOf("}") + 1);
		JSONObject jsonObject = JSONObject.fromObject(temp);
		JSONArray jsonList = jsonObject.getJSONArray("products");
		for (int i = 0; i < jsonList.size(); i++) {
			craw_customerweb_goods_info info=new craw_customerweb_goods_info();
			JSONObject itemJson = JSONObject.fromObject(jsonList.toArray()[i].toString());
			String egoodsId=itemJson.get("id").toString();
			try {
			  price=itemJson.getString("price").replaceAll("0", "").trim();
			} catch (Exception e) {
				 price="";
				logger.info("价格抓取失败"+e.toString() +message.get("url"));
			 
			}
			JSONArray  json = itemJson.getJSONArray("xmedia");
			String image=JSONObject.fromObject(json.toArray()[json.size()-1]).get("path").toString();
			String imageName=JSONObject.fromObject(json.toArray()[json.size()-1]).get("name").toString();
			image="http://static.zara-static.cn/photos//"+image+"/w/400/"+imageName+".jpg";
			String goods_name=itemJson.getJSONObject("detail").getString("name");
			String model=itemJson.getJSONObject("detail").getJSONObject("reference").getString("model").toString();
			String quality=itemJson.getJSONObject("detail").getJSONObject("reference").getString("quality").toString();
			String goods_url="https://www.zara.cn/cn/zh/"+URLEncoder.encode(goods_name, "UTF-8")+"-p0"+model+quality+".html?v1="+egoodsId+"";
			info.setEgoods_id(egoodsId);
			info.setCategories(message.get("categories").toString());
			info.setSeries(message.get("series").toString());
			info.setGoods_name(goods_name);
			info.setGoods_pic_url(image);
			info.setGoods_url(goods_url);
			info.setGoods_price(price);
			info.setUserid(Integer.valueOf(message.get("accountId")));
			if (100>=i || i<=200) {
				list2.add(info);
				info=null; 
			}else if(201>i || i<=400){
				list3.add(info);
				info=null;
			}else if(401>i || i<=600) {
				list1.add(info);
				info=null;
			}else if(601>i || i<=800){
				list4.add(info);
				info=null;
			}else if(801>i || i<=1000){
				list5.add(info);
				info=null; 
			}else {
				list6.add(info);
				info=null;
			}
			 
		} 
		logger.info("插入商品个数："+list2.size());
		logger.info("插入商品个数："+list3.size());
		logger.info("插入商品个数："+list1.size());
		logger.info("插入商品个数："+list4.size());
		logger.info("插入商品个数："+list5.size());
		logger.info("插入商品个数："+list6.size());
		if(list2.size()>0){
		logger.info("插入商品个数："+list2.size());
		crawlerPublicClassMapper.AaraInsertdetails(list2);	
		}
		Thread.sleep(1000);
		if(list3.size()>0){
		logger.info("插入商品个数："+list3.size());
		crawlerPublicClassMapper.AaraInsertdetails(list3); 
		}
		Thread.sleep(1000);
		if(list1.size()>0){
		logger.info("插入商品个数："+list1.size());
		crawlerPublicClassMapper.AaraInsertdetails(list1); 
		}
		Thread.sleep(1000);
		if(list4.size()>0){
		logger.info("插入商品个数："+list4.size());
		crawlerPublicClassMapper.AaraInsertdetails(list4); 
		}
		Thread.sleep(1000);
		if(list5.size()>0){
		logger.info("插入商品个数："+list5.size());
		crawlerPublicClassMapper.AaraInsertdetails(list5); 
		}
		Thread.sleep(1000);
		if(list6.size()>0){
		logger.info("插入商品个数："+list6.size());
		crawlerPublicClassMapper.AaraInsertdetails(list6); 
		} 
	}
	//同步数据
	public void SynchronousData(String userId){
		crawlerPublicClassMapper.Zara_synchronousData(Integer.valueOf(userId));
	}
}
