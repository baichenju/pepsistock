package com.eddc.service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_Fixed_Info;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_bsr_Info;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.TransApi;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service
public class Amazon_DataCrawlService {
	private static Logger logger=Logger.getLogger(Amazon_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	BatchInsertData batchInsertData;
	public Map<String, Object> amazonParseItemPage(String itemPage,Map<String,String>mapType,String database,String storage,CrawKeywordsInfo keywordInfo) throws Exception{
		Map<String, String> map=new HashMap<String, String>();
		Map<String, Object> mapData=new HashMap<String, Object>();
		String shopName="";String goodsName=""; String platform_shoptype="";String promotion="";
		String Stock="";String shopId=""; String Price="";String originalPrice="";
		String delivery_place="";String dontpackMall="";String image=""; String rateNum="";
		String bsr=""; String bsr_rank=""; String bsr_category=""; int comment_status=0;
		if(!Validation.isEmpty(itemPage)){
			Document docs =Jsoup.parse(itemPage.toString());
			if(docs.toString().contains("btAsinTitle")){
				goodsName=docs.getElementById("btAsinTitle").text().replace("'", "`");    
			}else if(docs.toString().contains("ebooksProductTitle")){
				goodsName=docs.getElementById("ebooksProductTitle").text().replace("'", "`");
			}else if(docs.toString().contains("productTitle")){
				try {
					goodsName=docs.getElementById("productTitle").text().replace("'", "`");
					if(goodsName.length()>190){
						goodsName=goodsName.substring(0,190)+"....";
					}
				} catch (Exception e) {
				}
			}else if(StringUtil.isEmpty(goodsName)){
				goodsName=docs.getElementsByClass("a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal").attr("title").toString();  
			} 
			if(StringUtil.isNotEmpty(docs.getElementsByClass("a-span12 a-color-secondary a-size-base").toString())){
				originalPrice=docs.getElementsByClass("a-span12 a-color-secondary a-size-base").text().replace("￥","").replace("$","");   
			}
			if(docs.toString().contains("priceblock_ourprice")){
				try {
					Price=docs.getElementById("priceblock_ourprice").text().replace("￥","").replace("$","");
					if(StringUtil.isEmpty(originalPrice)){
						originalPrice=Price; 
					}	
				} catch (Exception e) {
				}
			} 
			if(StringUtil.isNotEmpty(docs.getElementsByClass("a-size-base a-color-price a-color-price").toString())){
				Price=docs.getElementsByClass("a-size-base a-color-price a-color-price").text().replace("￥","").replace("$",""); 
				originalPrice=Price;
			}
			if(docs.toString().contains("ddmSelectedAddressText")){
				delivery_place=docs.getElementById("ddmSelectedAddressText").text();  
			}
			if(docs.toString().contains("price-shipping-message")){
				if(StringUtil.isNotEmpty(docs.getElementById("price-shipping-message").text().toString())){
					dontpackMall=docs.getElementById("price-shipping-message").text();
					if(dontpackMall.toString().contains(Fields.FULL)){
						dontpackMall=Fields.DONTPACK_MAIL;
					}else{
						dontpackMall=Fields.PACK_MAIL;
					}
				} 
			}                                                
			if(StringUtil.isNotEmpty(docs.getElementsByClass("a-color-success ddm-font-size-15").text().toString())){
				Stock=docs.getElementsByClass("a-color-success ddm-font-size-15").text();
				Pattern p = Pattern.compile(".*\\d+.*");
				Matcher m = p.matcher(Stock);
				if (m.matches()) {
					String regEx="[^0-9]"; 
					Pattern pa= Pattern.compile(regEx);
					Matcher ma = pa.matcher(Stock);  
					Stock=ma.replaceAll("").trim();
				}else{
					if(Stock.toString().contains(Fields.IN_STOCK)){
						Stock=Fields.IN_STOCK;
					}else{
						Stock=Fields.IS_NOT_STOCK;
					}	
				}
			}else if(StringUtil.isNotEmpty(docs.getElementsByClass("a-color-price ddm-font-size-15").text().toString())){
				Stock=docs.getElementsByClass("a-color-price ddm-font-size-15").text();
				if(Stock.toString().contains(Fields.IN_STOCK)){
					Stock=Fields.IN_STOCK;
				}else{ 
					Stock=Fields.IS_NOT_STOCK;
				}
			}else if(StringUtil.isNotEmpty(docs.getElementsByClass("buying mas-availability").text().toString())){
				if(Fields.NOW_PLACE_ORDER.contains(docs.getElementById("mas-availability").text())){
					Stock=Fields.IN_STOCK;
				}else{
					Stock=Fields.IS_NOT_STOCK;
				} 
			}else if(StringUtil.isNotEmpty(docs.getElementsByClass("a-size-medium a-color-price").text().toString())){
				if(!docs.getElementsByClass("a-size-medium a-color-price").text().toString().contains("￥")){
					if(docs.getElementsByClass("a-size-medium a-color-price").text().toString().contains(Fields.AMAZON_STOCK_LOWERCASE)){
						Stock=Fields.AMAZON_STOCK_YES;
					}else{
						Stock=Fields.AMAZON_STOCK_ON;
					}
				}

			}
			if(docs.getElementsByClass("a-size-medium a-color-success").text().toString().contains(Fields.AMAZON_STOCK)){ 
				if(docs.getElementsByClass("a-size-medium a-color-success").toString().contains(Fields.AMAZON_STOCK)){
					Stock=Fields.AMAZON_STOCK_YES;
				}else{
					Stock=Fields.AMAZON_STOCK_ON;
				}
			}
			if(docs.toString().contains("acrCustomerReviewText")){
				try {
					rateNum=docs.getElementById("averageCustomerReviews_feature_div").getElementById("acrCustomerReviewText").text();
					String regEx="[^0-9]"; 
					Pattern p = Pattern.compile(regEx);  
					Matcher m = p.matcher(rateNum);  
					rateNum=m.replaceAll("").trim();
				} catch (Exception e) {
				}
			}
			if(StringUtil.isNotEmpty(docs.getElementsByClass("imgTagWrapper").toString())){
				image=docs.getElementsByClass("imgTagWrapper").get(0).getElementsByTag("img").attr("data-old-hires").toString();
			}else if(StringUtil.isNotEmpty(docs.getElementsByClass("a-section image-2d clickableImage").toString())){
				image=docs.getElementsByClass("a-section image-2d clickableImage").get(0).getElementsByTag("img").attr("src").toString();
			}else if(docs.toString().contains("imgTagWrapper")){  
				try {image=docs.getElementById("imgTagWrapperId").getElementsByTag("img").attr("data-old-hires").toString();} catch (Exception e) {}
			}else if(docs.toString().contains("img-canvas")){
				try {image=docs.getElementsByClass("a-column a-span3 a-spacing-micro imageThumb thumb").get(0).getElementsByTag("img").attr("src").toString();} catch (Exception e) {}    
			}else if(docs.toString().contains("a-section image-2d clickableImage")){
				image=docs.getElementById("ebooks-img-canvas").getElementsByTag("img").attr("src").toString();
			}else if(StringUtil.isEmpty(image)){
				try {image=docs.getElementsByClass("a-link-normala-text-normal").get(0).getElementsByTag("img").attr("src").toString(); } catch (Exception e) {}
			}
			if(docs.toString().contains("bylineInfo")){
				try {
					shopName=docs.getElementById("bylineInfo").text();
					if(shopName.length()>100){
						shopName=shopName.substring(0,100)+"...";
					}
				} catch (Exception e) {
					shopName="";
				}
			}
			if(docs.toString().contains("ddmMerchantMessage")){
				platform_shoptype= docs.getElementById("ddmMerchantMessage").text();
				if(platform_shoptype.contains(Fields.PLATFORM_AMAZON_CN)){
					platform_shoptype=Fields.YES_PROPRIETARY;
				}else{
					platform_shoptype=Fields.PROPRIETARY;
				}
			} 
			if(StringUtil.isNotEmpty(docs.getElementsByClass("a-section pu-content").text().toString())){
				try {
					Elements li=docs.getElementsByClass("a-section pu-content").select("li");
					for(Element ment:li){
						if(Validation.isEmpty(promotion)){
							promotion=ment.getElementsByClass("pu-short-title a-text-bold").text();
						}else{
							promotion+="&&"+ment.getElementsByClass("pu-short-title a-text-bold").text();
						}
					}
				} catch (Exception e) {

				}
			}
			if(docs.toString().contains("productDetails_detailBullets_sections1")){
				try {
					List<Element> elements =docs.getElementById("productDetails_detailBullets_sections1").select("tr");
					for(int ii=0;ii<elements.size();ii++){ 
						if(elements.get(ii).getElementsByClass("a-color-secondary a-size-base prodDetSectionEntry").text().contains("Best Sellers Rank")){
							bsr=elements.get(ii).getElementsByTag("td").text();
							String ranking[]=bsr.split("#");
							for(int i=1;i<ranking.length;i++){
								bsr_rank=ranking[i].split("in")[0]; 
								bsr_category=ranking[i].toString().replaceAll("\\d+","").replace(",", "").trim();
								//获取商品bsr
								if(StringUtil.isNotEmpty(bsr_rank)){
									Craw_goods_bsr_Info  bsrInfo=crawlerPublicClassService.amazonusaGoodsBsr(mapType,bsr_rank,bsr_category,database,storage);
									mapData.put(Fields.CRAW_GOODS_BSR_INFO, bsrInfo);
								}
							}
						} 
					}
				} catch (Exception e) {
					logger.info("bsr fail to get >>>>>>>>>>>>>>>>>>>"+e.getMessage());
					// e.printStackTrace();
				}

			}
			if(docs.toString().contains("SalesRank")){
				try {
					System.out.println(docs.getElementById("SalesRank").text());
					String ranking[]=docs.getElementById("SalesRank").text().split("#");
					for(int i=1;i<ranking.length;i++){
						bsr_rank=ranking[i].split("in")[0]; 
						bsr_category=ranking[i].toString().replaceAll("\\d+","").replace(",", "").trim();
						//获取商品bsr
						if(StringUtil.isNotEmpty(bsr_rank)){
							Craw_goods_bsr_Info  bsrInfo=crawlerPublicClassService.amazonusaGoodsBsr(mapType,bsr_rank,bsr_category,database,storage);
							mapData.put(Fields.CRAW_GOODS_BSR_INFO, bsrInfo);
						}
					}
				} catch (Exception e) {
					logger.info("bsr fail to get >>>>>>>>>>>>>>>>>>>"+e.getMessage());
					//e.printStackTrace();
				} 
			}

			map.put("shopName", shopName);//店铺名称  
			map.put("shopid", shopId);//商品Id
			map.put("platform_shoptype", platform_shoptype);//平台商店类型
			map.put("sellerName", shopName);//卖家店铺名称
			map.put("picturl",  image);//图片url 
			map.put("goodsName", goodsName.replace("\"", ""));//商品名称
			map.put("delivery_place", delivery_place);//交易地址
			map.put("inventory", Stock);//商品库存
			map.put("postageFree", dontpackMall);//是不包邮
			map.put("rateNum", rateNum);//商品总评论数
			map.put("skuId", mapType.get("egoodsId").toString());//商品sku
			map.put("originalPrice", originalPrice);//原价
			map.put("currentPrice", Price);//现价
			map.put("goodsUrl",mapType.get("url"));
			map.put("channel",Fields.CLIENT_PC);
			map.put("promotion", promotion);//促销
			map.put("transactNum", null);//月销
			map.put("region",null);//卖家位置
			map.put("vendor", null);
			map.put("priceType", null);
			map.put("vendorType", null);
			map.put("subCatId", null);
			map.put("sellerId", null);
			map.put("message", null);   
			map.put("deposit", null);
			map.put("coupons", null);
			map.put("sale_qty", null);
			map.put("reserve_num", null);
			mapType.putAll(map);
			Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(mapType,database,storage); //商品详情 
			mapData.put(Fields.TABLE_CRAW_GOODS_INFO, info);
			if(StringUtil.isNotEmpty(Price)){
				Craw_goods_Price_Info pirce=crawlerPublicClassService.parseItemPrice(mapType,mapType.get("keywordId"),mapType.get("goodsId"),mapType.get("timeDate"),mapType.get("platform_name"),Fields.STATUS_COUNT_1,database,storage);
				mapData.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, pirce);
			}
			if(StringUtil.isNotEmpty(rateNum)){
				comment_status=Fields.STATUS_COUNT_1;
			}else{
				comment_status=Fields.STATUS_COUNT;
			}
			crawlerPublicClassService.goodsCommentInfo(mapType.get("timeDate").toString(),database.toString(),keywordInfo,rateNum,mapType.get("goodsId").toString(),comment_status);//插入评论	
		}
		mapData.putAll(map);
		return mapData;
	}
	//插入商品的上下时间 
	@SuppressWarnings("unchecked")
	public int amazonGoodsOnTime(String item,String database,Craw_goods_Info info,int page,String accountId,String goodsId)
			throws Exception{
		SimpleDateFormat sdf= new SimpleDateFormat("MMM dd,yyyy",Locale.ENGLISH);
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		Document docss =Jsoup.parse(item);  String dateCH=""; int status=0;
		Elements ment=docss.getElementsByClass("a-pagination").select("li");
		int lastPage=0;
		try {
			if(ment.size()==0){
				lastPage=1;
			}else{
				lastPage=Integer.valueOf(ment.get(ment.size()-2).text().replace(",", "").trim());
			}

			if(page==lastPage || lastPage==0){  
				if(StringUtil.isNotEmpty(docss.getElementsByClass("a-size-base a-color-secondary review-date").text())){
					Elements message=docss.getElementById("cm_cr-review_list").getElementsByClass("a-section review");
					String date=message.get(message.size()-1).getElementsByClass("a-size-base a-color-secondary review-date").text().replace("on","").trim().toString(); 
					dateCH = SimpleDate.SimpleDateData().format(sdf.parse(date));
					status=Fields.STATUS_COUNT_1;
				}
				Craw_goods_Fixed_Info  infoFixe=new Craw_goods_Fixed_Info();
				infoFixe.setCust_account_id(Integer.valueOf(accountId));
				infoFixe.setCust_keyword_id(Integer.valueOf(info.getCust_keyword_id()));
				infoFixe.setEgoodsid(info.getEgoodsId());
				infoFixe.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				infoFixe.setPlatform_name_en(info.getPlatform_name_en());
				infoFixe.setFirst_reviewtime(dateCH);
				infoFixe.setFixed_status(status);
				infoFixe.setGoodsId(goodsId);
				insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(infoFixe);
				insertprices.add(insertItemTime); 
				batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_GOODS_FIXED_INFO);//插入商品上架时间	 
			}
			return lastPage; 
		} catch (Exception e) {
			logger.info("解析失败》》》》》》》》》》》》》"+e.getMessage());
		}
		return lastPage;
	}
	//商品不存在上下架时间 
	@SuppressWarnings("unchecked")
	public void amazonGoodsOnTimeInexistence(String database,Craw_goods_Info info,String accountId,String goodsId) throws Exception{
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		try{
			Craw_goods_Fixed_Info  infoFixe=new Craw_goods_Fixed_Info();
			infoFixe.setCust_account_id(Integer.valueOf(accountId));
			infoFixe.setCust_keyword_id(Integer.valueOf(info.getCust_keyword_id()));
			infoFixe.setEgoodsid(info.getEgoodsId());
			infoFixe.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			infoFixe.setPlatform_name_en(info.getPlatform_name_en());
			infoFixe.setGoodsId(goodsId);
			infoFixe.setFixed_status(Fields.STATUS_COUNT);//标识商品暂无评论数
			insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(infoFixe);
			insertprices.add(insertItemTime); 
			batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_GOODS_FIXED_INFO);

		} catch (Exception e) {
           logger.info("Insert shelf time failed>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}
	}
}
