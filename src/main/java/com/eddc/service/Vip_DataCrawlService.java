package com.eddc.service;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
@Service
public class Vip_DataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	private static Logger logger = LoggerFactory.getLogger(Vip_DataCrawlService.class); 
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析vip页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> vipParseItemPage(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map,String database,String storage) {
		Map<String, String> data = new HashMap<String, String>();
		Map<String, Object> dataItem = new HashMap<String, Object>();
		data.putAll(map);
		String shopType = Fields.YES_PROPRIETARY;
		String sellerName =Fields.VIP_PROPRIETARY; //店铺
		String postageFree = Fields.DONTPACK_MAIL; //是否包邮
		String inventory =Fields.IN_STOCK; //有货无货
		String promotion = ""; //促销
		String image = ""; //图片路径
		String presentPrice = ""; //现价
		String originalPrice = ""; //原价
		String delivery_place = ""; //发货地址delivery_place
		String platformGoodsName="";
		String  shelves="";//是否下架
		String preferential="";//优惠卷
		String product_location="";//原产地
		Document docs = Jsoup.parse(item.toString());
		try {
			String Data_item=docs.toString().substring(docs.toString().indexOf("recommendProductInfo")+22,docs.toString().lastIndexOf("recommendSameBrand")-2);//recommendSameBrand
			JSONObject jsonObject = JSONObject.fromObject(Data_item);
			platformGoodsName=jsonObject.get("title").toString();
			image=jsonObject.get("smallImage").toString();
			if(!image.contains("//a.vpimg4.com")){
				image = "//a.vpimg4.com" + image;
			}
			presentPrice=jsonObject.get("vipshopPrice").toString();
			originalPrice=jsonObject.get("marketPrice").toString();
			promotion=jsonObject.get("vipDiscount").toString();
			if(docs.toString().contains("u-seldout-cover-seldout")){
				shelves=docs.getElementsByClass("u-seldout-cover-seldout").html();
			}
			if(docs.toString().contains("wap_have_over_sale_time_expose")){
				inventory=Fields.IS_NOT_STOCK;
			}
			if(jsonObject.toString().contains("coupons")){
				JSONArray  json = jsonObject.getJSONObject("coupon").getJSONArray("coupons");
				if(json.size()>0){ 
					String buy=JSONObject.fromObject(json.toArray()[0]).get("buy").toString();
					String fav=JSONObject.fromObject(json.toArray()[0]).get("fav").toString();
					preferential=Fields.FULL+buy+Fields.PREFERENTIAL+fav+Fields.MONEY;  
				}
			}
			try {
				product_location=jsonObject.get("areaOutput").toString();
			} catch (Exception e) {
				product_location=null;
			}
			if(Validation.isEmpty(originalPrice)){
				originalPrice=presentPrice;	
			}
			data.put("originalPrice", originalPrice.replaceAll("\"", ""));//原价
			data.put("currentPrice", presentPrice.replaceAll("\"", ""));//现价
			data.put("shopName", sellerName);//店铺名称
			data.put("region",delivery_place.replaceAll("\"", ""));//卖家位置
			data.put("platform_shoptype", shopType.replaceAll("\"", ""));//平台商店类型
			data.put("sellerName", sellerName);//卖家店铺名称
			data.put("picturl", image.replaceAll("\"", ""));//图片url 
			data.put("goodsName", platformGoodsName.replaceAll("\"", "").replaceAll("'", "`"));//商品名称
			data.put("delivery_place", delivery_place.replaceAll("\"", ""));//交易地址
			data.put("inventory", inventory);//商品库存
			data.put("postageFree",postageFree.replaceAll("\"", ""));//是不包邮
			data.put("skuId", wordsInfo.getCust_keyword_name());//商品sku
			data.put("promotion", promotion.replaceAll("\"", "")+preferential);//促销
			data.put("accountId",wordsInfo.getCust_account_id());
			data.put("goodsId", data.get("goodsId"));
			data.put("egoodsId", wordsInfo.getCust_keyword_name());
			data.put("timeDate", data.get("timeDate"));
			data.put("keywordId",wordsInfo.getCust_keyword_id());
			data.put("platform_name",data.get("platform"));
			data.put("goodsUrl",Fields.VIP_PC_URL+wordsInfo.getCust_keyword_name()+ ".html");
			data.put("channel",Fields.CLIENT_MOBILE);
			data.put("feature1", "1");
			data.put("product_location", product_location);//产地
			data.put("subCatId", null);
			data.put("rateNum", null);//商品总评论数
			data.put("transactNum", null);//月销
			data.put("message", null);   
			data.put("deposit", null);
			data.put("coupons", null);
			data.put("sale_qty", null);
			data.put("reserve_num", null);
			data.put("shopid", null);//商品Id
			data.put("sellerId", null);
			if(!Validation.isEmpty(presentPrice)){
				Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(data,database,storage); //商品详情插入数据库 
				Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(data,wordsInfo.getCust_keyword_id(),data.get("goodsId"),data.get("timeDate"),data.get("platform"),Fields.STATUS_COUNT_1,database,storage);//APP端价格插入数据库
				dataItem.put(Fields.TABLE_CRAW_GOODS_INFO, info);
				dataItem.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
				dataItem.putAll(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("插入信息失败------" + e.getMessage() + "----------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return dataItem;
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析唯品会PC页面价格信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> ResolutionCellPrices(CommodityPrices commodityPrices, String AppMessage) {
		Map<String, Object> data = new HashMap<String, Object>();
		String promotion = "";
		String presentPrice = "";
		String originalPrice = "";
		try {
			Document docs = Jsoup.parse(AppMessage.toString());
			promotion = docs.getElementsByClass("pbox-off J-discount").text();
			if(Validation.isEmpty(promotion)){
				if(docs.toString().contains("price-discount")){
					promotion=docs.getElementsByClass("price-info").get(0).getElementsByClass("price-discount").text();	
					if(Validation.isEmpty(promotion)){
						try {
							promotion=docs.toString().substring(docs.toString().indexOf("\"recommendProductInfo\": "), docs.toString().lastIndexOf("recommendSameBrand") + 1);
							JSONObject json= JSONObject.fromObject(promotion+"}");
							promotion=json.get("vipDiscount").toString();
						} catch (Exception e) {
							promotion="";
						}
					}
				}
			}
			if(docs.toString().contains("discount_index_min_tips")){ 
				promotion=StringHelper.getResultByReg(docs.toString(), "discount_index_min_tips : ([^,]+)").replaceAll("\"", "").trim();
			}
			if(Validation.isEmpty(promotion)){
				promotion=StringHelper.nullToString(StringHelper.getResultByReg(docs.toString(),"\"discount_index\":([^,]+)" )).replaceAll("\"", "");	
			}
			presentPrice = docs.getElementsByClass("J-price").text().split(" ")[0];
			if(Validation.isEmpty(presentPrice)){
				try {
					presentPrice=docs.getElementById("J-sale-price").text();
					if(Validation.isEmpty(presentPrice)){
						presentPrice=docs.getElementsByClass("u-product-info J-product-info").get(0).getElementsByClass("u-vipshop-price").text().replaceAll("¥", "");	
					}
				} catch (Exception e) {
				}	
			}
			String originalPriceStr = docs.getElementsByClass("J-mPrice").text();
			if(Validation.isEmpty(originalPriceStr)){
				try {
					originalPriceStr=docs.getElementsByClass("price-market").get(0).getElementsByTag("del").text().replaceAll("¥", "");
					if(Validation.isEmpty(originalPriceStr)){
						originalPriceStr=docs.getElementsByClass("u-product-info J-product-info").get(0).getElementsByClass("u-market-price").text().replaceAll("¥", "");	
					}
				} catch (Exception e) {
					originalPriceStr="";
				}
			}
			if(docs.toString().contains("sell_price")){
				presentPrice  = StringHelper.getResultByReg(docs.toString(),"\"sell_price\":([^,]+)");		
			}

			if(Validation.isEmpty(presentPrice)){
				presentPrice=StringHelper.nullToString(StringHelper.getResultByReg(docs.toString(),"\"sell_price\":([^,]+)" )).replaceAll("\"", "");  
			}
			if(Validation.isEmpty(originalPrice)){
				originalPrice=StringHelper.nullToString(StringHelper.getResultByReg(docs.toString(),"\"market_price\":([^,]+)" )).replaceAll("\"", "");	
			}

			if("".equals(originalPriceStr) || originalPriceStr == null){
				originalPrice = presentPrice;
			}else{
				originalPrice = originalPriceStr;
			}
			if(!Validation.isEmpty(originalPrice)){
				data.put("channel", Fields.CLIENT_PC);
				data.put("currentPrice",presentPrice);
				data.put("originalPrice", originalPrice);
				data.put("promotion", promotion.replaceAll("\"", ""));
				data.put("goodsId", commodityPrices.getGoodsid());
				data.put("keywordId", commodityPrices.getCust_keyword_id());
				data.put("SKUid",commodityPrices.getEgoodsId());
				if(StringUtil.isNotEmpty(presentPrice)){
					Craw_goods_Price_Info price=crawlerPublicClassService.commdityPricesData(commodityPrices,data);//插入PC端价格	
					data.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
					data.putAll(data);
				}
			}
		} catch (Exception e) {
			logger.info("解析PC端信息失败------" + e.getMessage() + "----------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return data;

	}
}
