package com.eddc.service;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.google.common.collect.Lists;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_goods_crowdfunding_price_Info;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.github.pagehelper.util.StringUtil;
@Service
public class CrowdFunding_DataCrawlService {
	private static Logger logger=Logger.getLogger(CrowdFunding_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	Search_DataCrawlService search_DataCrawlService;
	@Autowired
	BatchInsertData batchInsertData;
	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：Taobazc_DataCrawlService   
	 * 类描述：解析淘宝  京东 众筹详情数据   
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月31日 下午1:15:40   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月31日 下午1:15:40   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 *    
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object>crowdFunding(String item,Craw_goods_crowdfunding_Info crowdfunding,String database,String storage) throws Exception{
		Map<String,Object>data=new HashMap<String, Object>();
		List<Map<String,Object>> list_taobao = Lists.newArrayList();
		List<Map<String,Object>> list_jd = Lists.newArrayList();
		List<Map<String,Object>> list_fatherJd = Lists.newArrayList();
		Map<String,Object> insertItem_taobao=new HashMap<String,Object>();
		Map<String,Object> insertItem_jd=new HashMap<String,Object>();
		Map<String,Object> insertItem_fatherJd=new HashMap<String,Object>();
		List<Craw_goods_crowdfunding_price_Info>list=new ArrayList<Craw_goods_crowdfunding_price_Info>();
		List<Craw_goods_crowdfunding_price_Info>listJd=new ArrayList<Craw_goods_crowdfunding_price_Info>();
		if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_ZC)){
			try{
				String ite[]=item.split("item_id");
				for(int i=1;i<ite.length;i++){
					Craw_goods_crowdfunding_price_Info info=new Craw_goods_crowdfunding_price_Info();
					item="\"item_id"+ite[i];
					//System.out.println(StringHelper.getResultByReg(item.toString(),"\"images\": \"([^,\"]+)"));
					info.setGoodsId(crowdfunding.getGoodsId());
					info.setPlatform_name_en(crowdfunding.getPlatform_name_en());
					info.setCust_keyword_id(crowdfunding.getCust_keyword_id());
					info.setGoods_url(crowdfunding.getGoods_url());
					info.setSource_egoodsid(crowdfunding.getEgoodsId());
					info.setGoodsId(crowdfunding.getGoodsId());
					info.setEgoodsId(StringHelper.getResultByReg(item.toString(),"\"item_id\": \"([^,\"]+)"));
					info.setPlatform_goods_name(StringHelper.getResultByReg(item.toString(),"\"title\":\"([^,\"]+)"));//回报商品内容
					info.setPlatform_goods_remark(StringHelper.getResultByReg(item.toString(),"\"desc\": \"([^,\"]+)"));//回报商品内容的进一步说明
					String count=StringHelper.getResultByReg(item.toString(),"\"total\": ([^,\"]+)},");
					String Remain=StringHelper.getResultByReg(item.toString(),"\"support_person\": \"([^,\"]+)");
					info.setExp_shiptime_remark(StringHelper.getResultByReg(item.toString(),"\"make_days\": \"([^,\"]+)"));//预计回报说明
					if(StringUtil.isNotEmpty(count)){
						info.setLimit_total_count(Integer.valueOf(count.trim()));//限制人数;	
					}
					if(StringUtil.isNotEmpty(Remain)){
						info.setRemain_count(Integer.valueOf(Remain.trim()));////剩余人数	
					}
					info.setSource_curr_amount(crowdfunding.getCurr_amount());////已筹金额
					info.setSource_finish_per(crowdfunding.getFinish_per());//达成率
					info.setSource_focus_count(crowdfunding.getFocus_count());//喜欢人数
					info.setSource_support_count(crowdfunding.getSupport_count());//支持人数
					info.setSource_goods_status(crowdfunding.getGoods_status());//众筹状态
					info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					info.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					info.setGoods_pic_url(crowdfunding.getGoods_url());
					if(storage.equals(Fields.COUNT_4)){
						insertItem_taobao= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
						list_taobao.add(insertItem_taobao);  
					}else{
						list.add(info);
					}
				}
			} catch (Exception e) {
				logger.info("商品不存在");
			} 
		}else if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)){
			String regEx="[^0-9]"; Pattern p = Pattern.compile(regEx);     
			Document doc = Jsoup.parse(item); String count=""; String support_count="";
			try {
				support_count=doc.getElementsByClass("p-progress").last().getElementsByTag("span").last().text();
				Matcher m = p.matcher(support_count);
				support_count= m.replaceAll("").trim();//父节点支持者
			} catch (Exception e) {
				System.out.println(doc.getElementsByClass("p-progress").text());
				e.printStackTrace();
			}
			if(doc.toString().contains("details-right-fixed-box")){
				Elements lists=doc.getElementsByClass("details-right-fixed-box").get(0).getElementsByClass("box-grade");
				for(Element ment:lists){
					try {
						if(ment.toString().contains("font-b")){
							Craw_goods_crowdfunding_price_Info info=new Craw_goods_crowdfunding_price_Info();
							String price=ment.getElementsByClass("t-price").get(0).getElementsByTag("span").text();
							if(ment.toString().contains(Fields.UNLIMITED)){
								count="-1";//无限额人数
								info.setRemain_count(-1);
							}else{
								count=ment.getElementsByClass("limit-num").get(0).getElementsByTag("span").get(1).text(); 
								info.setRemain_count(Integer.valueOf(ment.getElementsByClass("limit-num").get(0).getElementsByTag("span").get(2).text()));////剩余人数
							}
							if(ment.toString().contains("box-imglist")){
								info.setGoods_url(ment.getElementsByClass("box-imglist").get(0).getElementsByTag("ul").get(0).getElementsByTag("li").get(0).getElementsByTag("img").attr("data-src").trim());	
							}
							Float  curr_price=Float.valueOf(price);//众筹价
							info.setCurr_price(curr_price);//众筹价
							info.setGoodsId(crowdfunding.getGoodsId());
							info.setEgoodsId(crowdfunding.getEgoodsId()); 
							info.setPlatform_name_en(crowdfunding.getPlatform_name_en());
							info.setCust_keyword_id(crowdfunding.getCust_keyword_id());
							info.setSource_egoodsid(crowdfunding.getEgoodsId());
							info.setPlatform_goods_name(doc.getElementsByClass("p-title").text());//回报商品内容
							info.setPlatform_goods_remark(ment.getElementsByClass("box-intro").text());//回报商品内容的进一步说明
							info.setLimit_total_count(Integer.valueOf(count));//限制人数;
							info.setExp_shiptime_remark(ment.getElementsByClass("box-item").get(1).text());//预计回报说明
							info.setExp_postfee_remark(ment.getElementsByClass("box-item").get(0).getElementsByClass("font-b").text());//邮费说明
							info.setSupport_count(Integer.valueOf(ment.getElementsByClass("t-people").get(0).getElementsByTag("span").text()));//子节点支持者
							info.setSource_curr_amount(crowdfunding.getCurr_amount());////已筹金额
							info.setSource_finish_per(crowdfunding.getFinish_per());//达成率
							info.setSource_focus_count(crowdfunding.getFocus_count());//喜欢人数
							info.setSource_support_count(Integer.valueOf(support_count=(String) support_count==""?"0":support_count));//父节点支持人数
							info.setSource_goods_status(crowdfunding.getGoods_status());//众筹状态
							info.setGoods_url(crowdfunding.getGoods_url());
							info.setGoods_pic_url(crowdfunding.getGoods_pic_url());
							info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							if(storage.equals(Fields.COUNT_4)){
								insertItem_jd= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
								list_jd.add(insertItem_jd);  
							}else{
								listJd.add(info);
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} 
				if(doc.toString().contains("projectMessage")){
					String end_date=doc.getElementById("projectMessage").getElementsByClass("f_red").first().text().trim();
					String target_amount=doc.getElementById("projectMessage").getElementsByClass("f_red").last().text().trim();
					crowdfunding.setBegin_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setEnd_date(end_date.replace(Fields.YEARS,"-").replace(Fields.MONTHS,"-").replace(Fields.DAY,"").trim());
					crowdfunding.setPrice_status(Fields.STATUS_COUNT);
					crowdfunding.setSupport_count(Integer.valueOf(support_count=(String) support_count==""?"0":support_count));
					crowdfunding.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setTarget_amount(Float.valueOf(target_amount.replace("￥","").trim()));//目标金额	
					insertItem_fatherJd=BeanMapUtil.convertBean2MapWithUnderscoreName(crowdfunding);
					list_fatherJd.add(insertItem_fatherJd);	
				}
			}
		}
		if(storage.equals(Fields.COUNT_4)){
			if(list_taobao!=null && list_taobao.size()>0){
				batchInsertData.insertIntoData(list_taobao,database,Fields.TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO);		
			}
			if(list_jd!=null && list_jd.size()>0){
				batchInsertData.insertIntoData(list_jd,database,Fields.TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO);		
			}
		   if(list_fatherJd!=null && list_fatherJd.size()>0){
			   batchInsertData.insertIntoData(list_fatherJd,database,Fields.CRAW_GOODS_TRANSLATE_INFO);		  
		   }
		}
		data.put("list_taobaozc", list);
		data.put("list_jdzc", listJd);
		data.put("list_fatherJd", list_fatherJd);
		return data;
	} 

	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：Taobazc_DataCrawlService   
	 * 类描述：解析淘宝  京东 众筹详情数据    更新 历史数据状态
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月31日 下午1:15:40   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月31日 下午1:15:40   
	 * 修改备注：   
	 * @version    
	 * @throws ParseException 
	 *    
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object>crowdFundingStatus(String item,Craw_goods_crowdfunding_Info crowdfunding,String database,String storage) throws ParseException{
		Map<String,Object>data=new HashMap<String, Object>();
		List<Map<String,Object>> list_status = Lists.newArrayList();
		List<Map<String,Object>> list_statusJd = Lists.newArrayList();
		Map<String,Object> insertItem_taobao=new HashMap<String,Object>();
		Map<String,Object> insertItem_jd=new HashMap<String,Object>();
		List<Craw_goods_crowdfunding_Info>list=new ArrayList<Craw_goods_crowdfunding_Info>();
		List<Craw_goods_crowdfunding_Info>listJd=new ArrayList<Craw_goods_crowdfunding_Info>();
		if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_ZC)){
			try{
				if(item.toString().contains("status_value")){
					String itmess=item.substring(item.indexOf("{"),item.lastIndexOf("status_value")-4)+"}}";
					JSONObject json = JSONObject.fromObject(itmess);
					String status=json.getJSONObject("data").get("status").toString();
					if(json.getJSONObject("data").getInt("finish_per")>100){
						crowdfunding.setGoods_status(Fields.PROJECT_SUCCESS);
					}else{
						crowdfunding.setGoods_status(status);	
					}
					crowdfunding.setBatch_time(item);
					crowdfunding.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
					crowdfunding.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
					if(storage.equals(Fields.COUNT_4)){
						insertItem_taobao= BeanMapUtil.convertBean2MapWithUnderscoreName(crowdfunding);
						list_status.add(insertItem_taobao);  
						//crawlerPublicClassService.updateStatus(crowdfunding.getEgoodsId(),database,crowdfunding.getGoods_status());
					}else{
						list.add(crowdfunding);
					}
					//更新商品状态
					// crawlerPublicClassService.updateStatus(crowdfunding.getEgoodsId(),database,new String(status.getBytes("iso-8859-1"), "GBK"));
				}
			} catch (Exception e) {
				logger.info("商品不存在");
			} 
		}else if(crowdfunding.getPlatform_name_en().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)){
			try{
				String regEx="[^0-9]"; Pattern p = Pattern.compile(regEx);     
				Document doc = Jsoup.parse(item);
				if(doc.toString().contains("projectMessage")){
					String dataMessage=doc.getElementById("projectMessage").getElementsByClass("f_red").get(0).text();
					String str =dataMessage.replaceAll("[^x00-xff]", "-");
					str=str.substring(0, str.length()-1);
					Date date =SimpleDate.SimpleDateTime().parse(str);
					if (new Date().getTime()> date.getTime() ) {
						String message=doc.getElementsByClass("fl percent").text().trim();
						Matcher m = p.matcher(message);
						if(Integer.valueOf(m.replaceAll("").trim())>=100){
							crowdfunding.setGoods_status(Fields.PROJECT_SUCCESS);
							//crawlerPublicClassService.updateStatus(crowdfunding.getEgoodsId(),database,Fields.PROJECT_SUCCESS);  
						}else{
							crowdfunding.setGoods_status(Fields.PROJECT_FAILED);
							//crawlerPublicClassService.updateStatus(crowdfunding.getEgoodsId(),database,Fields.PROJECT_FAILED);  
						}
						crowdfunding.setBatch_time(item);
						crowdfunding.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						crowdfunding.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
						crowdfunding.setBatch_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						if(storage.equals(Fields.COUNT_4)){
							insertItem_jd= BeanMapUtil.convertBean2MapWithUnderscoreName(crowdfunding);
							list_statusJd.add(insertItem_jd);  
						}else{
							listJd.add(crowdfunding);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(storage.equals(Fields.COUNT_4)){ 
			if(list_status!=null && list_status.size()>0){
				batchInsertData.insertIntoData(list_status,database,Fields.CRAW_GOODS_TRANSLATE_INFO);		
			}
			if(list_statusJd!=null && list_statusJd.size()>0){
				batchInsertData.insertIntoData(list_statusJd,database,Fields.CRAW_GOODS_TRANSLATE_INFO);		
			}
		}
		data.put("list_taobaozc", list);
		data.put("list_jdzc", listJd);
		return data;
	} 
}
