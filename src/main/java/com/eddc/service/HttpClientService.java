package com.eddc.service;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import net.sf.json.JSONObject;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.util.ClientHttpDataUtil;
import com.eddc.util.ClientHttpUtil;
import com.eddc.util.Fields;
import com.eddc.util.HttpClientUtils;
import com.eddc.util.HttpCrawlerUtil;
import com.eddc.util.JavaHttpClient;
import com.eddc.util.OkHttpClientUtil;
import com.eddc.util.SimpleDate;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import example.HttpClientLMT;
@Service
public class HttpClientService {
	@Autowired
	private ClientHttpDataUtil clientHttpDataUtil;
	@Autowired
	private ClientHttpUtil clientHttpUtil;
	@Autowired
	private HttpCrawlerUtil tttpCrawlerUtil;
	@Autowired
	private JavaHttpClient javaHttpClient;
	@Autowired
	private HttpClientUtils httpClientUtils;
	private static Logger logger=Logger.getLogger(ClientHttpUtil.class);
	public String interfaceSwitch(String count,Map<String,String>Map,String database){
		String StringMessage="";
		try {
			if(count.equals(Fields.COUNT_01)){
				StringMessage=abcCloudCrawler(Map,database);//阿布云1
			}else if(count.equals(Fields.COUNT_2)){
				StringMessage=mixedInterfaceCrawler(Map,database);//阿布云接口 代理云 2
			}else if(count.equals(Fields.COUNT_3)){
				StringMessage=requestData(Map);//luminati
			}else if(count.equals(Fields.COUNT_4)){
				StringMessage=GoodsProductDetailsData(Map);//云代理
			}else if(count.equals(Fields.COUNT_5)){
				StringMessage=agentInterfaceCrawler(Map);//云代理+luminati
			}else if(count.equals(Fields.COUNT_6)){
				StringMessage=abyAgentInterfaceCrawlerLuminti(Map,database);//luminati+阿布云
			}else if(count.equals(Fields.COUNT_7)){
				StringMessage=getJsoupResult4GData(Map);//4G网卡
			}else{
				StringMessage=GoodsProductDetailsData(Map);//抓取Itemyem 
			} 
			if(StringUtil.isEmpty(StringMessage) ||StringMessage.contains(Fields.RETURNURL_FLAG)){
				StringMessage=GoodsProductDetailsData(Map);//云代理	
			}
		} catch (Exception e) {
			logger.info("请求数据失败"+e.getMessage());
		}
		return StringMessage;
	}

	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： 阿布云接口请求数据 1
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 *    
	 */
	public String abcCloudCrawler(Map<String,String>map,String database) throws Exception{
		String itemPage = null;
		int count = 0;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)|| itemPage.contains(Fields.MEMBERSHIP_ONLY)  ) {
			count++;
			if (count >8) {
				break;
			}
			try {
				itemPage=javaHttpClient.doGetRequest(map,database);
				if(Validation.isEmpty(itemPage)){
					itemPage=JavaHttpClient.getUrlProxyContent(map.get("url"),map.get("urlRef"));	
				}
			} catch (Exception e) {
				logger.info("请求"+map.get("platform")+":数据失败"+e.getMessage());
			}
		}
		return itemPage;
	}
    
	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： 阿布云,代理云 接口请求数据 2
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws InterruptedException 
	 *    
	 */
	public String mixedInterfaceCrawler(Map<String,String>map,String database) throws InterruptedException{
		String itemPage = null;
		int count = 0;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.MEMBERSHIP_ONLY) ) {
			count++;
			if (count >10) {
				break;
			}
			//Thread.sleep(2000);
			try {
				itemPage=GoodsProductDetailsData(map);
				if(Validation.isEmpty(itemPage)||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.MEMBERSHIP_ONLY) ){
					itemPage=javaHttpClient.doGetRequest(map,database);
				}
				if(Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.MEMBERSHIP_ONLY) ){
					itemPage=JavaHttpClient.getUrlProxyContent(map.get("url"),map.get("urlRef"));	
				}
			} catch (Exception e) {
				logger.info("The request for"+map.get("platform")+" data failed >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
			}
		}
		return itemPage;
	}
	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： 代理云 接口请求数据 4
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws InterruptedException 
	 *    
	 */
	public String GoodsProductDetailsData(Map<String,String> Map) throws Exception{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.MEMBERSHIP_ONLY) ) {
			Map<String, String>result=new HashMap<String, String>();
			count++;
			if (count > 5) {
				break;
			}
			try { 
				result =getResultMap(Map);//请求获取数据
				itemPage=result.get("result").toString();
				Map.put("status", "2");
				Map .put("messageLog","请求数据"+Fields.SUCCESS);
			}catch (Exception e) {
				Map.put("status", "0");
				Map .put("messageLog",Map.get("platform")+Fields.JD_DATA+e.toString()+e.getMessage());
				logger.info("The request for"+Map.get("platform")+" data failed >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				e.printStackTrace(new PrintStream(baos));
				String exception = baos.toString();	
				if(exception.contains("http://err.tmall.com/m-error1.html")) {
					return "当前商品已经下架,很抱歉，您查看的宝贝不存在，可能已下架或被转移";
				}
			}
			Map.put("messageLog",""); 

		}
		return itemPage;
	}
	
	/**  
	* @Title: getJsoupResult4GData  
	* @Description: TODO(4G网络请求数据)  
	* @param @param map
	* @param @return
	* @param @throws Exception    设定文件  
	* @return String    返回类型  
	* @throws  
	*/  
	public String getJsoupResult4GData(Map<String,String> map) throws Exception{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) ) {
			count++;
			if (count > 10) {
				break;
			}
			try {
				Thread.sleep(2000); 
				itemPage=getJsoupResult4G(map);
				if(Validation.isEmpty(itemPage)){
					Map<String,String>result=clientHttpDataUtil.getMap(map);//开始请求数据	
					itemPage=result.get("result").toString();
				}
			} catch (Exception e) {
				itemPage=GoodsProductDetailsData(map);
				logger.info("请求"+map.get("platform")+"数据失败"+e.getMessage());
			}
		}

		return itemPage;

	}
	/**  
	* @Title: getJsoupResult4G  
	* @Description: TODO(4G网络抓取数据)  
	* @throws  
	*/  
	public  String getJsoupResult4G(Map <String,String>mapLog) throws Exception {
		String result="";
//		try {
//			String path=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
//			String ipData=getJsonObj(path);
//			JSONObject currPriceJson = JSONObject.fromObject(ipData);
//			mapLog.put("ip", currPriceJson.getString("ip"));
//			mapLog.put("port",currPriceJson.getString("port"));
//		} catch (Exception e) {
//			logger.info("IP请求失败");
//		}
		try {
			result=clientHttpDataUtil.getJsoupResult4G(mapLog);//开始请求数据
			return result;
		} catch (Exception ex) {
			return result;
		}
	}
	

	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： 代理云 +luminati:5
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws InterruptedException 
	 *    
	 */
	public String agentInterfaceCrawler(Map<String,String> map) throws InterruptedException{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)  || itemPage.contains(Fields.MEMBERSHIP_ONLY)) {
			count++;
			//Thread.sleep(2000); 
			if (count > 10) {
				break;
			}
			try { 
				itemPage=GoodsProductDetailsData(map);
				if(Validation.isEmpty(itemPage)){
					Map<String,String > mp = new HttpClientLMT(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
					itemPage = mp.get("result").toString();	
				}
			} catch (Exception e) {
				logger.info("The request for"+map.get("platform")+" data failed >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
			}
		}

		return itemPage;

	}

	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： 阿布云 +luminati:6
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 *    
	 */
	public String abyAgentInterfaceCrawlerLuminti(Map<String,String> map,String database) throws Exception{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT) || itemPage.contains(Fields.MEMBERSHIP_ONLY) ) {
			count++;

			if (count > 10) {
				break;
			}
			try {
				//Thread.sleep(2000); 
				itemPage=javaHttpClient.doGetRequest(map,database);
				if(Validation.isEmpty(itemPage)){
					Map<String,String > mp = new HttpClientLMT(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
					itemPage = mp.get("result").toString();	
				}
			} catch (Exception e) {
				itemPage=GoodsProductDetailsData(map);
				logger.info("The request for"+map.get("platform")+" data failed >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
			}
		}

		return itemPage;

	}
	//httpclient4.5接口
	public String GoodsProductHttpClient(Map<String,String> Map) throws Exception{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)  ) {

			count++;
			if (count > 10) {
				break;
			}
			try { 
				//Thread.sleep(2000);
				itemPage =tttpCrawlerUtil.httpGetRequestData(Map);//请求获取数据
				Map.put("status", "2");
				Map .put("messageLog","请求数据"+Fields.SUCCESS);
			} catch (Exception e) {
				Map.put("status", "0"); 
				Map .put("messageLog",Map.get("platform")+Fields.JD_DATA+e.toString()+e.getMessage());
				logger.info("The request for"+Map.get("platform")+" data failed >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
			}
			//logger.info("获取当前"+Map.get("platform")+"商品==={"+Map.get("egoodsId")+"}===的item页面次数为:>>> {"+count+"}"+"商品url:"+Map.get("url"));
			//kafkaProducerLog(Map);
			Map.put("messageLog",""); 

		}
		return itemPage;
	}

	/**   
	 *    
	 * 项目名称：Price_monitoring_crawler   
	 * 类名称：HttpClientService   
	 * 类描述： luminati3
	 * 创建人：jack.zhao   
	 * 创建时间：2018年5月22日 上午10:19:27   
	 * 修改人：jack.zhao   
	 * 修改时间：2018年5月22日 上午10:19:27   
	 * 修改备注：   
	 * @version    
	 * @throws InterruptedException 
	 *    
	 */
	public String  requestData(Map<String,String> map) throws Exception{
		int count = 0;
		String itemPage = null;
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)  || itemPage.contains(Fields.MEMBERSHIP_ONLY)) {
			count++;
			//Thread.sleep(2000); 
			if (count > 8) {
				break;
			}
			try { 
				Map<String,String > mp = new HttpClientLMT(null).request(map.get("url"), map.get("coding"), map.get("cookie"), map.get("urlRef"));
				if(Validation.isEmpty(mp)){
					itemPage=GoodsProductDetailsData(map);
				}else{
					itemPage = mp.get("result").toString();	
				}
				map.put("status", "2");
				map.put("messageLog",map.get("platform")+logMessage(map.get("status"))+map.get("messageLog")+Fields.SUCCESS);
			} catch (ConnectTimeoutException e) {
				if(Validation.isEmpty(itemPage)){
					itemPage=GoodsProductDetailsData(map);
				}
				map.put("status", "0");
				map.put("count", "0");
				map.put("messageLog", map.get("platform")+logMessage(map.get("status"))+e.toString());
				logger.info(map.get("platform")+map.get("messageLog")+logMessage(map.get("status"))+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
			}catch (Exception e) {
				map.put("count", "1"); 
				map.put("status", "0");
				map.put("messageLog",map.get("platform")+map.get("messageLog")+logMessage(map.get("status"))+e.toString());
				logger.info(map.get("platform")+map.get("messageLog")+logMessage(map.get("status"))+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
			map.put("messageLog","");
		}
		return itemPage;

	}


	/***
	 * 普通接口请求 数据
	 * @throws Exception 
	 */
	 public String GoodsProduct(Map<String,String>map,String database) throws Exception{
		String itemPage = null;//Map<String,String>postMap
		int count = 0;
		map.put("messageLog",Fields.DATAILS); 
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.RETURNURL) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.QUERYHTM) || itemPage.contains(Fields.MSGTIP) || itemPage.contains(Fields.ISCOASEOUT)  ) {
			count++;
			if (count > 10) {
				break;
			}
			//Thread.sleep(2000);
			try { 
				itemPage=GoodsProductDetailsData(map);
				if(Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.QUERYHTM)|| itemPage.contains(Fields.MSGTIP)|| itemPage.contains(Fields.ISCOASEOUT)   ){
					itemPage=javaHttpClient.doGetRequest(map,database);
				}
				if(Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.QUERYHTM)|| itemPage.contains(Fields.MSGTIP)|| itemPage.contains(Fields.ISCOASEOUT)   ){
					itemPage=getJsoupResultMessage(map);
				}
				if(Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.QUERYHTM)|| itemPage.contains(Fields.MSGTIP)|| itemPage.contains(Fields.ISCOASEOUT)  ){
					itemPage=GoodsProductDetailsData(map); 
				}
				if(Validation.isEmpty(itemPage) || itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.QUERYHTM)|| itemPage.contains(Fields.MSGTIP)|| itemPage.contains(Fields.ISCOASEOUT)   ){
					itemPage=GoodsProductDetailsData(map); 
				}
			} catch (Exception e) {
				itemPage=GoodsProductDetailsData(map);
			}
		}
		//logger.info("获取当前"+map.get("platform")+"商品==={"+map.get("egoodsId")+"}===的item页面次数为:>>> {"+count+"} 商品url:"+map.get("url")+" 当前时间："+SimpleDate.SimpleDateFormatData().format(new Date()));
		return itemPage; 
	}
	
	
	/***
	 * 普通接口Item页面请求 数据
	 * @throws Exception 
	 */
	public String GoodsProductItem(Map<String,String>map) throws Exception{ 
		String itemPage = null;
		int count = 0;
		map.put("messageLog",Fields.DATAILS);
		while (Validation.isEmpty(itemPage) ||itemPage.contains(Fields.PLEASE_LATER)|| itemPage.contains(Fields.RETURNURL_FLAG)|| itemPage.contains(Fields.FORM_J_FORM)|| itemPage.contains(Fields.QUERYHTM)|| itemPage.contains(Fields.MSGTIP)|| itemPage.contains(Fields.ISCOASEOUT)  || itemPage.contains(Fields.VIEWPORT) ) { 
			count++;
			if (count > 8) {
				break;
			}
			try {
				//Thread.sleep(2000);
				itemPage=GoodsProductDetailsData(map); 
				if(Validation.isEmpty(itemPage) ){
				} 
			} catch (Exception e) {
				itemPage=requestData(map);
			}
		}
		//logger.info("获取当前"+map.get("platform")+"商品==={"+map.get("egoodsId")+"}===的item页面次数为:>>> {"+count+"} 当前时间："+SimpleDate.SimpleDateFormatData().format(new Date()));
		return itemPage;
	}


	public  Map<String,String> getResultMap(Map <String,String>mapLog) throws Exception {
		Map<String,String> result=new HashMap<String, String>();
		List<String> list=new ArrayList<String>();
		//Thread.sleep(1000);  
		String count="0";
		try {
			String path=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
			String ipData=getJsonObj(path);
			JSONObject currPriceJson = JSONObject.fromObject(ipData);
			mapLog.put("ip", currPriceJson.getString("ip"));
			mapLog.put("port",currPriceJson.getString("port"));
		} catch (Exception e) {
			logger.info("IP请求失败");
		}
		if(list.size()>0){
			count="2";
		}
		try {
			//result=clientHttpDataUtil.getMap(mapLog);//开始请求数据
			String item=httpClientUtils.doGet(mapLog);//开始请求数据
			result.put("result", item);
			result.put("status", count);
			result.putAll(mapLog);
			return result;
		} catch (Exception ex) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ex.printStackTrace(new PrintStream(baos));
			String exception = baos.toString();	
			if(exception.contains("http://err.tmall.com/m-error1.html")) {
				result.put("result","当前商品已经下架,很抱歉，您查看的宝贝不存在，可能已下架或被转移");
			}
			logger.error("", ex);
			return result;
		}
	}

	/**  
	 * @Title: httpGet  
	 * @Description: TODO 请求商品信息
	 * @param @param url
	 * @param @param cookie
	 * @param @return
	 * @param @throws IOException    设定文件  
	 * @return String    返回类型  
	 * @throws  
	 */  
	public String httpGet(String url,String cookie) throws IOException{
		String document="";
		try {
			Document reult=clientHttpUtil.httpGet(url, cookie);
			document=reult.toString();
		} catch (Exception e) {
			document=null;
		}
		return document;

	}

	public   String  getJsoupResultMessage(Map <String,String>mapLog){
		String document="";
		int count=0;
		try {
			//int result=random.nextInt(15);
			while (Validation.isEmpty(document) || document.contains(Fields.RETURNURL_FLAG)) {
				count++;
				if (count > 5) {
					break;
				} 
				//Thread.sleep(result*1000);
				document=clientHttpUtil.getJsoupResultMessage(mapLog);
				if(Validation.isEmpty(document) || document.contains(Fields.RETURNURL_FLAG)){
					document=getJsonObj(mapLog.get("url"));
				}
			}
		} catch (Exception e) {
			document="";
		} 
		return document;
	}
	
	public String getOkHttpClient(String priductUrl,Map<String,String> param) {
		StringBuffer item=new StringBuffer();
		OkHttpClientUtil okhtp=new OkHttpClientUtil();
		int count=0;
		while (Validation.isEmpty(item.toString()) ||item.toString().contains(Fields.PLEASE_LATER)|| item.toString().contains(Fields.FORM_J_FORM)|| item.toString().contains(Fields.RETURNURL) || item.toString().contains(Fields.RETURNURL_FLAG)|| item.toString().contains(Fields.QUERYHTM) || item.toString().contains(Fields.MSGTIP) || item.toString().contains(Fields.ISCOASEOUT) || item.toString().contains(Fields.MEMBERSHIP_ONLY) ) {
			count++;
			if (count > 10) {
				break;
			}
			try {
				 item.append(okhtp.okHttpClient(priductUrl,param));
			} catch (Exception e) {
				logger.info("==>>【请求数据失败 error:"+e+"<<==");
				e.printStackTrace();
			}
		}
		return item.toString();
	}

	/**  
	 * @Title: getJsonObj  
	 * @Description: TODO 请求数据
	 * @param @param src
	 * @param @param code
	 * @param @return    设定文件  
	 * @return String    返回类型  
	 * @throws  
	 */  
	public String  getJsonObj(String src) {
		String message="";
		int count=0;
		while (Validation.isEmpty(message)) {
			count++;
			if (count > 5) {
				break;
			}
			message=clientHttpUtil.getJsonObj(src); 
		}
		return message;

	}
	public String logMessage(String log){
		String message="";
		if(log.equals("0")){
			message= Fields.JD_TIMEOUT;
		}else if(log.equals("1")){
			message= Fields.JD_DATA;
		}else{
			message= Fields.SUCCESS;
		}
		return message;	
	}
	public Map<String,String>jumeiMap (){
		Map<String,String>jumeiMap=new HashMap<String, String>();
		jumeiMap.put("cookie","");
		jumeiMap.put("coding", "UTF-8");
		jumeiMap.put("urlRef", "item.jumei.com");
		jumeiMap.put("host", "item.jumei.com");
		jumeiMap.put("platform", Fields.PLATFORM_JUMEI);
		jumeiMap.put("ip","-");
		return jumeiMap;
	}

	public Map<String,String>KaolaMap (){
		Map<String,String>Map=new HashMap<String, String>();
		Map.put("coding", "UTF-8");
		Map.put("urlRef", "");
		Map.put("cookie","");
		Map.put("ip","-");
		Map.put("platform", Fields.PLATFORM_KAOLA);
		return Map;
	}

	public Map<String,String>lazadaMap (){
		Map<String,String>Map=new HashMap<String, String>();
		Map.put("cookie","");
		Map.put("coding", "UTF-8");
		Map.put("urlRef", "www.lazada.com.my");
		Map.put("platform", Fields.PLATFORM_LAZADA);
		Map.put("ip","-");
		return Map;
	}
	public Map<String,String>sillaMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie","JSESSIONID=jtlZ8ll0i37iq2JRa0O8myA27rOUsULZaEWYiPjKj30WkARp3Nwv!-1089477281; ");
		map.put("coding", "UTF-8");
		map.put("urlRef", "www.shilladfs.com");
		map.put("platform", Fields.PLATFORM_SILLA);
		map.put("ip","-");
		return map;
	}
	public Map<String,String>suningMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie","cityId=9264; districtId=12113; SN_CITY=20_021_1000267_9264_01_12113_3_0; _snstyxuid=654260626164CBLF; _snsr=baidu%7Cbrand%7C%7Clogo%7C%25E8%258B%258F%25E5%25AE%2581*%3A*; authId=si590FAA6D8F49F4D1F97F6001806593BA; secureToken=8BC76544D7102B852B6C28DC2DA84DF9; _snms=150270379321633744; smhst=101923925|0000000000a101923918|0000000000a101822315|0000000000a625019371|0000000000a102668611|0000000000; _snma=1%7C150086118535495580%7C1500861185354%7C1502703813564%7C1502703822326%7C31%7C3; _snmc=1; _snmp=15027038222755593; _snmb=150270377280922319%7C1502703822438%7C1502703822330%7C8; _ga=GA1.2.721456464.1500861186; _gid=GA1.2.1016444502.1502703773");
		map.put("coding", "UTF-8");
		map.put("urlRef", null);
		map.put("platform", Fields.PLATFORM_SUNING);
		map.put("ip","-");
		return map;
	}
	public Map<String,String>tmallMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie",null);
		map.put("coding", "UTF-8");
		map.put("urlRef", "acs.m.taobao.com");
		map.put("platform", Fields.PLATFORM_TMALL_EN);
		map.put("ip","-");
		return map;
	}

	public Map<String,String>vipMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie",null);
		map.put("coding", "UTF-8");
		map.put("urlRef", null);
		map.put("platform", Fields.PLATFORM_VIP);
		map.put("ip","-");
		return map;
	}

	public Map<String,String>yhdMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie","__jdv=259140492|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|yhdbaidupcpz007_0_3bb96dad87b9484281e519ff3d85487c|1508120819221; cart_cookie_uuid=587fdf90-85c3-47c6-9c68-afc36011d322; yhd_location=2_2817_51973_0; provinceId=2; cityId=2817; mba_muid=15081208192201111174638; test=1; cart_num=0; __jda=81617359.15081208192201111174638.1508120819.1508134896.1508137618.4; __jdb=81617359.1.15081208192201111174638|4.1508137618; __jdc=81617359");
		map.put("coding", "UTF-8");
		map.put("urlRef", "www.yhd.com");
		map.put("platform", Fields.PLATFORM_YHD);
		map.put("ip","-");
		return map;
	}

	public Map<String,String>jdMap (){
		Map<String,String>map=new HashMap<String, String>();
		map.put("cookie","");
		map.put("coding", "utf-8");
		map.put("urlRef", "www.jd.com");
		map.put("host", "www.jd.com");
		map.put("platform", Fields.PLATFORM_JD);
		map.put("ip","-");
		return map;
	}

}
