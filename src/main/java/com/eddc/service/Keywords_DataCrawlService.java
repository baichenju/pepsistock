package com.eddc.service;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.Craw_Access_keyword;
import com.eddc.model.Craw_customerweb_categorys_info;
import com.eddc.model.Craw_keywords_Info;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import example.HttpClientLMT;
@Service
public class Keywords_DataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	@Autowired
	private HttpClientService httpClientService;
	private static Logger logger=Logger.getLogger(Keywords_DataCrawlService.class);
	public void keywords_DataCrawl_Data(Map<String,String>map){
		List<Craw_keywords_Info>list_info=new ArrayList<Craw_keywords_Info>();
		try {
			if(map.get("StringMessage").indexOf("i2iTags")>0){
				String[] str = map.get("StringMessage").split("i2iTags");
				for (int j = 1; j < str.length; j++) {
					String titleName = StringHelper.nullToString(StringHelper.getResultByReg(str[j], "\\[\\{\"title\":\"([^<>\"]+)\"")).replaceAll("'", ""); 
					if(!titleName.equals("掌柜热卖宝贝")){
						Craw_keywords_Info info=new Craw_keywords_Info();
						if(str[j].indexOf("item.htm")>0){
							String egoodsid = StringHelper.nullToString(StringHelper.getResultByReg(str[j], "nid\":\"([^<>\"]+)\"")).replaceAll("'", "");
							String sellername = StringHelper.nullToString(StringHelper.getResultByReg(str[j], "nick\":\"([^<>\"]+)\"")).replaceAll("'", "");
							if(Fields.ACCOUNTID_33.equals(map.get("accountId"))){
								if(j==13){ 
									if(Integer.valueOf(map.get("count").toString())==2){ 
										logger.info(map.get("count").toString());
										break; 
									}   
								}    
							}else if(Integer.valueOf(map.get("count").toString())==Integer.valueOf(map.get("page").toString())){ 
									logger.info(map.get("count").toString());
									break; 
							}    
							info.setCust_keyword_name(egoodsid);
							info.setPlatform_name(map.get("platform"));
							info.setCust_account_id(Integer.valueOf(map.get("accountId")));
							info.setCust_account_name("ClothingDemo");
							info.setCust_keyword_id("12356272");
							info.setCust_keyword_type("egoodsid");
							info.setFeature_1(map.get("feature_1"));
							info.setCrawling_status(1);  
							info.setCrawling_fre("hourly");
							info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setAlways_abnormal("0");
							info.setIs_presell("0");
							info.setCount(1);
							if(Fields.ACCOUNTID_35.equals(map.get("accountId"))){
								if(sellername.equals(Fields.ZARA_STORE)){
									list_info=crawlerPublicClassMapper.Craw_keywords_Info(Integer.valueOf(map.get("accountId")),egoodsid);	
									if(list_info.size()>0){  
										logger.info("数据已经存在不需要插入");
									}else{
										crawlerPublicClassMapper.keywordInsertObject(info);
									}	
								}
								
							}else{
								list_info=crawlerPublicClassMapper.Craw_keywords_Info(Integer.valueOf(map.get("accountId")),egoodsid);
								if(list_info.size()>0){  
									logger.info("数据已经存在不需要插入");
								}else{
									crawlerPublicClassMapper.keywordInsertObject(info);
								}
							}
						} 
					}
				}
			}
		} catch (Exception e) {
			logger.info("搜索商品失败-----------------------------"+e.toString());
		} 
	}
	public void keywords_ListData(String accountId){
    //对比生产表数据是否存在
		crawlerPublicClassMapper.updateCraw_keywords_info(Integer.valueOf(accountId),1);//1生产2临时
		crawlerPublicClassMapper.updateCraw_keywords_info(Integer.valueOf(accountId),2);//生产
		List<Craw_keywords_Info>list_info=crawlerPublicClassMapper.queryList(Integer.valueOf(accountId));//获取当前存在的商品
		for(int i=0;i<list_info.size();i++){ 
		Craw_keywords_Info info=new Craw_keywords_Info();
		info.setCust_keyword_name(list_info.get(i).getCust_keyword_name());
		info.setPlatform_name(list_info.get(i).getPlatform_name());
		info.setCust_account_id(list_info.get(i).getCust_account_id());
		info.setCust_account_name(list_info.get(0).getCust_account_name());
		info.setCust_keyword_id(list_info.get(i).getCust_keyword_id());
		info.setCust_keyword_type(list_info.get(i).getCust_keyword_type());
 		info.setFeature_1(list_info.get(i).getFeature_1());
		info.setCrawling_status(list_info.get(i).getCrawling_status());
		info.setCrawling_fre(list_info.get(i).getCrawling_fre());
		info.setUpdate_date(list_info.get(i).getUpdate_date());
		info.setUpdate_time(list_info.get(i).getUpdate_time());
		info.setAlways_abnormal(list_info.get(i).getAlways_abnormal());
		info.setIs_presell(list_info.get(i).getIs_presell());
		info.setCount(2);
		List<Craw_keywords_Info>list=crawlerPublicClassMapper.keywords_Info_query(Integer.valueOf(accountId), list_info.get(i).getCust_keyword_name());
		if(list.size()>0){//判断商品是否存在如果存在更新状态不存在就插入
			crawlerPublicClassMapper.updatekeyordInfo(list_info.get(i).getCust_keyword_name());
		}else{
			crawlerPublicClassMapper.keywordInsertObject(info);	
		}
		
		}
		//删除临时表数据
		crawlerPublicClassMapper.delete_Craw_keywords_temp_Info_forsearch(Integer.valueOf(accountId));
	}
	
	/**  
	* @Title: craw_Access_keyword_query  
	* @Description: TODO(根据关键词搜索商品EgoodsId)  
	* @param @param accountId
	* @param @return    设定文件  
	* @return List<Craw_Access_keyword>    返回类型  
	* @throws  
	*/  
	public List<Craw_Access_keyword>craw_Access_keyword_query(String  accountId){
		return crawlerPublicClassMapper.craw_Access_keyword_query(accountId);
		
	} 
	
	/**
	 * @throws Exception   
	* @Title: queryKeywordData  
	* @Description: TODO查询详情
	* @param @return    设定文件  
	* @return List<Craw_customerweb_categorys_info>    返回类型  
	* @throws  
	*/  
	public List<Craw_customerweb_categorys_info>queryKeywordData(String userId) throws Exception{
		String url="";String message="";String category="";
		List<Craw_customerweb_categorys_info>list=new ArrayList<Craw_customerweb_categorys_info>();
		for(int i=0;i<4;i++){
			if(i==0){
				url=Fields.MEN_URL ;
				category=Fields.MEN; 
			}else if(i==1){
				url=Fields.MS_URL ;
				category=Fields.MS;
			}else if(i==2){
				url=Fields.CHILDREN_URL;
				category=Fields.CHILDREN;
			}else if(i==3){
				url=Fields.TRF_URL;
				category=Fields.TRF;
			} 
			message=httpClientService.getJsonObj(url);
			if(Validation.isEmpty(message)){
				Map<String,String > mp = new HttpClientLMT(null).request(url,"UTF-8",null, null);
				message=mp.get("result").toString();
			} 		
			if(!Validation.isEmpty(message)){
				String temp = message.substring(message.indexOf("{"), message.lastIndexOf("}") + 1);
				JSONObject jsonObject = JSONObject.fromObject(temp);
				System.out.println(jsonObject);
				JSONArray jsonList = jsonObject.getJSONArray("facets");
				JSONObject itemJson = JSONObject.fromObject(jsonList.toArray()[0].toString());
				JSONArray json = itemJson.getJSONArray("values");
				for(int ii=0;ii<json.size();ii++){  
					JSONObject object = JSONObject.fromObject(json.toArray()[ii].toString());
					if(category.equals(object.get("value"))){
						JSONArray subcategories = object.getJSONArray("subcategories");	
						for(int k=0;k<subcategories.size();k++){
							JSONObject sub = JSONObject.fromObject(subcategories.toArray()[k].toString());
							int count=0;
							int a=Integer.valueOf(sub.get("count").toString());
								if(a>512){
									if(a%2!=0){
										count=a/2+1;	
									}else{
										count=a/2;
									}
								for(int is=0;is<2;is++){ 
									String urls="";
									Craw_customerweb_categorys_info info=new Craw_customerweb_categorys_info();	
									info.setUserid(Integer.valueOf(userId));
									info.setSeries(category);
									info.setCategories(sub.get("value").toString());
									if(is==0){
										info.setSkus(String.valueOf(count));
										urls="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111207381440113544226_1510817130631&o=json&m=24&q="+URLEncoder.encode(category, "UTF-8")+"&filter="+URLEncoder.encode(sub.get("filter").toString(), "UTF-8")+"&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows="+count+"&user=3323ac02-b022-4126-8c5c-1eb8fbac393c&_=1510817130641";
									}else{
										info.setSkus(String.valueOf(count*2));
										urls="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111207381440113544226_1510817130631&o=json&m=24&q="+URLEncoder.encode(category, "UTF-8")+"&filter="+URLEncoder.encode(sub.get("filter").toString(), "UTF-8")+"&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start="+count+1+"&rows="+count*2+"&user=3323ac02-b022-4126-8c5c-1eb8fbac393c&_=1510817130641";
									}
									info.setWebsite_url(urls);  
									list.add(info);	
								} 
							}else{
								String urls="https://api.empathybroker.com/search/v1/query/zara/search?jsonCallback=jQuery111207381440113544226_1510817130631&o=json&m=24&q="+URLEncoder.encode(category, "UTF-8")+"&filter="+URLEncoder.encode(sub.get("filter").toString(), "UTF-8")+"&scope=default&t=*&lang=zh_CN&store=11716&catalogue=21551&warehouse=14551&start=0&rows="+sub.get("count").toString()+"&user=3323ac02-b022-4126-8c5c-1eb8fbac393c&_=1510817130641";
								Craw_customerweb_categorys_info info=new Craw_customerweb_categorys_info();
								info.setUserid(Integer.valueOf(userId));
								info.setSeries(category);
								info.setCategories(sub.get("value").toString()); 
								info.setSkus(sub.get("count").toString());
								info.setWebsite_url(urls); 
								list.add(info);
							}
						}
						crawlerPublicClassMapper.InsetCustomerweb_categorys_info(list); 
					}  
				}
			}
		}
		List<Craw_customerweb_categorys_info>list_info=crawlerPublicClassMapper.AaraGetproductdetails();//获取详情
		return list_info;

	}
}
