package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Jd_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.HttpCrawlerUtil;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class Jd_DataCrawlTask implements Callable<Map<String, Object>> {
    Logger logger = Logger.getLogger(Jd_DataCrawlTask.class);
    @Autowired
    CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    HttpClientService httpClientService;
    @Autowired
    private Jd_DataCrawlService jd_DataCrawlService;
    @Autowired
    private WhetherShelves whetherShelves;
    private CommodityPrices commodityPricesJD;
    private CrawKeywordsInfo crawKeywordsInfo;
    private Craw_goods_Info craw_goods_Info;
    private Map<String, Object> jdMap;
    private String page;
    private String accountId;
    private String dataType;
    private String setUrl;
    private String IPPROXY;
    private String Sum;
    private String timeDate;
    private String type;
    private String cookie;
    private String cityCode;
    private String place_code;
    private String status_type;
    private String proxy;
    private String database;
    private String storage;
    public Map<String, Object> call() throws Exception {//call解析数据
    	synchronized(this) {
    	Map<String, String> Map = new HashMap<String, String>();
    	Map = publicClass.parameterMap(setUrl, accountId, dataType, timeDate, IPPROXY);
    	Map.put("proxy",proxy);
    	Map.put("cityCode",cityCode);
    	Map.put("accountId",accountId);
    	Map.put("timeDate",timeDate);
    	Map.put("dataType",dataType);
    	Map.put("storage",storage);
    	Map.put("cookie",cookie);
    	Map.put("status_type",status_type);
    	Map.put("database", database);
    	Map.put("page",String.valueOf(page));
    	Map.put("database", database);
    	Map.put("place_code", place_code);
    	Map.put("delivery", jdMap.get("delivery").toString());
    	if(crawKeywordsInfo!=null){
    		Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());		
		}else{
			Map.put("keywordId",commodityPricesJD.getCust_keyword_id());
		}
    	logger.info(">>> Account ID : " + accountId + " Current URL : " + setUrl + ">>>");
    	if (!Validation.isEmpty(cookie)) {
    		if (StringUtils.isNotEmpty(place_code)) {
    			String code = StringHelper.getResultByReg(place_code, "areaId=([0-9]+);");
    			Map.put("url", Fields.JD_INVENTORY_HK + crawKeywordsInfo.getCust_keyword_name() + "&provinceId=" + code + "");
    			Map.put("urlRef", setUrl);
    		}
    	}else {
    		Map.put("cookie", place_code);
    		//Map.put("cookie", cookie.replaceAll("\"", "\'"));
    		//Map.put("cookile", "shshshfpa=302dc358-c830-2c7f-b7bb-b8ba3bb417f7-1603179150; __jdu=16031635450441281671470; areaId=2; ipLoc-djd=2-2841-51980-0; shshshfpb=daMalnW82h60jM%20wdtGDr2Q%3D%3D; commonAddress=0; regionAddress=2%2C78%2C51978%2C0; unpl=V2_ZzNtbUJfRRYmARNSfU5ZVmIHFl0RUkVGcgoRAyhKDlJlBBReclRCFnQURlVnGl0UZwQZWEZcRhVFCEVkexhdBWMBFF5EUXMlRQtGZHopXAJmARJaQFNGEXQNQ1R4EFwCYQMXW0NncxJ1AXZkM10DUDlcRF5DV0sldQhCVH8ZbARXAxpYQ15EEncOQWQwd11IZwQTX0JQQRFwDEdRfhlfDGcEFF1HUUIldDhF; __jdv=76161171|kong|t_1001537277_|tuiguang|1972b8d77f4b4551b47b63ffbbcf3672|1603183541417; PCSYCityID=undefined_undefined_undefined_undefined; wxa_level=1; retina=0; cid=9; jxsid=16031852953174596200; webp=1; mba_muid=16031635450441281671470; visitkey=17187629239191604; autoOpenApp_downCloseDate_auto=1603185296780_21600000; 3AB9D23F7A4B3C9B=DHR7KQ256S7HZIJLC2ZUD5FWXAD2FEE2JUD4Q33KYOZZYFQQKFSIWXS2NVQMVUJVIJYINZ6U6WQ7WJ3DNQIFTU222U; sc_width=1920; __jdc=122270672; __jda=122270672.16031635450441281671470.1603163545.1603183541.1603189681.5; warehistory=\"885162,19037330024,12943831131,13354613071,\"; jxsid_s_u=https%3A//item.m.jd.com/product/885162.html; shshshfp=3d7c11cd67f3418c3b9c95d7c33c65e2; sk_history=885162%2C19037330024%2C; wq_addr=0%7C2_78_51978_0%7C%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_%7C%7C; jdAddrName=%u4E0A%u6D77_%u9EC4%u6D66%u533A_%u57CE%u533A_; jdAddrId=2_78_51978_0; mitemAddrName=; mitemAddrId=2_78_51978_0; wq_logid=1603189750.1564841616; wqmnx1=MDEyNjM3NHNtY2Q1bTg5TS9pTjs7QWI3SGlrbzAxYS4zMlVCMlJJKikl; __jdb=122270672.3.16031635450441281671470|5.1603189681; mba_sid=16031897172306889263506457162.2; __jd_ref_cls=MDownLoadFloat_FloatShield; __wga=1603189751796.1603189717945.1603185296212.1603185296212.2.2; PPRD_P=UUID.16031635450441281671470-LOGID.1603189751833.1168568727; jxsid_s_t=1603189751897; shshshsID=3ae5fdf31d3e00f4746c97dc777a820a_3_1603189753069");
    	}
    	
    	if (Fields.STYPE_1.equals(type)) {//解析数据
    		logger.info("开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try { 
				if(status_type.equals(Fields.COUNT_4)){
					Map<String, Object> message=parseData(Map, place_code, crawKeywordsInfo,null);//解析数据
					if(StringUtil.isNotEmpty(message.toString())){
						jdMap.putAll(message);	
					}
//					if(message.toString().contains("present")){
//						if(StringUtil.isNotEmpty(message.get("present").toString())){
//							if(status_type.equals(Fields.COUNT_4)){
//								message=parseData(Map, place_code, crawKeywordsInfo,message.get("present").toString());//解析数据
//							}
//						}	
//					} 	
				}else{//不抓取赠品
					Map<String, Object> message=parsePortData(Map, place_code, crawKeywordsInfo,null);//解析数据
					if(StringUtil.isNotEmpty(message.toString())){
						jdMap.putAll(message);	
					}	
				}
				 
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("解析京东数据失败-"+e);
			}
    	} else if (Fields.STYPE_6.equals(type)) {//pc价格抓取
    		try {
    			logger.info(">>> start JDparseItemPagePrice >>>");
    			Craw_goods_Price_Info price = jd_DataCrawlService.JDparseItemPagePrice(commodityPricesJD, accountId, IPPROXY,database);//PC端价格
    			if (StringUtil.isNotEmpty(price.getCurrent_price())) {
    				jdMap.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
    				Map.put("egoodsId", commodityPricesJD.getEgoodsId());
    				jdMap.putAll(Map);
    			}
    			logger.info(">>> " + setUrl + ": Analysis JD Price Data finish >>>");
    		} catch (Exception e) {
    			logger.error(">>> " + setUrl + ": Analysis JD Price Data Fail,please check! >>>");
    			logger.error(e.getMessage());
    		}
    	  }
    	}
    	return jdMap;
    }
    
  //抓取商品详情
  	public Map<String, Object>parsePortData(Map<String,String>Map,String place_code,CrawKeywordsInfo crawKeywordsInfo,String present) throws Exception{
  		Map<String, Object> parseDataMap=new HashMap<String, Object>(); String StringMessage="";
  		Map.put("url",Fields.JD_INVENTORY_WQITEM.replace("EGOODSID", crawKeywordsInfo.getCust_keyword_name()));
  		Map.put("urlRef", setUrl);	
  		String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_JD);//生成goodSId
  		Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
  		Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
  		Map.put("goodsId", goodsId);
//  		if(Validation.isNotEmpty(place_code)){
//  			Map.put("cookie", place_code); 	
//  		}
//  		StringMessage=httpClientService.interfaceSwitch(Map.get("proxy"), Map,Map.get("database").toString());//请求数据
  		
  		Map<String,String> param=new HashMap<String,String>();
  		param.put("referer", setUrl);
  		param.put("User-Agent",HttpCrawlerUtil.CrawlerAttribute());
  		
  		if(Validation.isNotEmpty(place_code)){
  			param.put("cookie", place_code);
  		}
  		String productUrl=Map.get("url");
  		StringMessage=httpClientService.getOkHttpClient(productUrl,param);
  		
  		Map<String, Object>data=insertParsePortData(Map, place_code, crawKeywordsInfo,StringMessage);
  		parseDataMap.putAll(data);
  		return jdMap;
  	}
    /**
	 * @throws Exception   
	 * @Title: parseData  
	 * @Description: TODO(请求数据)  
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings("unused")
	public Map<String, Object>  parseData(Map<String,String>Map,String place_code,CrawKeywordsInfo crawKeywordsInfo,String present) throws Exception{
		Map<String, Object> parseDataMap=new HashMap<String, Object>(); 	String StringMessage="";
		try{
//			if(StringUtil.isNotEmpty(present)){
//				if(present.contains(",")){
//					String presentData[]=present.split(",");
//					for(int ii=0;ii<presentData.length; ii++){
//						crawKeywordsInfo.setCust_keyword_name(presentData[ii].trim().toString());
//						String goodsId=StringHelper.encryptByString(presentData[ii].trim().toString()+Fields.PLATFORM_JD);//生成goodSId
//						Map.put("url",Fields.JD_URL_APP+presentData[ii].trim().toString()+".html");
//						Map.put("goodsId", goodsId);
//						Map.put("egoodsId", present);
//						StringMessage=httpClientService.interfaceSwitch(Map.get("proxy"), Map,Map.get("database").toString());//请求数据
//						Map<String, Object>data=insertParseData(Map, place_code, crawKeywordsInfo,StringMessage);
//						jdMap.putAll(data);
//						return jdMap;
//					} 
//				}else{
//					crawKeywordsInfo.setCust_keyword_name(present);
//					String goodsId=StringHelper.encryptByString(present+Fields.PLATFORM_JD);//生成goodSId
//					Map.put("url",Fields.JD_URL_APP+present.toString()+".html");
//					Map.put("goodsId", goodsId);
//					Map.put("egoodsId", present);
//					StringMessage=httpClientService.interfaceSwitch(Map.get("proxy"),Map,Map.get("database").toString());//请求数据
//					Map<String, Object>data=insertParseData(Map, place_code, crawKeywordsInfo,StringMessage);
//					jdMap.putAll(data); 
//					return jdMap;
//				}
//			}else{
				if(!setUrl.contains("mitem.jd.hk")){//正常抓取数据
					if(StringUtils.isNotEmpty(place_code)){
						//String code=StringHelper.getResultByReg(place_code,"areaId=([0-9]+);");//JD_INVENTORY_DOMESTIC &provinceId="+jdAddrId+"
						//String jdAddrId=StringHelper.getResultByReg(place_code,"jdAddrId=([^<>\"]+)");//.replace("AREA",jdAddrId)
						Map.put("url",Fields.JD_INVENTORY_WQITEM.replace("EGOODSID", crawKeywordsInfo.getCust_keyword_name()));
						Map.put("urlRef", setUrl);	
						//Map.put("cookie", "__jdu=1832750257; shshshfpb=189ddd26536874c1a925d2447308e86ee17e8ba21dab6e1775affa34c0; shshshfpa=974262c3-38ec-eccc-b658-33a1bd1ccb56-1531230616; SERVERID=200afdc3641ff4e28453c7b915b267b4; unpl=V2_ZzNtbUoDEUEmDkUEfxpZAWICGg1LX0BGIg5HB3IbVFIyCkdaclRCFXwUR1xnGVUUZAIZXUpcRxRFCHZXchBYAWcCGllyBBNNIEwHDCRSBUE3XHwNRAAXEHYIQAEvSg4ANAISVUtVRBB2WBRcKxpfUGcLR21yVEMldQl2VHIbXwNjAhFZRGdzEkU4dlN5H18MYjMTbUNnAUEpCkBRcx4RBW4BEVtGVkARczhHZHg%3d; __jda=122270672.1832750257.1531230303.1536391451.1537273191.4; __jdc=122270672; __jdv=122270672|baidu-pinzhuan|t_288551095_baidupinzhuan|cpc|a7fe4217debc4b01983642ac9a22d19d_0_9eceb72a5245409a892bf70b839fd8d6|1537273191277; PCSYCityID=1827; ipLoc-djd=1-72-2799-0; 3AB9D23F7A4B3C9B=CTSPUM4EZ75G5URBVUE664FYJIW2YV7I5NYWLID4RRYGRH65MHOKQBPRTAS5YTK5HDB3TFWNKD2XQ4SVUML5RHHGAM; wxa_level=1; retina=1; cid=9; webp=1; mba_muid=1832750257; sc_width=375; wq_area=2_2824_0%7C; shshshfp=30b92888abf3c65c5fcdc149e936f365; visitkey=25105464066895245; mobilev=html5; sid=3d93ec2e0adb4f374f07791c3e658524; __jdb=122270672.8.1832750257|4.1537273191; mba_sid=15372732294517304827284604616.5; __wga=1537273320406.1537273229553.1537273229553.1537273229553.5.1; PPRD_P=UUID.1832750257-LOGID.1537273320433.320367391; shshshsID=e7714af8ca841767b76d40f974d19655_13_1537273321357; wq_addr=0%7C2_2817_51973_0%7C%u4E0A%u6D77_%u9759%u5B89%u533A_%u57CE%u533A_%7C%7C; jdAddrId=2_2817_51973_0; jdAddrName=%u4E0A%u6D77_%u9759%u5B89%u533A_%u57CE%u533A_; mitemAddrId=2_2817_51973_0; mitemAddrName=; wq_logid=1537273339.1475626333");
						Map.put("cookie", place_code); 	
					} 
				}
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_JD);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				StringMessage=httpClientService.interfaceSwitch(Map.get("proxy"), Map,Map.get("database").toString());//请求数据
				Map<String, Object>data=insertParseData(Map, place_code, crawKeywordsInfo,StringMessage);
				jdMap.putAll(data);
				return jdMap;
			//}
		}catch(Exception e){
			logger.error("解析京东数据失败-"+e.toString());
		}
		return jdMap;
	}
	/**
	 * @throws Exception 
	 * @throws Exception   
	 * @Title: parseData  
	 * @Description: TODO(解析数据)  
	 * @param     设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	public Map<String, Object>insertParseData(Map<String,String>Map,String place_code,CrawKeywordsInfo crawKeywordsInfo,String StringMessage) throws Exception{
		Map<String, String> itemFieldsJD = null;
		Map<String, Object> item = new HashMap<String, Object>();
		if(StringMessage.contains(Fields.RETURNURL)){
			Map.put("cookie", crawlerPublicClassService.dataCookie(Fields.ACCOUNTID_28,Map.get("database"),crawKeywordsInfo.getPlatform_name()).replaceAll("\"","\'"));
			Map.put("url", Fields.JD_URL_APP_INTERNATIONL+crawKeywordsInfo.getCust_keyword_name());
			StringMessage=httpClientService.interfaceSwitch(Map.get("proxy"), Map,Map.get("database"));//请求数据
			Map.put("cookie",null);
		}
		String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
		if(status.contains("0")) {
			crawKeywordsInfo.setCrawling_fre(Map.get("delivery"));
		}
		Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,Map.get("database"),crawKeywordsInfo,Map.get("goodsId").toString(),Map.get("timeDate"),Map.get("dataType"),Map.get("url"),Map.get("storage"));
		if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
			item.putAll(sum_status);
			logger.info(crawKeywordsInfo.getPlatform_name() + " >>> The goods have been removed from the shelves_"
					+ Map.get("egoodsId") + " SOLD OUT : >>>>>>>>>>>>>>>>>>>>>>>" + Sum + ">>>");
			return item; 
		}
		if(!Validation.isEmpty(cookie)){

			if(StringUtils.isNotEmpty(place_code)){
				itemFieldsJD=jd_DataCrawlService.JDparseItemPageInventory(StringMessage,Map);
			}else{
				itemFieldsJD =jd_DataCrawlService.JDparseItemPage(StringMessage,Map);//解析数据  	
			}
		}else if(StringUtils.isNotEmpty(Map.get("cityCode"))){
			itemFieldsJD =jd_DataCrawlService.JDparseItemPageByCity(Map,Map.get("cityCode"),StringMessage);//解析数据
		}else{
			if(!setUrl.contains("mitem.jd.hk")){
				if(StringUtils.isNotEmpty(place_code)){
					itemFieldsJD=jd_DataCrawlService.JDparseItemPageInventory(StringMessage,Map);
				}else{
					itemFieldsJD =jd_DataCrawlService.JDparseItemPage(StringMessage,Map);//解析数据  	
				}
			}else{
				itemFieldsJD =jd_DataCrawlService.JDparseItemPage(StringMessage,Map);//解析数据  
			}
		}
		Map.putAll(itemFieldsJD);
		if(itemFieldsJD.toString().contains("currentPrice")){
			if(!StringUtil.isEmpty(itemFieldsJD.get("currentPrice").toString())){
				Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(Map,Map.get("database"),Map.get("storage")); //商品详情 
				//Map<String, String> map,String keywordId,String  goodsId,String batch_time,String platform,int pageTop,String database,String storage
				 Craw_goods_Price_Info price = crawlerPublicClassService.parseItemPrice(Map, crawKeywordsInfo.getCust_keyword_id(),Map.get("goodsId").toString(),Map.get("timeDate"),Map.get("dataType"), Fields.STATUS_COUNT_1,Map.get("database"),Map.get("storage"));
				try{
					if(StringUtil.isNotEmpty(info.getPlatform_goods_name())){
						item.putAll(sum_status);
						item.put(Fields.TABLE_CRAW_GOODS_INFO, info);
						item.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
						item.putAll(Map);
						logger.info(" >>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>>>>>>>>>>>>>>>>>"+Sum);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}	
		}
		return item;	
	}
	
	public Map<String, Object>insertParsePortData(Map<String,String>Map,String place_code,CrawKeywordsInfo crawKeywordsInfo,String StringMessage) throws Exception{
		Map<String, String> itemFieldsJD = null;
		Map<String, Object> item = new HashMap<String, Object>();
	    try {
	    	String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
			Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,Map.get("database").toString(),crawKeywordsInfo,Map.get("goodsId").toString(),timeDate,dataType,setUrl,storage);
			if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
				item.putAll(sum_status);
				logger.info(crawKeywordsInfo.getPlatform_name() + " >>> The goods have been removed from the shelves_"
						+ Map.get("egoodsId") + " SOLD OUT : >>>>>>>>>>>>>>>>>>>>>>>" + Sum + ">>>");
				return item; 
			}
			itemFieldsJD=jd_DataCrawlService.JDparseItemPageInventory(StringMessage,Map);
			Map.putAll(itemFieldsJD);
			if(itemFieldsJD.toString().contains("currentPrice")){
				if(!StringUtil.isEmpty(itemFieldsJD.get("currentPrice").toString())){
					Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(Map,database,storage); //商品详情 
					Craw_goods_Price_Info price = crawlerPublicClassService.parseItemPrice(Map, crawKeywordsInfo.getCust_keyword_id(),Map.get("goodsId").toString(),Map.get("timeDate"),Map.get("dataType"), Fields.STATUS_COUNT_1,Map.get("database"),Map.get("storage"));
					try{
						if(StringUtil.isNotEmpty(info.getPlatform_goods_name())){
							item.putAll(sum_status);
							item.put(Fields.TABLE_CRAW_GOODS_INFO, info);
							item.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
							item.putAll(Map);
							logger.info(" >>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>>>>>>>>>>>>>>>>>"+Sum);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}	
			}	
		} catch (Exception e) {
			logger.error("请求数据失败。。。。。。。。"+e);
		}
		
		return item;	
	}
    public CrawKeywordsInfo getCrawKeywordsInfo() {
        return crawKeywordsInfo;
    }

    public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
        this.crawKeywordsInfo = crawKeywordsInfo;
    }

    public String getSum() {
        return Sum;
    }

    public void setSum(String sum) {
        Sum = sum;
    }

    public String getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CrawlerPublicClassService getCrawlerPublicClassService() {
        return crawlerPublicClassService;
    }

    public void setCrawlerPublicClassService(
            CrawlerPublicClassService crawlerPublicClassService) {
        this.crawlerPublicClassService = crawlerPublicClassService;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getSetUrl() {
        return setUrl;
    }

    public void setSetUrl(String setUrl) {
        this.setUrl = setUrl;
    }

    public String getIPPROXY() {
        return IPPROXY;
    }

    public void setIPPROXY(String iPPROXY) {
        IPPROXY = iPPROXY;
    }

    public CommodityPrices getCommodityPricesJD() {
        return commodityPricesJD;
    }

    public void setCommodityPricesJD(CommodityPrices commodityPricesJD) {
        this.commodityPricesJD = commodityPricesJD;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPlace_code() {
        return place_code;
    }

    public void setPlace_code(String place_code) {
        this.place_code = place_code;
    }

    public Craw_goods_Info getCraw_goods_Info() {
        return craw_goods_Info;
    }

    public void setCraw_goods_Info(Craw_goods_Info craw_goods_Info) {
        this.craw_goods_Info = craw_goods_Info;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStatus_type() {
        return status_type;
    }

    public void setStatus_type(String status_type) {
        this.status_type = status_type;
    }
	public String getProxy() {
		return proxy;
	}
	public void setProxy(String proxy) {
		this.proxy = proxy;
	}
	public Map<String, Object> getJdMap() {
        return jdMap;
    }

    public void setJdMap(Map<String, Object> jdMap) {
        this.jdMap = jdMap;
    }
    public String getDatabase() {
        return database;
    }
    public void setDatabase(String database) {
        this.database = database;
    }

	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}

	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}
    
}
