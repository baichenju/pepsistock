/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午4:00:14 
 */
package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Vip_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Vip_DataCrawlTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午4:00:14   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午4:00:14   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class Vip_DataCrawlTask implements Callable<Map<String, Object>> {
	Logger logger = Logger.getLogger(Vip_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private Vip_DataCrawlService vip_DataCrawlService;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>vipMap;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum; 
	private String timeDate;
	private String type;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() {
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("唯品会平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try {
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_VIP);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				String status =whetherShelves.parseGoodsStatusVip(StringMessage);//判断商品是否下架
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					vipMap.putAll(sum_status);
					return vipMap; 
				}
				Map<String, Object> dataVip=vip_DataCrawlService.vipParseItemPage(StringMessage,crawKeywordsInfo,Map,database,storage);//解析数据
				if(dataVip.toString().contains("goodsName")){
					vipMap.putAll(dataVip);
				}	
			} catch (Exception e) { 
				logger.info("解析唯品会数据失败-"+e.getMessage()+SimpleDate.SimpleDateFormatData().format(new Date()));
			}

		}else if(Fields.STYPE_6.equals(type)){
			try {
				Thread.sleep(1000);
				Map.put("cookie","wap_consumer=A1; WAP[p_area]=%25E4%25B8%258A%25E6%25B5%25B7; m_vip_province=103101; vip_first_visitor=1; vip_ipver=31; _smt_uid=5a17acb2.2f6af19c; vipte_viewed_vip_ht_sh=317821761; vipte_viewed_vip_nh=335841733; WAP[p_wh]=VIP_SH; warehouse=VIP_SH; vipte_viewed_vip_sh=211587640%2C335411897%2C335411900%2C335411909%2C335411921; vipAc=8f752d3cd4a214b99ea4efcec21fca4e; VipDFT=0; user_class=a; VipUINFO=luc%3Aa%7Csuc%3Aa%7Cbct%3Ac_new%7Chct%3Ac_new%7Cbdts%3A0%7Cbcts%3A0%7Ckfts%3A0%7Cc10%3A0%7Crcabt%3A0%7Cp2%3A0%7Cp3%3A1%7Cp4%3A0%7Cp5%3A0; vip_province=103101; vip_wh=VIP_SH; vipte_viewed_vip_ht_hz=270863159%2C270863155; mars_pid=208; mars_cid=1511500544993_51d12e9779dbe3c1d66affeac21fa7e3; mars_sid=e908667511390b422556a01b9d389436; visit_id=288EB5B62257C6981E6160FFADF85493; _jzqco=%7C%7C%7C%7C%7C1.1321206542.1511500977804.1511511961487.1511512321081.1511511961487.1511512321081..0.0.36.36");
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				String AppMessage =httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				commodityPrices.setBatch_time(timeDate);//重新赋值
				Map<String, Object> vipdata=vip_DataCrawlService.ResolutionCellPrices(commodityPrices, AppMessage);//解析数据
				if(vipdata.toString().contains("currentPrice")){
					vipMap.putAll(vipdata);
				}
			} catch (InterruptedException e) {
				logger.info("请求唯品会PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			} catch (Exception e) {
				logger.info("请求唯品会PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}
		return vipMap; 
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public String getSum() {
		return Sum;
	}
	public void setSum(String sum) {
		Sum = sum;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getVipMap() {
		return vipMap;
	}
	public void setVipMap(Map<String, Object> vipMap) {
		this.vipMap = vipMap;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}

}
