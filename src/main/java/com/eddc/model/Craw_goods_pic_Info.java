/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年8月15日 下午2:00:37 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_pic_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年8月15日 下午2:00:37   
* 修改人：jack.zhao   
* 修改时间：2018年8月15日 下午2:00:37   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_pic_Info implements Serializable{
private int cust_keyword_id;
private String goodsid;
private String pic_url;
private String pic_size;
private String update_time;
private String update_date;
private String batch_time;
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsid
 */
public String getGoodsid() {
	return goodsid;
}
/**
 * @param goodsid the goodsid to set
 */
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
/**
 * @return the pic_url
 */
public String getPic_url() {
	return pic_url;
}
/**
 * @param pic_url the pic_url to set
 */
public void setPic_url(String pic_url) {
	this.pic_url = pic_url;
}
/**
 * @return the pic_size
 */
public String getPic_size() {
	return pic_size;
}
/**
 * @param pic_size the pic_size to set
 */
public void setPic_size(String pic_size) {
	this.pic_size = pic_size;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}

}
