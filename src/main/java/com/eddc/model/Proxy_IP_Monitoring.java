/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年6月26日 上午11:51:01 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
 *    
 * 项目名称：selection_sibrary_crawler   
 * 类名称：Proxy_IP_Monitoring   
 * 类描述：  代理监控 
 * 创建人：jack.zhao   
 * 创建时间：2018年6月26日 上午11:51:01   
 * 修改人：jack.zhao   
 * 修改时间：2018年6月26日 上午11:51:01   
 * 修改备注：   
 * @version    
 *    
 */
public class Proxy_IP_Monitoring implements Serializable {
	private String platform;
	private String agent;
	private String update_date;
	private String update_time;
	private int status;
	private String code;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}
	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}
	/**
	 * @return the update_date
	 */
	public String getUpdate_date() {
		return update_date;
	}
	/**
	 * @param update_date the update_date to set
	 */
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	/**
	 * @return the update_time
	 */
	public String getUpdate_time() {
		return update_time;
	}
	/**
	 * @param update_time the update_time to set
	 */
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
  

}
