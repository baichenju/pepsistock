/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月10日 下午5:03:00 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_category_info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月10日 下午5:03:00   
* 修改人：jack.zhao   
* 修改时间：2018年5月10日 下午5:03:00   
* 修改备注：   
* @version    
*    
*/
public class Craw_aliindex_category_info implements Serializable {
private int category_id;//类别id
private String category_name;//类别名称 例如 家用游戏机
private int parent_id;//父类id
private int category_level;//类别等级
private  int has_child;//是否有子类  1表示有 0表示无
private String category_path;//组合类别id 例如
private  int category_status;//---类别状态  0表示未删除
private String platform_name;//平台 index1688
private String insert_time;//插入时间
private String update_time;//更新时间
/**
 * @return the category_id
 */
public int getCategory_id() {
	return category_id;
}
/**
 * @param category_id the category_id to set
 */
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}
/**
 * @return the category_name
 */
public String getCategory_name() {
	return category_name;
}
/**
 * @param category_name the category_name to set
 */
public void setCategory_name(String category_name) {
	this.category_name = category_name;
}
/**
 * @return the parent_id
 */
public int getParent_id() {
	return parent_id;
}
/**
 * @param parent_id the parent_id to set
 */
public void setParent_id(int parent_id) {
	this.parent_id = parent_id;
}
/**
 * @return the category_level
 */
public int getCategory_level() {
	return category_level;
}
/**
 * @param category_level the category_level to set
 */
public void setCategory_level(int category_level) {
	this.category_level = category_level;
}
/**
 * @return the has_child
 */
public int getHas_child() {
	return has_child;
}
/**
 * @param has_child the has_child to set
 */
public void setHas_child(int has_child) {
	this.has_child = has_child;
}
/**
 * @return the category_path
 */
public String getCategory_path() {
	return category_path;
}
/**
 * @param category_path the category_path to set
 */
public void setCategory_path(String category_path) {
	this.category_path = category_path;
}
/**
 * @return the category_status
 */
public int getCategory_status() {
	return category_status;
}
/**
 * @param category_status the category_status to set
 */
public void setCategory_status(int category_status) {
	this.category_status = category_status;
}
/**
 * @return the platform_name
 */
public String getPlatform_name() {
	return platform_name;
}
/**
 * @param platform_name the platform_name to set
 */
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
/**
 * @return the insert_time
 */
public String getInsert_time() {
	return insert_time;
}
/**
 * @param insert_time the insert_time to set
 */
public void setInsert_time(String insert_time) {
	this.insert_time = insert_time;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}

}
