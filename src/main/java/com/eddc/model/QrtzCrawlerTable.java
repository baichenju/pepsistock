package com.eddc.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class QrtzCrawlerTable implements Serializable {
private int id;
private String user_Id;
private String job_name;
private String platform;
private String IP;
private String dataSource;
private String data_time;
private String status;
private String cronEx_expression;
private int thread_sum;
private String docker;
private String number;
private String describe;
private String inventory;
private String client;
private int   Promotion_status;
private int   price_status;
private int   inventory_status;
private String databases;
private String storage;
private int sums;
public int getSums() {
	return sums;
}
public void setSums(int sums) {
	this.sums = sums;
}
public int getPromotion_status() {
	return Promotion_status;
}
public void setPromotion_status(int promotion_status) {
	Promotion_status = promotion_status;
}
public int getPrice_status() {
	return price_status;
}
public void setPrice_status(int price_status) {
	this.price_status = price_status;
}
public int getInventory_status() {
	return inventory_status;
}
public void setInventory_status(int inventory_status) {
	this.inventory_status = inventory_status;
}
public String getClient() {
	return client;
}
public void setClient(String client) {
	this.client = client;
}
public String getInventory() {
	return inventory;
}
public void setInventory(String inventory) {
	this.inventory = inventory;
}
public String getDescribe() {
	return describe;
}
public void setDescribe(String describe) {
	this.describe = describe;
}
public String getDocker() {
	return docker;
}
public void setDocker(String docker) {
	this.docker = docker;
}
public int getThread_sum() {
	return thread_sum;
}
public void setThread_sum(int thread_sum) {
	this.thread_sum = thread_sum;
}
public String getCronEx_expression() {
	return cronEx_expression;
}
public void setCronEx_expression(String cronEx_expression) {
	this.cronEx_expression = cronEx_expression;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getUser_Id() {
	return user_Id;
}
public void setUser_Id(String user_Id) {
	this.user_Id = user_Id;
}
public String getJob_name() {
	return job_name;
}

public void setJob_name(String job_name) {
	this.job_name = job_name;
}
public String getPlatform() {
	return platform;
}

public void setPlatform(String platform) {
	this.platform = platform;
}
public String getIP() {
	return IP;
}
public void setIP(String iP) {
	IP = iP;
}
public String getDataSource() {
	return dataSource;
}
public void setDataSource(String dataSource) {
	this.dataSource = dataSource;
}
public String getData_time() {
	return data_time;
}
public void setData_time(String data_time) {
	this.data_time = data_time;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getNumber() {
	return number;
}
public void setNumber(String number) {
	this.number = number;
}

/**
 * @return the databases
 */
public String getDatabases() {
	return databases;
}
/**
 * @param databases the databases to set
 */
public void setDatabases(String databases) {
	this.databases = databases;
}
public String getStorage() {
	return storage;
}
public void setStorage(String storage) {
	this.storage = storage;
}
@Override
public String toString() {
	return "QrtzCrawlerTable [id=" + id + ", user_Id=" + user_Id + ", job_name=" + job_name + ", platform=" + platform
			+ ", IP=" + IP + ", dataSource=" + dataSource + ", data_time=" + data_time + ", status=" + status
			+ ", cronEx_expression=" + cronEx_expression + ", thread_sum=" + thread_sum + ", docker=" + docker
			+ ", number=" + number + ", describe=" + describe + ", inventory=" + inventory + ", client=" + client
			+ ", Promotion_status=" + Promotion_status + ", price_status=" + price_status + ", inventory_status="
			+ inventory_status + ", databases=" + databases + ", storage=" + storage + "]";
}

}
