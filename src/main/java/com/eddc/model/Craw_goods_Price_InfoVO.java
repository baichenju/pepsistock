package com.eddc.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Craw_goods_Price_InfoVO implements Serializable {
private String cust_keyword_id;
private String goodsid;
private String original_price;
private String current_price;
private String update_time;
private String update_date;
private String batch_time;
private String channel;
private String skuId;
private String promotion;
private String delivery_place;


/**
 * @return the delivery_place
 */
public String getDelivery_place() {
	return delivery_place;
}
/**
 * @param delivery_place the delivery_place to set
 */
public void setDelivery_place(String delivery_place) {
	this.delivery_place = delivery_place;
}

/**
 * @return the promotion
 */
public String getPromotion() {
	return promotion;
}
/**
 * @param promotion the promotion to set
 */
public void setPromotion(String promotion) {
	this.promotion = promotion;
}
/**
 * @return the skuId
 */
public String getSkuId() {
	return skuId;
}
/**
 * @param skuId the skuId to set
 */
public void setSkuId(String skuId) {
	this.skuId = skuId;
}
/**
 * @return the channel
 */
public String getChannel() {
	return channel;
}
/**
 * @param channel the channel to set
 */
public void setChannel(String channel) {
	this.channel = channel;
}
/**
 * @return the cust_keyword_id
 */
public String getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsid
 */
public String getGoodsid() {
	return goodsid;
}
/**
 * @param goodsid the goodsid to set
 */
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
/**
 * @return the original_price
 */
public String getOriginal_price() {
	return original_price;
}
/**
 * @param original_price the original_price to set
 */
public void setOriginal_price(String original_price) {
	this.original_price = original_price;
}
/**
 * @return the current_price
 */
public String getCurrent_price() {
	return current_price;
}
/**
 * @param current_price the current_price to set
 */
public void setCurrent_price(String current_price) {
	this.current_price = current_price;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}

}
