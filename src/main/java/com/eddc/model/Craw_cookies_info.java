/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2017年12月12日 下午1:30:47 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Craw_cookies_info   
 * 类描述：   获取商品的Cookie
 * 创建人：jack.zhao   
 * 创建时间：2017年12月12日 下午1:30:47   
 * 修改人：jack.zhao   
 * 修改时间：2017年12月12日 下午1:30:47   
 * 修改备注：   
 * @version    
 *    
 */
public class Craw_cookies_info implements Serializable {
private int id;
private int userid ;
private String  platform_name ;
private String  cookie;
private String  cookie_state;
private String  url;
private String  insert_time;
private String update_time;
/**
 * @return the id
 */
public int getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
	this.id = id;
}
/**
 * @return the userid
 */
public int getUserid() {
	return userid;
}
/**
 * @param userid the userid to set
 */
public void setUserid(int userid) {
	this.userid = userid;
}
/**
 * @return the platform_name
 */
public String getPlatform_name() {
	return platform_name;
}
/**
 * @param platform_name the platform_name to set
 */
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
/**
 * @return the cookie
 */
public String getCookie() {
	return cookie;
}
/**
 * @param cookie the cookie to set
 */
public void setCookie(String cookie) {
	this.cookie = cookie;
}
/**
 * @return the cookie_state
 */
public String getCookie_state() {
	return cookie_state;
}
/**
 * @param cookie_state the cookie_state to set
 */
public void setCookie_state(String cookie_state) {
	this.cookie_state = cookie_state;
}
/**
 * @return the url
 */
public String getUrl() {
	return url;
}
/**
 * @param url the url to set
 */
public void setUrl(String url) {
	this.url = url;
}
/**
 * @return the insert_time
 */
public String getInsert_time() {
	return insert_time;
}
/**
 * @param insert_time the insert_time to set
 */
public void setInsert_time(String insert_time) {
	this.insert_time = insert_time;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}


}
