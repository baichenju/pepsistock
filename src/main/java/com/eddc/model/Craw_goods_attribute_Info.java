/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年6月26日 上午11:01:08 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：selection_sibrary_crawler   
* 类名称：Craw_goods_attribute_Info   
* 类描述：   商品属性表
* 创建人：jack.zhao   
* 创建时间：2018年6月26日 上午11:01:08   
* 修改人：jack.zhao   
* 修改时间：2018年6月26日 上午11:01:08   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_attribute_Info implements Serializable{
private int cust_keyword_id;
private String goodsid;
private String egoodsid;
private String craw_attri_cn;
private String craw_attri_en;
private String attri_value;
private String update_time;
private String update_date;
private String batch_time;
private String platform_name_en;

public int getCust_keyword_id() {
	return cust_keyword_id;
}

public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}

public String getGoodsid() {
	return goodsid;
}
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
public String getEgoodsid() {
	return egoodsid;
}
public void setEgoodsid(String egoodsid) {
	this.egoodsid = egoodsid;
}
public String getCraw_attri_cn() {
	return craw_attri_cn;
}

public void setCraw_attri_cn(String craw_attri_cn) {
	this.craw_attri_cn = craw_attri_cn;
}
public String getCraw_attri_en() {
	return craw_attri_en;
}
public void setCraw_attri_en(String craw_attri_en) {
	this.craw_attri_en = craw_attri_en;
}
public String getAttri_value() {
	return attri_value;
}
public void setAttri_value(String attri_value) {
	this.attri_value = attri_value;
}
public String getUpdate_time() {
	return update_time;
}
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
public String getUpdate_date() {
	return update_date;
}
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
public String getBatch_time() {
	return batch_time;
}
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
public String getPlatform_name_en() {
	return platform_name_en;
}
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}

}
