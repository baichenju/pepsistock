/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月14日 下午1:36:15 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_comment_Info   
* 类描述：商品评论数
* 创建人：jack.zhao   
* 创建时间：2018年5月14日 下午1:36:15   
* 修改人：jack.zhao   
* 修改时间：2018年5月14日 下午1:36:15   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_comment_Info implements Serializable{
private int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_name_en;
private String ttl_comment_num;//总评
private String pos_comment_num;//好评
private String neg_comment_num;//差评
private String neu_comment_num;//中评
private String update_time;
private String update_date;
private String batch_time;
private int comment_status;

/**
 * @return the comment_status
 */
public int getComment_status() {
	return comment_status;
}
/**
 * @param comment_status the comment_status to set
 */
public void setComment_status(int comment_status) {
	this.comment_status = comment_status;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the ttl_comment_num
 */
public String getTtl_comment_num() {
	return ttl_comment_num;
}
/**
 * @param ttl_comment_num the ttl_comment_num to set
 */
public void setTtl_comment_num(String ttl_comment_num) {
	this.ttl_comment_num = ttl_comment_num;
}
/**
 * @return the pos_comment_num
 */
public String getPos_comment_num() {
	return pos_comment_num;
}
/**
 * @param pos_comment_num the pos_comment_num to set
 */
public void setPos_comment_num(String pos_comment_num) {
	this.pos_comment_num = pos_comment_num;
}
/**
 * @return the neg_comment_num
 */
public String getNeg_comment_num() {
	return neg_comment_num;
}
/**
 * @param neg_comment_num the neg_comment_num to set
 */
public void setNeg_comment_num(String neg_comment_num) {
	this.neg_comment_num = neg_comment_num;
}
/**
 * @return the neu_comment_num
 */
public String getNeu_comment_num() {
	return neu_comment_num;
}
/**
 * @param neu_comment_num the neu_comment_num to set
 */
public void setNeu_comment_num(String neu_comment_num) {
	this.neu_comment_num = neu_comment_num;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}


 
}
