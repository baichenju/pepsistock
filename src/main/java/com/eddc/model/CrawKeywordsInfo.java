package com.eddc.model;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class CrawKeywordsInfo implements Serializable {  
private String cust_keyword_name;
private String cust_keyword_id;
private String platform_name;
private String cust_account_id;
private String batch_time;
private String number;
private String cust_keyword_url;
private String cust_account_name;
private String cust_keyword_remark;
private String cust_keyword_type;
private String crawling_fre;
private String always_abnormal;
private String is_presell;
private int search_page_nums;
private int crawling_status;
private String category_name;
private String category_id;
private String search_price_range;
private String cust_keyword_ref;

public String getCust_keyword_ref() {
	return cust_keyword_ref;
}
public void setCust_keyword_ref(String cust_keyword_ref) {
	this.cust_keyword_ref = cust_keyword_ref;
}
public String getCategory_id() {
	return category_id;
}
public void setCategory_id(String category_id) {
	this.category_id = category_id;
}
public String getCategory_name() {
	return category_name;
}
public void setCategory_name(String category_name) {
	this.category_name = category_name;
}
public int getSearch_page_nums() {
	return search_page_nums;
}
public void setSearch_page_nums(int search_page_nums) {
	this.search_page_nums = search_page_nums;
}
public String getIs_presell() {
	return is_presell;
}
public void setIs_presell(String is_presell) {
	this.is_presell = is_presell;
}
public String getCrawling_fre() {
	return crawling_fre;
}
public void setCrawling_fre(String crawling_fre) {
	this.crawling_fre = crawling_fre;
}
public String getAlways_abnormal() {
	return always_abnormal;
}
public void setAlways_abnormal(String always_abnormal) {
	this.always_abnormal = always_abnormal;
}

public String getCust_keyword_type() {
	return cust_keyword_type;
}
public void setCust_keyword_type(String cust_keyword_type) {
	this.cust_keyword_type = cust_keyword_type;
}
public String getCust_keyword_url() {
	return cust_keyword_url;
}
public void setCust_keyword_url(String cust_keyword_url) {
	this.cust_keyword_url = cust_keyword_url;
}
public String getBatch_time() {
	return batch_time;
}
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
public String getCust_account_id() {
	return cust_account_id;
}
public void setCust_account_id(String cust_account_id) {
	this.cust_account_id = cust_account_id;
}
public String getCust_keyword_name() {
	return cust_keyword_name;
}
public String getCust_keyword_id() {
	return cust_keyword_id;
}
public String getPlatform_name() {
	return platform_name;
}
public void setCust_keyword_name(String cust_keyword_name) {
	this.cust_keyword_name = cust_keyword_name;
}
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
public String getNumber() {
	return number;
}
public void setNumber(String number) {
	this.number = number;
}
public String getCust_account_name() {
	return cust_account_name;
}
public void setCust_account_name(String cust_account_name) {
	this.cust_account_name = cust_account_name;
}
public String getCust_keyword_remark() {
	return cust_keyword_remark;
}
public void setCust_keyword_remark(String cust_keyword_remark) {
	this.cust_keyword_remark = cust_keyword_remark;
}
public int getCrawling_status() {
	return crawling_status;
}
public void setCrawling_status(int crawling_status) {
	this.crawling_status = crawling_status;
}
/**
 * @return the search_price_range
 */
public String getSearch_price_range() {
	return search_price_range;
}
/**
 * @param search_price_range the search_price_range to set
 */
public void setSearch_price_range(String search_price_range) {
	this.search_price_range = search_price_range;
}

}
