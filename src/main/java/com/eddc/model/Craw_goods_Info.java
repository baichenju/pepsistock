package com.eddc.model;
import java.io.Serializable;
public class Craw_goods_Info  implements Serializable {
private static final long serialVersionUID = 2L;  
private int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String message;
private String platform_goods_name;
private String platform_name_en;
private String platform_category;
private String platform_sellerid;
private String platform_sellername;
private String platform_shopid;
private String platform_shopname;
private String platform_shoptype;
private String delivery_info;
private String delivery_place;
private String seller_location;
private int  goods_status;
private String inventory;
private String sale_qty;
private String ttl_comment_num;
private String pos_comment_num;
private String neg_comment_num;
private String neu_comment_num;
private String goods_url;
private String goods_pic_url;
private String update_time;
private String update_date;
private String feature1;
private String batch_time;
private String deposit;
private String to_use_amount;
private String reserve_num;
private String product_location;
private String putaway;
private String shelves;
private String cust_account_id;
private String position;
private String bsr_rank;

/**
 * @return the bsr_rank
 */
public String getBsr_rank() {
	return bsr_rank;
}
/**
 * @param bsr_rank the bsr_rank to set
 */
public void setBsr_rank(String bsr_rank) {
	this.bsr_rank = bsr_rank;
}
/**
 * @return the position
 */
public String getPosition() {
	return position;
}
/**
 * @param position the position to set
 */
public void setPosition(String position) {
	this.position = position;
}
public String getCust_account_id() {
	return cust_account_id;
}
public void setCust_account_id(String cust_account_id) {
	this.cust_account_id = cust_account_id;
}
public String getPutaway() {
	return putaway;
}
public void setPutaway(String putaway) {
	this.putaway = putaway;
}
public String getShelves() {
	return shelves;
}
public void setShelves(String shelves) {
	this.shelves = shelves;
}

/**
 * @return the product_location
 */
public String getProduct_location() {
	return product_location;
}
/**
 * @param product_location the product_location to set
 */
public void setProduct_location(String product_location) {
	this.product_location = product_location;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the message
 */
public String getMessage() {
	return message;
}
/**
 * @param message the message to set
 */
public void setMessage(String message) {
	this.message = message;
}
/**
 * @return the platform_goods_name
 */
public String getPlatform_goods_name() {
	return platform_goods_name;
}
/**
 * @param platform_goods_name the platform_goods_name to set
 */
public void setPlatform_goods_name(String platform_goods_name) {
	this.platform_goods_name = platform_goods_name;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the platform_category
 */
public String getPlatform_category() {
	return platform_category;
}
/**
 * @param platform_category the platform_category to set
 */
public void setPlatform_category(String platform_category) {
	this.platform_category = platform_category;
}
/**
 * @return the platform_sellerid
 */
public String getPlatform_sellerid() {
	return platform_sellerid;
}
/**
 * @param platform_sellerid the platform_sellerid to set
 */
public void setPlatform_sellerid(String platform_sellerid) {
	this.platform_sellerid = platform_sellerid;
}
/**
 * @return the platform_sellername
 */
public String getPlatform_sellername() {
	return platform_sellername;
}
/**
 * @param platform_sellername the platform_sellername to set
 */
public void setPlatform_sellername(String platform_sellername) {
	this.platform_sellername = platform_sellername;
}
/**
 * @return the platform_shopid
 */
public String getPlatform_shopid() {
	return platform_shopid;
}
/**
 * @param platform_shopid the platform_shopid to set
 */
public void setPlatform_shopid(String platform_shopid) {
	this.platform_shopid = platform_shopid;
}
/**
 * @return the platform_shopname
 */
public String getPlatform_shopname() {
	return platform_shopname;
}
/**
 * @param platform_shopname the platform_shopname to set
 */
public void setPlatform_shopname(String platform_shopname) {
	this.platform_shopname = platform_shopname;
}
/**
 * @return the platform_shoptype
 */
public String getPlatform_shoptype() {
	return platform_shoptype;
}
/**
 * @param platform_shoptype the platform_shoptype to set
 */
public void setPlatform_shoptype(String platform_shoptype) {
	this.platform_shoptype = platform_shoptype;
}
/**
 * @return the delivery_info
 */
public String getDelivery_info() {
	return delivery_info;
}
/**
 * @param delivery_info the delivery_info to set
 */
public void setDelivery_info(String delivery_info) {
	this.delivery_info = delivery_info;
}
/**
 * @return the delivery_place
 */
public String getDelivery_place() {
	return delivery_place;
}
/**
 * @param delivery_place the delivery_place to set
 */
public void setDelivery_place(String delivery_place) {
	this.delivery_place = delivery_place;
}
/**
 * @return the seller_location
 */
public String getSeller_location() {
	return seller_location;
}
/**
 * @param seller_location the seller_location to set
 */
public void setSeller_location(String seller_location) {
	this.seller_location = seller_location;
}
/**
 * @return the goods_status
 */
public int getGoods_status() {
	return goods_status;
}
/**
 * @param goods_status the goods_status to set
 */
public void setGoods_status(int goods_status) {
	this.goods_status = goods_status;
}
/**
 * @return the inventory
 */
public String getInventory() {
	return inventory;
}
/**
 * @param inventory the inventory to set
 */
public void setInventory(String inventory) {
	this.inventory = inventory;
}
/**
 * @return the sale_qty
 */
public String getSale_qty() {
	return sale_qty;
}
/**
 * @param sale_qty the sale_qty to set
 */
public void setSale_qty(String sale_qty) {
	this.sale_qty = sale_qty;
}
/**
 * @return the ttl_comment_num
 */
public String getTtl_comment_num() {
	return ttl_comment_num;
}
/**
 * @param ttl_comment_num the ttl_comment_num to set
 */
public void setTtl_comment_num(String ttl_comment_num) {
	this.ttl_comment_num = ttl_comment_num;
}
/**
 * @return the pos_comment_num
 */
public String getPos_comment_num() {
	return pos_comment_num;
}
/**
 * @param pos_comment_num the pos_comment_num to set
 */
public void setPos_comment_num(String pos_comment_num) {
	this.pos_comment_num = pos_comment_num;
}
/**
 * @return the neg_comment_num
 */
public String getNeg_comment_num() {
	return neg_comment_num;
}
/**
 * @param neg_comment_num the neg_comment_num to set
 */
public void setNeg_comment_num(String neg_comment_num) {
	this.neg_comment_num = neg_comment_num;
}
/**
 * @return the neu_comment_num
 */
public String getNeu_comment_num() {
	return neu_comment_num;
}
/**
 * @param neu_comment_num the neu_comment_num to set
 */
public void setNeu_comment_num(String neu_comment_num) {
	this.neu_comment_num = neu_comment_num;
}
/**
 * @return the goods_url
 */
public String getGoods_url() {
	return goods_url;
}
/**
 * @param goods_url the goods_url to set
 */
public void setGoods_url(String goods_url) {
	this.goods_url = goods_url;
}
/**
 * @return the goods_pic_url
 */
public String getGoods_pic_url() {
	return goods_pic_url;
}
/**
 * @param goods_pic_url the goods_pic_url to set
 */
public void setGoods_pic_url(String goods_pic_url) {
	this.goods_pic_url = goods_pic_url;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the feature1
 */
public String getFeature1() {
	return feature1;
}
/**
 * @param feature1 the feature1 to set
 */
public void setFeature1(String feature1) {
	this.feature1 = feature1;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
/**
 * @return the deposit
 */
public String getDeposit() {
	return deposit;
}
/**
 * @param deposit the deposit to set
 */
public void setDeposit(String deposit) {
	this.deposit = deposit;
}
/**
 * @return the to_use_amount
 */
public String getTo_use_amount() {
	return to_use_amount;
}
/**
 * @param to_use_amount the to_use_amount to set
 */
public void setTo_use_amount(String to_use_amount) {
	this.to_use_amount = to_use_amount;
}
/**
 * @return the reserve_num
 */
public String getReserve_num() {
	return reserve_num;
}
/**
 * @param reserve_num the reserve_num to set
 */
public void setReserve_num(String reserve_num) {
	this.reserve_num = reserve_num;
}
/* (non Javadoc) 
 * @Title: toString
 * @Description: TODO
 * @return 
 * @see java.lang.Object#toString() 
 */ 
@Override
public String toString() {
	return "Craw_goods_Info [cust_keyword_id=" + cust_keyword_id + ", goodsId=" + goodsId + ", egoodsId=" + egoodsId
			+ ", message=" + message + ", platform_goods_name=" + platform_goods_name + ", platform_name_en="
			+ platform_name_en + ", platform_category=" + platform_category + ", platform_sellerid=" + platform_sellerid
			+ ", platform_sellername=" + platform_sellername + ", platform_shopid=" + platform_shopid
			+ ", platform_shopname=" + platform_shopname + ", platform_shoptype=" + platform_shoptype
			+ ", delivery_info=" + delivery_info + ", delivery_place=" + delivery_place + ", seller_location="
			+ seller_location + ", goods_status=" + goods_status + ", inventory=" + inventory + ", sale_qty=" + sale_qty
			+ ", ttl_comment_num=" + ttl_comment_num + ", pos_comment_num=" + pos_comment_num + ", neg_comment_num="
			+ neg_comment_num + ", neu_comment_num=" + neu_comment_num + ", goods_url=" + goods_url + ", goods_pic_url="
			+ goods_pic_url + ", update_time=" + update_time + ", update_date=" + update_date + ", feature1=" + feature1
			+ ", batch_time=" + batch_time + ", deposit=" + deposit + ", to_use_amount=" + to_use_amount
			+ ", reserve_num=" + reserve_num + ", product_location=" + product_location + ", putaway=" + putaway
			+ ", shelves=" + shelves + ", cust_account_id=" + cust_account_id + ", position=" + position + ", bsr_rank="
			+ bsr_rank + "]";
   }
}
