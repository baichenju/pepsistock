package com.eddc.model;

import java.io.Serializable;
import java.math.BigInteger;

public class JobAndTrigger implements Serializable { 
	private String SCHED_NAME;
	private String JOB_NAME;
	private String JOB_GROUP;
	private String JOB_CLASS_NAME;
	private String TRIGGER_NAME;
	private String TRIGGER_GROUP;
	private BigInteger REPEAT_INTERVAL;
	private BigInteger TIMES_TRIGGERED;
	private String CRON_EXPRESSION;
	private String TIME_ZONE_ID;
	private String STATUS;
	private String IP;
	private String DATASOURCE;
	private String PLATFORM;
	private int THREAD_SUM;
	private String DOCKER;
	private String DESCRIBE;
	private String INVENTORY;
	private String CLIENT;
	private int   PROMOTION_STATUS;
	private int   PRICE_STATUS;
	private int   INVENTORY_STATUS;
	private String DATABASES;
	private String STORAGE;
	private int SUMS;
	public int getSUMS() {
		return SUMS;
	}
	public void setSUMS(int sUMS) {
		SUMS = sUMS;
	}

	public int getPROMOTION_STATUS() {
		return PROMOTION_STATUS;
	}

	public void setPROMOTION_STATUS(int pROMOTION_STATUS) {
		PROMOTION_STATUS = pROMOTION_STATUS;
	}

	public int getPRICE_STATUS() {
		return PRICE_STATUS;
	}

	public void setPRICE_STATUS(int pRICE_STATUS) {
		PRICE_STATUS = pRICE_STATUS;
	}

	public int getINVENTORY_STATUS() {
		return INVENTORY_STATUS;
	}

	public void setINVENTORY_STATUS(int iNVENTORY_STATUS) {
		INVENTORY_STATUS = iNVENTORY_STATUS;
	}
	public String getCLIENT() {
		return CLIENT;
	}
	public void setCLIENT(String cLIENT) {
		CLIENT = cLIENT;
	}
	public String getDESCRIBE() {
		return DESCRIBE;
	}
	public void setDESCRIBE(String dESCRIBE) {
		DESCRIBE = dESCRIBE;
	}
	public String getINVENTORY() {
		return INVENTORY;
	}
	public void setINVENTORY(String iNVENTORY) {
		INVENTORY = iNVENTORY;
	}
	public String getDOCKER() {
		return DOCKER;
	}
	public void setDOCKER(String dOCKER) {
		DOCKER = dOCKER;
	}
	public int getTHREAD_SUM() {
		return THREAD_SUM;
	}
	public void setTHREAD_SUM(int tHREAD_SUM) {
		THREAD_SUM = tHREAD_SUM;
	}
	public String getSCHED_NAME() {
		return SCHED_NAME;
	}
	public void setSCHED_NAME(String sCHED_NAME) {
		SCHED_NAME = sCHED_NAME;
	}
	public String getJOB_NAME() {
		return JOB_NAME;
	}
	public void setJOB_NAME(String jOB_NAME) {
		JOB_NAME = jOB_NAME;
	}
	public String getJOB_GROUP() {
		return JOB_GROUP;
	}
	public void setJOB_GROUP(String jOB_GROUP) {
		JOB_GROUP = jOB_GROUP;
	}
	public String getJOB_CLASS_NAME() {
		return JOB_CLASS_NAME;
	}
	public void setJOB_CLASS_NAME(String jOB_CLASS_NAME) {
		JOB_CLASS_NAME = jOB_CLASS_NAME;
	}
	public String getTRIGGER_NAME() {
		return TRIGGER_NAME;
	}
	public void setTRIGGER_NAME(String tRIGGER_NAME) {
		TRIGGER_NAME = tRIGGER_NAME;
	}
	public String getTRIGGER_GROUP() {
		return TRIGGER_GROUP;
	}
	public void setTRIGGER_GROUP(String tRIGGER_GROUP) {
		TRIGGER_GROUP = tRIGGER_GROUP;
	}
	public BigInteger getREPEAT_INTERVAL() {
		return REPEAT_INTERVAL;
	}
	public void setREPEAT_INTERVAL(BigInteger rEPEAT_INTERVAL) {
		REPEAT_INTERVAL = rEPEAT_INTERVAL;
	}
	public BigInteger getTIMES_TRIGGERED() {
		return TIMES_TRIGGERED;
	}
	public void setTIMES_TRIGGERED(BigInteger tIMES_TRIGGERED) {
		TIMES_TRIGGERED = tIMES_TRIGGERED;
	}
	public String getCRON_EXPRESSION() {
		return CRON_EXPRESSION;
	}
	public void setCRON_EXPRESSION(String cRON_EXPRESSION) {
		CRON_EXPRESSION = cRON_EXPRESSION;
	}
	public String getTIME_ZONE_ID() {
		return TIME_ZONE_ID;
	}
	public void setTIME_ZONE_ID(String tIME_ZONE_ID) {
		TIME_ZONE_ID = tIME_ZONE_ID;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getDATASOURCE() {
		return DATASOURCE;
	}
	public void setDATASOURCE(String dATASOURCE) {
		DATASOURCE = dATASOURCE;
	}
	public String getPLATFORM() {
		return PLATFORM;
	}
	public void setPLATFORM(String pLATFORM) {
		PLATFORM = pLATFORM;
	}
	public String getSTORAGE() {
		return STORAGE;
	}
	public void setSTORAGE(String sTORAGE) {
		STORAGE = sTORAGE;
	}
	public String getDATABASES() {
		return DATABASES;
	}
	public void setDATABASES(String dATABASES) {
		DATABASES = dATABASES;
	}
	
}
