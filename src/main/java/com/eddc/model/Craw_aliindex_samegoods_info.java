/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月15日 上午11:51:04 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_samegoods_info   
* 类描述：获取阿里指数相似商品
* 创建人：jack.zhao   
* 创建时间：2018年5月15日 上午11:51:04   
* 修改人：jack.zhao   
* 修改时间：2018年5月15日 上午11:51:04   
* 修改备注：   
* @version    
*    
*/
public class Craw_aliindex_samegoods_info  implements Serializable{
private int soure_category_id;// ---类别id 例如 
private  String soure_egoods_id;//商品父节点ID
private String egoods_id;//商品egoodsid
private String goods_name;//商品名称
private String image_url;//-商品图片 
private String company_name;//公司名称
private String  company_url;//公司链接
private String  comment_count;//商品评论数
private Float goods_price;//商品价格
private String insert_time;//插入时间
private String update_time;//更新时间
/**
 * @return the soure_category_id
 */
public int getSoure_category_id() {
	return soure_category_id;
}
/**
 * @param soure_category_id the soure_category_id to set
 */
public void setSoure_category_id(int soure_category_id) {
	this.soure_category_id = soure_category_id;
}
/**
 * @return the soure_egoods_id
 */
public String getSoure_egoods_id() {
	return soure_egoods_id;
}
/**
 * @param soure_egoods_id the soure_egoods_id to set
 */
public void setSoure_egoods_id(String soure_egoods_id) {
	this.soure_egoods_id = soure_egoods_id;
}
/**
 * @return the egoods_id
 */
public String getEgoods_id() {
	return egoods_id;
}
/**
 * @param egoods_id the egoods_id to set
 */
public void setEgoods_id(String egoods_id) {
	this.egoods_id = egoods_id;
}
/**
 * @return the goods_name
 */
public String getGoods_name() {
	return goods_name;
}
/**
 * @param goods_name the goods_name to set
 */
public void setGoods_name(String goods_name) {
	this.goods_name = goods_name;
}
/**
 * @return the image_url
 */
public String getImage_url() {
	return image_url;
}
/**
 * @param image_url the image_url to set
 */
public void setImage_url(String image_url) {
	this.image_url = image_url;
}
/**
 * @return the company_name
 */
public String getCompany_name() {
	return company_name;
}
/**
 * @param company_name the company_name to set
 */
public void setCompany_name(String company_name) {
	this.company_name = company_name;
}
/**
 * @return the company_url
 */
public String getCompany_url() {
	return company_url;
}
/**
 * @param company_url the company_url to set
 */
public void setCompany_url(String company_url) {
	this.company_url = company_url;
}
/**
 * @return the comment_count
 */
public String getComment_count() {
	return comment_count;
}
/**
 * @param comment_count the comment_count to set
 */
public void setComment_count(String comment_count) {
	this.comment_count = comment_count;
}
/**
 * @return the goods_price
 */
public Float getGoods_price() {
	return goods_price;
}
/**
 * @param goods_price the goods_price to set
 */
public void setGoods_price(Float goods_price) {
	this.goods_price = goods_price;
}
/**
 * @return the insert_time
 */
public String getInsert_time() {
	return insert_time;
}
/**
 * @param insert_time the insert_time to set
 */
public void setInsert_time(String insert_time) {
	this.insert_time = insert_time;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}

}
