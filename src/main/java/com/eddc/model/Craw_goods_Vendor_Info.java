/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月16日 下午3:13:10 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Vendor_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月16日 下午3:13:10   
* 修改人：jack.zhao   
* 修改时间：2018年4月16日 下午3:13:10   
* 修改备注：   
* @version    
*    
*/
public class Craw_goods_Vendor_Info implements Serializable {
public int cust_keyword_id;
public String goodsid;
public String egoodsid;//---商品平台ID
public String platform_goodsname;//商品名称
public String platform_goods_detail;//商品详情
public String platform_goods_satisfaction;//商品满意度
public String platform_goodsurl;//商品页链接
public String platform_goods_picurl;//商品图片
public String platform_vendorid;//供应商ID
public String platform_vendorname;//供应商名称
public String platform_vendortype;//供应商类型（经营模式）
public String platform_vendoraddress;//供应商地址
public String goods_description;//货物描述分
public String service_attitude;//服务态度分
public String delivery_speed;//发货速度分
public String purchase_back;//继续购买分

public String goods_description_rate;//货描 高于或者低于均值
public String service_attitude_rate;//服务 高于或者低于均值
public String delivery_speed_rate;//-发货 高于或者低于均值
public String purchase_back_rate;//回头率 高于或者低于均值
public String purchase_stock;//采购库存
public String purchase_qty;//采购数量
public String purchase_comment_num;//评价数
public String purchase_promotion;//采购促销
public String purchaser_impression;//采购商印象
public String channel;//渠道
public int goods_status;
public String update_time;
public String update_date;
public String batch_time;

/**
 * @return the goods_status
 */
public int getGoods_status() {
	return goods_status;
}
/**
 * @param goods_status the goods_status to set
 */
public void setGoods_status(int goods_status) {
	this.goods_status = goods_status;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsid
 */
public String getGoodsid() {
	return goodsid;
}
/**
 * @param goodsid the goodsid to set
 */
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
/**
 * @return the egoodsid
 */
public String getEgoodsid() {
	return egoodsid;
}
/**
 * @param egoodsid the egoodsid to set
 */
public void setEgoodsid(String egoodsid) {
	this.egoodsid = egoodsid;
}
/**
 * @return the platform_goodsname
 */
public String getPlatform_goodsname() {
	return platform_goodsname;
}
/**
 * @param platform_goodsname the platform_goodsname to set
 */
public void setPlatform_goodsname(String platform_goodsname) {
	this.platform_goodsname = platform_goodsname;
}
/**
 * @return the platform_goods_detail
 */
public String getPlatform_goods_detail() {
	return platform_goods_detail;
}
/**
 * @param platform_goods_detail the platform_goods_detail to set
 */
public void setPlatform_goods_detail(String platform_goods_detail) {
	this.platform_goods_detail = platform_goods_detail;
}
/**
 * @return the platform_goods_satisfaction
 */
public String getPlatform_goods_satisfaction() {
	return platform_goods_satisfaction;
}
/**
 * @param platform_goods_satisfaction the platform_goods_satisfaction to set
 */
public void setPlatform_goods_satisfaction(String platform_goods_satisfaction) {
	this.platform_goods_satisfaction = platform_goods_satisfaction;
}
/**
 * @return the platform_goodsurl
 */
public String getPlatform_goodsurl() {
	return platform_goodsurl;
}
/**
 * @param platform_goodsurl the platform_goodsurl to set
 */
public void setPlatform_goodsurl(String platform_goodsurl) {
	this.platform_goodsurl = platform_goodsurl;
}
/**
 * @return the platform_goods_picurl
 */
public String getPlatform_goods_picurl() {
	return platform_goods_picurl;
}
/**
 * @param platform_goods_picurl the platform_goods_picurl to set
 */
public void setPlatform_goods_picurl(String platform_goods_picurl) {
	this.platform_goods_picurl = platform_goods_picurl;
}
/**
 * @return the platform_vendorid
 */
public String getPlatform_vendorid() {
	return platform_vendorid;
}
/**
 * @param platform_vendorid the platform_vendorid to set
 */
public void setPlatform_vendorid(String platform_vendorid) {
	this.platform_vendorid = platform_vendorid;
}
/**
 * @return the platform_vendorname
 */
public String getPlatform_vendorname() {
	return platform_vendorname;
}
/**
 * @param platform_vendorname the platform_vendorname to set
 */
public void setPlatform_vendorname(String platform_vendorname) {
	this.platform_vendorname = platform_vendorname;
}
/**
 * @return the platform_vendortype
 */
public String getPlatform_vendortype() {
	return platform_vendortype;
}
/**
 * @param platform_vendortype the platform_vendortype to set
 */
public void setPlatform_vendortype(String platform_vendortype) {
	this.platform_vendortype = platform_vendortype;
}
/**
 * @return the platform_vendoraddress
 */
public String getPlatform_vendoraddress() {
	return platform_vendoraddress;
}
/**
 * @param platform_vendoraddress the platform_vendoraddress to set
 */
public void setPlatform_vendoraddress(String platform_vendoraddress) {
	this.platform_vendoraddress = platform_vendoraddress;
}
/**
 * @return the goods_description
 */
public String getGoods_description() {
	return goods_description;
}
/**
 * @param goods_description the goods_description to set
 */
public void setGoods_description(String goods_description) {
	this.goods_description = goods_description;
}
/**
 * @return the service_attitude
 */
public String getService_attitude() {
	return service_attitude;
}
/**
 * @param service_attitude the service_attitude to set
 */
public void setService_attitude(String service_attitude) {
	this.service_attitude = service_attitude;
}
/**
 * @return the delivery_speed
 */
public String getDelivery_speed() {
	return delivery_speed;
}
/**
 * @param delivery_speed the delivery_speed to set
 */
public void setDelivery_speed(String delivery_speed) {
	this.delivery_speed = delivery_speed;
}
/**
 * @return the purchase_back
 */
public String getPurchase_back() {
	return purchase_back;
}
/**
 * @param purchase_back the purchase_back to set
 */
public void setPurchase_back(String purchase_back) {
	this.purchase_back = purchase_back;
}
/**
 * @return the goods_description_rate
 */
public String getGoods_description_rate() {
	return goods_description_rate;
}
/**
 * @param goods_description_rate the goods_description_rate to set
 */
public void setGoods_description_rate(String goods_description_rate) {
	this.goods_description_rate = goods_description_rate;
}
/**
 * @return the service_attitude_rate
 */
public String getService_attitude_rate() {
	return service_attitude_rate;
}
/**
 * @param service_attitude_rate the service_attitude_rate to set
 */
public void setService_attitude_rate(String service_attitude_rate) {
	this.service_attitude_rate = service_attitude_rate;
}
/**
 * @return the delivery_speed_rate
 */
public String getDelivery_speed_rate() {
	return delivery_speed_rate;
}
/**
 * @param delivery_speed_rate the delivery_speed_rate to set
 */
public void setDelivery_speed_rate(String delivery_speed_rate) {
	this.delivery_speed_rate = delivery_speed_rate;
}
/**
 * @return the purchase_back_rate
 */
public String getPurchase_back_rate() {
	return purchase_back_rate;
}
/**
 * @param purchase_back_rate the purchase_back_rate to set
 */
public void setPurchase_back_rate(String purchase_back_rate) {
	this.purchase_back_rate = purchase_back_rate;
}
/**
 * @return the purchase_stock
 */
public String getPurchase_stock() {
	return purchase_stock;
}
/**
 * @param purchase_stock the purchase_stock to set
 */
public void setPurchase_stock(String purchase_stock) {
	this.purchase_stock = purchase_stock;
}
/**
 * @return the purchase_qty
 */
public String getPurchase_qty() {
	return purchase_qty;
}
/**
 * @param purchase_qty the purchase_qty to set
 */
public void setPurchase_qty(String purchase_qty) {
	this.purchase_qty = purchase_qty;
}
/**
 * @return the purchase_comment_num
 */
public String getPurchase_comment_num() {
	return purchase_comment_num;
}
/**
 * @param purchase_comment_num the purchase_comment_num to set
 */
public void setPurchase_comment_num(String purchase_comment_num) {
	this.purchase_comment_num = purchase_comment_num;
}
/**
 * @return the purchase_promotion
 */
public String getPurchase_promotion() {
	return purchase_promotion;
}
/**
 * @param purchase_promotion the purchase_promotion to set
 */
public void setPurchase_promotion(String purchase_promotion) {
	this.purchase_promotion = purchase_promotion;
}
/**
 * @return the purchaser_impression
 */
public String getPurchaser_impression() {
	return purchaser_impression;
}
/**
 * @param purchaser_impression the purchaser_impression to set
 */
public void setPurchaser_impression(String purchaser_impression) {
	this.purchaser_impression = purchaser_impression;
}
/**
 * @return the channel
 */
public String getChannel() {
	return channel;
}
/**
 * @param channel the channel to set
 */
public void setChannel(String channel) {
	this.channel = channel;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}


}
