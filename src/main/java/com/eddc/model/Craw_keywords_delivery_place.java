/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年3月15日 下午3:24:52 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_keywords_delivery_place   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月15日 下午3:24:52   
* 修改人：jack.zhao   
* 修改时间：2018年3月15日 下午3:24:52   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_keywords_delivery_place implements Serializable {
private int id;
private String cust_account_id;
private String platform_name;
private String delivery_place_code;
private String delivery_place_name;
private String delivery_place_type;
private int  crawling_status;
private String update_time;
private String update_date;
private int search_status;

/**
 * @return the search_status
 */
public int getSearch_status() {
	return search_status;
}
/**
 * @param search_status the search_status to set
 */
public void setSearch_status(int search_status) {
	this.search_status = search_status;
}
/**
 * @return the id
 */
public int getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
	this.id = id;
}
/**
 * @return the cust_account_id
 */
public String getCust_account_id() {
	return cust_account_id;
}
/**
 * @param cust_account_id the cust_account_id to set
 */
public void setCust_account_id(String cust_account_id) {
	this.cust_account_id = cust_account_id;
}
/**
 * @return the platform_name
 */
public String getPlatform_name() {
	return platform_name;
}
/**
 * @param platform_name the platform_name to set
 */
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
/**
 * @return the delivery_place_code
 */
public String getDelivery_place_code() {
	return delivery_place_code;
}
/**
 * @param delivery_place_code the delivery_place_code to set
 */
public void setDelivery_place_code(String delivery_place_code) {
	this.delivery_place_code = delivery_place_code;
}
/**
 * @return the delivery_place_name
 */
public String getDelivery_place_name() {
	return delivery_place_name;
}
/**
 * @param delivery_place_name the delivery_place_name to set
 */
public void setDelivery_place_name(String delivery_place_name) {
	this.delivery_place_name = delivery_place_name;
}
/**
 * @return the delivery_place_type
 */
public String getDelivery_place_type() {
	return delivery_place_type;
}
/**
 * @param delivery_place_type the delivery_place_type to set
 */
public void setDelivery_place_type(String delivery_place_type) {
	this.delivery_place_type = delivery_place_type;
}
/**
 * @return the crawling_status
 */
public int getCrawling_status() {
	return crawling_status;
}
/**
 * @param crawling_status the crawling_status to set
 */
public void setCrawling_status(int crawling_status) {
	this.crawling_status = crawling_status;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}

}
