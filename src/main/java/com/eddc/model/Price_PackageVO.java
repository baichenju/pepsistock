/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年6月19日 下午1:44:09 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：selection_sibrary_crawler   
* 类名称：Price_PackageVO   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年6月19日 下午1:44:09   
* 修改人：jack.zhao   
* 修改时间：2018年6月19日 下午1:44:09   
* 修改备注：   
* @version    
*    
*/
public class Price_PackageVO implements Serializable {
private String egoodsId;
private String keyword;
private String dataEgoodsId;
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the keyword
 */
public String getKeyword() {
	return keyword;
}
/**
 * @param keyword the keyword to set
 */
public void setKeyword(String keyword) {
	this.keyword = keyword;
}
/**
 * @return the dataEgoodsId
 */
public String getDataEgoodsId() {
	return dataEgoodsId;
}
/**
 * @param dataEgoodsId the dataEgoodsId to set
 */
public void setDataEgoodsId(String dataEgoodsId) {
	this.dataEgoodsId = dataEgoodsId;
}

}
