/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年7月25日 下午1:18:45 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：selection_sibrary_crawler   
* 类名称：Craw_keywords_temp_Info_forsearch_history   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年7月25日 下午1:18:45   
* 修改人：jack.zhao   
* 修改时间：2018年7月25日 下午1:18:45   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_keywords_temp_Info_forsearch_history implements Serializable{
private String platform_name;
private int cust_account_id;
private int sums;
private int for_craw;
private String job_name;
private String complete;

/**
 * @return the complete
 */
public String getComplete() {
	return complete;
}
/**
 * @param complete the complete to set
 */
public void setComplete(String complete) {
	this.complete = complete;
}
/**
 * @return the platform_name
 */
public String getPlatform_name() {
	return platform_name;
}
/**
 * @param platform_name the platform_name to set
 */
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
/**
 * @return the cust_account_id
 */
public int getCust_account_id() {
	return cust_account_id;
}
/**
 * @param cust_account_id the cust_account_id to set
 */
public void setCust_account_id(int cust_account_id) {
	this.cust_account_id = cust_account_id;
}
/**
 * @return the sums
 */
public int getSums() {
	return sums;
}
/**
 * @param sums the sums to set
 */
public void setSums(int sums) {
	this.sums = sums;
}
/**
 * @return the for_craw
 */
public int getFor_craw() {
	return for_craw;
}
/**
 * @param for_craw the for_craw to set
 */
public void setFor_craw(int for_craw) {
	this.for_craw = for_craw;
}
/**
 * @return the job_name
 */
public String getJob_name() {
	return job_name;
}
/**
 * @param job_name the job_name to set
 */
public void setJob_name(String job_name) {
	this.job_name = job_name;
}

}
