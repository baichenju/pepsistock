package com.eddc.model;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class CommodityPrices implements Serializable{
private String goodsid;
private String egoodsId;
private String cust_keyword_id;
private String platform_category;
private String platform_shopid;
private String batch_time;
private String message;
private String platform_name_en;
private String channel;
private String platform_sellerid;
public String getPlatform_sellerid() {
	return platform_sellerid;
}
public void setPlatform_sellerid(String platform_sellerid) {
	this.platform_sellerid = platform_sellerid;
}
public String getChannel() {
	return channel;
}
public void setChannel(String channel) {
	this.channel = channel;
}
public String getGoodsid() {
	return goodsid;
}
public String getEgoodsId() {
	return egoodsId;
}
public String getCust_keyword_id() {
	return cust_keyword_id;
}
public String getPlatform_category() {
	return platform_category;
}
public String getPlatform_shopid() {
	return platform_shopid;
}
public String getBatch_time() {
	return batch_time;
}
public String getMessage() {
	return message;
}
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
public void setPlatform_category(String platform_category) {
	this.platform_category = platform_category;
}
public void setPlatform_shopid(String platform_shopid) {
	this.platform_shopid = platform_shopid;
}
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
public void setMessage(String message) {
	this.message = message;
}
public String getPlatform_name_en() {
	return platform_name_en;
}
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}

}
