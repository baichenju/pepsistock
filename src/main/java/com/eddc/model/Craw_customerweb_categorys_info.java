/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2017年11月16日 下午3:40:39 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Craw_customerweb_categorys_info   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月16日 下午3:40:39   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月16日 下午3:40:39   
 * 修改备注：   
 * @version    
 *    
 */
public class Craw_customerweb_categorys_info implements Serializable {
private int Userid ;
private String series ;
private String categories ;
private String skus ;
private String  website_name;
private String website_url ;
private String update_time;
private String update_date;
public int getUserid() {
	return Userid;
}
public void setUserid(int userid) {
	Userid = userid;
}
public String getSeries() {
	return series;
}
public void setSeries(String series) {
	this.series = series;
}
public String getCategories() {
	return categories;
}
public void setCategories(String categories) {
	this.categories = categories;
}
public String getSkus() {
	return skus;
}
public void setSkus(String skus) {
	this.skus = skus;
}
public String getWebsite_name() {
	return website_name;
}
public void setWebsite_name(String website_name) {
	this.website_name = website_name;
}

public String getWebsite_url() {
	return website_url;
}
public void setWebsite_url(String website_url) {
	this.website_url = website_url;
}
public String getUpdate_time() {
	return update_time;
}
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
public String getUpdate_date() {
	return update_date;
}
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}

}
