package com.eddc.model;

import java.io.Serializable;

public class Craw_Access_keyword implements Serializable {
private int id;
private String accountid;
private String platform_name_en;
private String platform_name_cn;
private String keywords_cn;
private String keywords_en;
private String is_searched;
private String search_time;
private String price_floor;
private String page_nums;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getAccountid() {
	return accountid;
}
public void setAccountid(String accountid) {
	this.accountid = accountid;
}
public String getPlatform_name_en() {
	return platform_name_en;
}
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
public String getPlatform_name_cn() {
	return platform_name_cn;
}
public void setPlatform_name_cn(String platform_name_cn) {
	this.platform_name_cn = platform_name_cn;
}
public String getKeywords_cn() {
	return keywords_cn;
}
public void setKeywords_cn(String keywords_cn) {
	this.keywords_cn = keywords_cn;
}
public String getKeywords_en() {
	return keywords_en;
}
public void setKeywords_en(String keywords_en) {
	this.keywords_en = keywords_en;
}
public String getIs_searched() {
	return is_searched;
}
public void setIs_searched(String is_searched) {
	this.is_searched = is_searched;
}
public String getSearch_time() {
	return search_time;
}
public void setSearch_time(String search_time) {
	this.search_time = search_time;
}
public String getPrice_floor() {
	return price_floor;
}
public void setPrice_floor(String price_floor) {
	this.price_floor = price_floor;
}
public String getPage_nums() {
	return page_nums;
}
public void setPage_nums(String page_nums) {
	this.page_nums = page_nums;
}

}
