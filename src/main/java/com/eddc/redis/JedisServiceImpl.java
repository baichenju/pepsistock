package com.eddc.redis;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisPool;
import java.util.List;

/**
 * jedis 操作实现类
 * Created by YanJun on 2018/3/1.
 */
@Service
public class JedisServiceImpl implements JedisService {

	private static final Logger logger = LoggerFactory.getLogger(JedisServiceImpl.class);

	@Autowired
	private JedisPool jedisPool;

	@Override
	public void set(String key, String value) {
		jedisPool.getResource().set(key,value);
	}

	@Override
	public void setObj(String key, Object obj) {
		String value = JSONObject.toJSONString(obj);
		set(key,value);
	}

	@Override
	public String get(String key) {
		return jedisPool.getResource().get(key);
	}

	@Override
	public <T> Object getObj(String key, Class<T> clazz) {
		String objStr = get(key);
		return JSON.parseObject(objStr, clazz);
	}

	@Override
	public long expire(String key, int expire) {
		return jedisPool.getResource().expire(key,expire);
	}

	@Override
	public <T> void setList(String key, List<T> list) {
		jedisPool.getResource().set(key, JSONArray.toJSONString(list));
	}

	@Override
	public <T> void setDataList(String key, List<T> list) {
		try {
			Gson son=new Gson();
			jedisPool.getResource().set(key, son.toJson(list));
		} catch (Exception e) {
			logger.info("Data conversion failed>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}
		
	}

	@Override
	public <T> List<T> getList(String key, Class<T> clz) {
		String json = get(key);
		if(json!=null){
			List<T> list = (List<T>) JSONArray.parseArray(json, clz);
			return list;
		}
		return null;
	}

	@Override
	public long lPush(String key, Object obj) {
		String value = JSONObject.toJSONString(obj);
		return jedisPool.getResource().lpush(key,value);
	}

	@Override
	public long rPush(String key, Object obj) {
		String value = JSONObject.toJSONString(obj);
		return jedisPool.getResource().rpush(key,value);
	}

	@Override
	public String lPop(String key) {
		return jedisPool.getResource().lpop(key);
	}

	@Override
	public void close() {
		jedisPool.close();	
	}
}
