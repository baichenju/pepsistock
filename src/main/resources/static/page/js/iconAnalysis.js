$(function () {		
//指定图表的配置项和数据
	dataIconAnalysis();
});

function dataIconAnalysis(){
	$.ajax({
		url: '../chart/dataAnalysis',
		type: 'post',
		success: function (data) {
			var result = $.parseJSON(data);	
			console.log(result);
			var iconAnalysisChart = echarts.init(document.getElementById('main'));
			var option ={
					title: {
		               // text: '每个小时爬虫模块数据统计'
						},  
				    tooltip : {
				        trigger: 'axis',
				        axisPointer : {             // 坐标轴指示器，坐标轴触发有效
				            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
				        }
				    },
				    toolbox: {
				        feature: {
				            dataView: {show: true, readOnly: false},
				            magicType: {show: true, type: ['line', 'bar']},
				            restore: {show: true},
				            saveAsImage: {show: true}
				        }
				    },
				    legend: {
				        data:['tmall','jd','yhd','suning','vip','jumei','kaola','jumeiglobal']
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    xAxis : [
				        {
				            type : 'category',
				            data : ['周一','周二','周三','周四','周五','周六','周日'],
				            axisPointer: {
				                type: 'shadow'
				            }
				        }
				    ],
				    yAxis : [
			             {
			                 type: 'value',
			                 name: '商品个数',
			                 min: 100,
			                 max: 700,
			                 interval: 100
			                
			             }
				    ],
				    series : []
			};
            var seriesArray = new Array();
            var xAxisArray=new Array();
			for(var i=0;i<result.length;i++){
				
                xAxisArray.push(result[i].cust_account_id);
			}
			seriesArray.push({name:'商品个数',data:[222,334,113,222,444,555,777],type:'line'});
            option.xAxis[0].data =xAxisArray;//x轴
            option.series = seriesArray;
			iconAnalysisChart.setOption(option); 
		} 
	});
}