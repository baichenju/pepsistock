	$(function(){
	selectProducts();//获取查询信息
	validateForm(); 
	
});
function selectProducts (){
		var url= '/job/queryjob?pageNum=1&pageSize=2';
		$("#example2").bootstrapTable('destroy');
		hotSaleTablePro=$('#example2').bootstrapTable({ 
			 url: url,
			 method: 'post',
			 url: url,
			cache: false,////是否使用缓存，默认为true  
			striped: true, //是否显示行间隔色
			search: true,  //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，
			showRefresh: true,//是否显示刷新按钮
			clickToSelect: true, //是否启用点击选中行 
			pagination: true,//是否显示分页（*）
			striped: true,//条纹
			pageSize: 10, //每页的记录行数（*）    
			pageList: [10],         //可供选择的每页的行数（*）  25, 50, 100
			sortOrder : 'desc',
			height:750,
			showColumns: true, //是否显示所有的列
			cardView: false, //是否显示详细视图
			showToggle:true, //是否显示详细视图和列表视图的切换按钮
			columns: [//PLATFORM
			          {
			        	  field: 'PLATFORM',
			        	  title: '平台',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  console.log(full);
			                return '<span class="textAuto" title='+'\''+full.DESCRIBE+'\''+'>'+platformsData(data)+'</span>'; 
			        	  }   
			          },
			          {
			        	  field: 'TIME_ZONE_ID',
			        	  title: '用户', 
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  var name=full.JOB_NAME.split("_");
			        		  return name[3];
			        	  }
			          },
			          { 
			        	  field: 'CRON_EXPRESSION',
			        	  title: '表达式',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  editable:{ 
				        		  type: 'text',
				        		  title: '调度'
				                 }
			        	  
			          }, 
			          { 
			        	  field: 'DOCKER',
			        	  title: '终止',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  editable: {
				        	   type: 'text',
				        	   title: '终止'
				        	  }
			        	  
			          },
			          { 
			        	  field: 'SUMS',
			        	  title: '请求数',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  editable: {
				        	   type: 'text',
				        	   title: '请求数'
				        	   /*source:[
				        	       {value:"300",text:"300"},{value:"500",text:"500"},{value:"700",text:"700"},{value:"800",text:"800"},
				        	       {value:"900",text:"900"},{value:"1000",text:"1000"},{value:"1500",text:"1500"},{value:"2000",text:"2000"},
				        	       {value:"2500",text:"2500"},{value:"3500",text:"3500"},{value:"4500",text:"4500"},{value:"5500",text:"5500"},
				        	       {value:"6000",text:"6000"},{value:"7000",text:"7000"},{value:"8000",text:"8000"},{value:"10000",text:"10000"},
				        	       {value:"15000",text:"15000"},{value:"20000",text:"20000"},{value:"25000",text:"25000"},{value:"30000",text:"30000"},
				        	       {value:"45000",text:"45000"},{value:"50000",text:"50000"},{value:"55000",text:"55000"},{value:"60000",text:"60000"},
				        	       {value:"70000",text:"70000"},{value:"80000",text:"80000"},{value:"90000",text:"90000"},{value:"500000",text:"500000"}
				        	   ]*/
				        	  }
			        	  
			          },
			          {
			        	  field: 'JOB_NAME',
			        	  title: 'Job名称',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true
			          },
			          
			          {
			        	  field: 'JOB_CLASS_NAME',
			        	  title: '目录',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true
			          },
			          /*{
			        	  field: 'DATASOURCE',
			        	  title: '代理IP', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
				        	   type: 'select',
				        	   title: '代理IP',
				        	   source:[
				        	   {value:"ipProxy1",text:"ipProxy1"},{value:"ipProxy2",text:"ipProxy2"},{value:"ipProxy3",text:"ipProxy3"},
				        	   {value:"ipProxy4",text:"ipProxy4"},{value:"ipProxy5",text:"ipProxy5"},{value:"ipProxy6",text:"ipProxy6"},
				        	   {value:"ipProxy7",text:"ipProxy7"},{value:"ipProxy8",text:"ipProxy8"},{value:"ipProxy9",text:"ipProxy9"}
				        	   
				        	   ]
				        	  }
			          },*/
			          {
			        	  field: 'DATABASES',
			        	  title: '数据库', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
				        	   type: 'select',
				        	   title: '数据库',
				        	   source:[{value:"CustomerMonitor",text:"CustomerMonitor"},{value:"cmtcatdata",text:"cmtcatdata"},{value:"CmtTest",text:"CmtTest"},{value:"cmtcatdataTest",text:"cmtcatdataTest"},{value:"CrawlElve",text:"CrawlElve"},{value:"MonitoringShiBie",text:"MonitoringShiBie"},{value:"customermonitorRedshift",text:"customermonitorRedshift"}]
				        	  }
			          },
			          {
			        	  field: 'STORAGE',
			        	  title: '数据存储', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
				        	   type: 'select',
				        	   title: '数据存储',
				        	   source:[{value:"0",text:"0"},{value:"1",text:"1"},{value:"2",text:"2"},{value:"3",text:"3"},{value:"4",text:"4"}]
				        	  }
			          },
			          {
			        	  field: 'THREAD_SUM',
			        	  title: '线程', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
			        		  type: 'select',
			        		  title: '线程',
			        		  source:[
			        		          {value:"1",text:"1"},{value:"3",text:"3"},{value:"5",text:"5"},{value:"10",text:"10"},{value:"15",text:"15"},
			        		          {value:"25",text:"25"},{value:"30",text:"30"},{value:"35",text:"35"},{value:"40",text:"40"},{value:"50",text:"50"}
			        		          ] 
			        	  }
			          },
			          {
			        	  field: 'INVENTORY',
			        	  title: '监控区', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable:{ 
			        		  type: 'text',
			        		  title: '监控城市'
			                 }
			          },
			          {
			        	  field: 'CLIENT',
			        	  title: '客户端', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
				        	   type: 'select',
				        	   title: '客户端',
				        	   source:[{value:"ALL",text:"ALL"},{value:"MOBILE",text:"MOBILE"}]
				        	  }
			          },
			          {
			        	  field: 'JOB_GROUP',
			        	  title: '抓数', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  if(full.STATUS=='1'){
			        			  return "<button type=\"button\" class=\"btn btn-success\" style='padding:0px; font-size: 10px;' onclick=crawData('"+full.JOB_NAME+"','"+full.PLATFORM+"','"+full.JOB_CLASS_NAME+"')> 开始  </button>";	  
			        		  }else{
			        			  return "<span  class=\"label label-warning\" style=\' font-size: 10px;\'>关闭</span>";	  
			        		  }
			        	  }
			          },
			          {
			        	  field: 'IP',
			        	  title: '代理云接口', 
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  editable: {
				        	   type: 'select',
				        	   title: '代理云接口',
				        	   source:[{value:"1",text:"阿布云"},{value:"2",text:"阿布云+云代理"},{value:"3",text:"luminati"},{value:"4",text:"云代理"},{value:"5",text:"云代理+luminati"},{value:"6",text:"阿布云+luminati"},,{value:"7",text:"4G网卡"}]
				        	  }
			          },
			         {
			        	  field: 'DESCRIBE',
			        	  title: '描述', 
			        	  align: 'left', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  var dataName='';
			        		  if(data!=undefined){ 
			        			  if(data.length>3){
					  					dataName=data.substring(0,3)+"...";
					  				}else{
					  					dataName=data;  
					  				}  
			        		  }    
			  				return '<span class="textAuto" title='+'\''+data+'\''+'>'+dataName+'</span>';  
			        	  }
			          }, 
			          { 
			        	  field: 'STATUS',
			        	  //width :"2%",
			        	  title: '状态', 
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true, 
			        	  formatter:function(data,full,type,meta){//padding:0px;
			        		  if(data=='1'){
			        		  return '<span  class=\"label label-success\" style=\' font-size: 10px;\'>启用</span>';	  
			        		  }else{
			        		   return '<span  class=\"label label-warning\" style=\' font-size: 10px;\'>禁用</span>'	  
			        		  }
			        	  }
			          },
			          {    
			        	  field: 'SCHED_NAME',
			        	  title: '操作',
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  
			        	  formatter:function(data, full, type, meta){
			        		  return "<span style='text-align: right;'\><button type=\"button\" class=\"btn btn-success\" style='padding:0px; font-size: 5px;' onclick=handleResume('"+full.JOB_NAME+"','"+full.JOB_GROUP+"')>启用</button>&nbsp;&nbsp;" 
			        		         +"<button type=\"button\" class=\"btn btn-warning\" style='padding:0px; font-size: 5px;' onclick=handlePause('"+full.JOB_NAME+"','"+full.JOB_GROUP+"')>禁用</button>&nbsp;&nbsp;"
			        		         +"<button type=\"button\" class=\"btn btn-danger\" style='padding:0px; font-size: 5px;' onclick=handleDelete('"+full.JOB_NAME+"','"+full.JOB_GROUP+"')>删除</button>&nbsp;&nbsp;</span>";
			        		         
			        	  }   
			          } 

			          ],
			          onEditableSave: function (field, row, oldValue, $el) {
			        	   var url='';
			        	  if(field=='DATASOURCE'){
			        		  url='/job/reschedule_job_datasource?JOB_NAME='+row.JOB_NAME+'&DATASOURCE='+row.DATASOURCE;
			        	  }else if(field=='THREAD_SUM'){
			        		  url='/job/reschedule_job_thread?JOB_NAME='+row.JOB_NAME+'&THREAD_SUM='+row.THREAD_SUM+'&PLATFORM='+row.PLATFORM;
			        	  }else if(field=='INVENTORY'){
			        		  url='/job/reschedule_job_inventory?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&INVENTORY='+row.INVENTORY;
			        	  }else if(field=='CLIENT'){
			        		  url='/job/reschedule_job_client?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&CLIENT='+row.CLIENT;
			        	  }else if(field=='IP'){
			        		  url='/job/reschedule_job_interface?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&INTERFACE='+row.IP;  
			        	  }else if(field=='DATABASES'){
			        		  url='/job/reschedule_job_database?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&DATABASES='+row.DATABASES;  
			        	  }else if(field=='STORAGE'){
			        		  url='/job/reschedule_job_storage?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&STORAGE='+row.STORAGE;  
			        	  }else if(field=='SUMS'){
			        		  url='/job/reschedule_job_sums?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&SUMS='+row.SUMS;  
			        	  }else if(field=='DOCKER'){
			        		  var sear=new RegExp('SEARCH');
			        		  if(sear.test(row.JOB_NAME)){
			        			  url='/job/reschedule_job_docker?JOB_NAME='+row.JOB_NAME+'&PLATFORM='+row.PLATFORM+'&DOCKER='+row.DOCKER;  
			        		  }else{
			        			  alert('暂无权限修改.....');
			        			  $("#example2").bootstrapTable('refresh'); 
			        			  return;
			        		  }
			        	  }else{
			        		  url='/job/reschedulejob?cronExpression='+row.CRON_EXPRESSION+'&triggerName='+row.TRIGGER_NAME+'&jobGroupName='+row.JOB_GROUP ; 
			        	  } 
			              $.ajax({ 
			            	  type: "post",
			            	  url: url,
			                  success: function (data, status) {
			                      if (status == "success") {    
			                    	  $("#example2").bootstrapTable('refresh'); 
			                      }
			                  },
			                  error: function () {
			                      alert("Error");
			                  },
			                  complete: function () {

			                  } 
			              });
			          }
		});
		$('#example2').bootstrapTable('hideColumn', 'SUMS');
		$('#example2').bootstrapTable('hideColumn', 'DESCRIBE');
		$('#example2').bootstrapTable('hideColumn', 'JOB_CLASS_NAME');
		$('#example2').bootstrapTable('hideColumn', 'THREAD_SUM');	
		$('#example2').bootstrapTable('hideColumn', 'DOCKER');
		$('#example2').bootstrapTable('hideColumn', 'DATASOURCE');	
		$('#example2').bootstrapTable('hideColumn', 'IP');
} 

function handleResume(jobClassName,jobGroupName){
	$.post('/job/resumejob',{"jobClassName":jobClassName,"jobGroupName":jobGroupName},function (data){
		if(data.success=1){
			alert('定时任务已恢复!');	
		}else{
			alert('定时任务恢复失败!');	
		}
		$("#example2").bootstrapTable('refresh');
	});
	
}
function handlePause(jobClassName,jobGroupName){ 
	$.post('/job/pausejob',{'jobClassName':jobClassName,'jobGroupName':jobGroupName},function (data){
		if(data.success=1){
			alert('定时任务已经暂停运行!');	 
		}else{
			alert('定时任务暂停运行失败!');	
		}
		$("#example2").bootstrapTable('refresh');
	});	
}
function handleDelete(jobClassName,jobGroupName){
	$.post('/job/deletejob',{'jobClassName':jobClassName,'jobGroupName':jobGroupName},function (data){
		if(data.success=1){
			alert('定时任务删除成功!'); 
		}else{
			alert('定时任务删除失败!');
		}
		$("#example2").bootstrapTable('refresh');
	});	
}
function crawData(JOB_NAME,PLATFORM,JOB_CLASS_NAME ){//手动爬虫
	$.post('/job/crawlerMessageData',{'JOB_NAME':JOB_NAME,'PLATFORM':PLATFORM,'JOB_CLASS_NAME':JOB_CLASS_NAME},function (data){
		if(data.success=1){
			alert('数据爬取完成!');	 
		}else{
			alert('数据爬取失败!');	
		}
		$("#example2").bootstrapTable('refresh');
	});	
}
function addjobqrtz(){ 
	validateForm();//判断信息不能为空值
}; 
function validateForm(){  //bootstrapValidator
	$('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {/*输入框不同状态，显示图片的样式*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证*/
        	jobName: {/*键名username和input name值对应*/
                message: 'Task name cannot be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '任务名称不能为空'
                    }
                   
                }
            },
        	jobClassName: {/*键名username和input name值对应*/
                message: 'Task cannot be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '任务类不能为空'
                    }
                }
            },
          /*  triggersName: {键名username和input name值对应
                message: 'The trigger name cannot be empty',
                validators: {
                    notEmpty: {非空提示
                        message: '触发器名称不能为空'
                    }
                }
            },*/
            jobGroup: {/*键名username和input name值对应*/
                message: 'Task group can t be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '任务分组不能为空'
                    }
                }
            },
            cronExpression: {/*键名username和input name值对应*/
                message: 'Task group can t be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '表达式不能为空'
                    }
                }
            },//"promotion_status"
            database: {/*键名username和input name值对应*/
                message: 'Specifies that the database cannot be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '指定数据库不能为空'
                    }
                }
            },//"promotion_status"
            promotion_status: {/*键名username和input name值对应*/ 
                message: 'Task group can t be empty',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '表达式不能为空'
                    },
                    stringLength: {
                        min: 0,
                        max: 1,
                        message: '长度必须在0到1位之间'
                    },
                    regexp: {
                        regexp: /^[0-9_]+$/,
                        message: '只能输入数字(0到9之间)' 
                    }
                }
            }
        } 
    }).on('success.form.bv', function (e) {
	          $.ajax({
				cache : true,
				type : "post",
				data : $('#defaultForm').serialize(),
				async : false,
				url : '/job/addjob',
				success : function(data) {
					if (data.success = 1) {
						alert('创建定时任务成功!');	
					} else {
						alert('创建定时任务失败!');
					}
					$("#example2").bootstrapTable('refresh');
				},
				error : function(request) {
					alert('创建定时任务失败!'+request);
					console.log(request);
				}
			});
		});
	}