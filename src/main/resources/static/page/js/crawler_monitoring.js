$(function(){
	grabTotalNumberGoods();//查询爬虫商品总个数
	selectProducts();//获取每个用户查询详情信息
	
});
function selectProducts (){
	var url= '/job/generationGrabbing?pageNum=1&databases=CrawlElve';
		$("#example2").bootstrapTable('destroy');
		hotSaleTablePro=$('#example2').bootstrapTable({ 
			 url: url,
			 method: 'post',
			 url: url,
			cache: false,////是否使用缓存，默认为true  
			striped: true, //是否显示行间隔色
			search: true,  //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，
			showRefresh: true,//是否显示刷新按钮
			clickToSelect: true, //是否启用点击选中行 
			pagination: true,//是否显示分页（*）
			striped: true,//条纹
			pageSize: 5, //每页的记录行数（*）    
			pageList: [5],         //可供选择的每页的行数（*）  25, 50, 100
			sortOrder : 'desc',
			height:440,
			showColumns: true, //是否显示所有的列
			cardView: false, //是否显示详细视图
			showToggle:true, //是否显示详细视图和列表视图的切换按钮
			columns: [
			          {
			        	  field: 'platform_name',
			        	  title: '平台',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			                return  platformsData(data); 
			        	  }
			          },
			          {
			        	  field: 'cust_account_id',
			        	  title: '用户', 
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  return '<span  style=\' font-size: 15px;\'>'+data+'</span>';  
			        	  }
			          },
			          { 
			        	  field: 'sums',
			        	  title: '商品总数',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  formatter:function(data,full,type,meta){
			        		  return '<span  style=\' font-size: 18px; color:red;\'>'+data+'</span>';  
			        	  }
			          },
			          { 
			        	  field: 'for_craw',
			        	  title: '待抓商品',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  formatter:function(data,full,type,meta){//padding:0px;
			        		  if(data==0){
				        		  return '<span  class=\"label label-success\" style=\' font-size: 18px;\'>完成</span>';	  
				        		  }else{
				        		   return '<span  class=\"label label-warning\" style=\' font-size: 18px;\'>'+data+'</span>'	  
				        		  }
				        	  }
			        	  
			          },
			          {
			        	  field: 'complete',
			        	  title: '成功率',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
				        		var  dataMessage=data.split('.');
				        		if(parseInt(dataMessage)>=80){
				        			return '<span  style=\' font-size: 18px; color:green;\'>'+data+'</span>';	
				        		}else if(parseInt(dataMessage)>=70 && parseInt(dataMessage)<80){
				        			return '<span   style=\' font-size: 18px; color:#ffcc66;\'>'+data+'</span>';	
				        		}else {
				        			return '<span  style=\' font-size: 18px; color:red;\'>'+data+'</span>';	
				        		}
				        	  }
			          },
			          {
			        	  field: 'job_name',
			        	  title: '任务名称',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		  return '<span  style=\' font-size: 13px; \'>'+data+'</span>';  
			        	  }
			          },
			          {    
			        	  field: 'job_name',
			        	  title: '操作',
			        	  align: 'center', 
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data, full, type, meta){
			        		  var dataMessage='';
			        		  if(full.for_craw!=0){//onclick=crawlerMonitoring('CrawlElve','"+full.job_name+"')
			        			dataMessage="<span style=\'text-align: right;\'><button data-target=\'#myModal\' id='crawlerid' data-toggle=\'modal\' type=\'button\' class=\'btn btn-success\' style=\'padding:0px; font-size:18px;\' onclick=crawlerMonitoring('CrawlElve','"+full.job_name+"')>启动爬虫</button>&nbsp;&nbsp;";  
			        		  }else{
			        			  dataMessage="<span style=\'text-align: right;\'><button type=\'button\' class=\'btn btn-success\' style=\'padding:0px; font-size:18px; \' disabled=\'false\'>启动爬虫</button>&nbsp;&nbsp;";
			        		  }
			        		  return dataMessage;          
			        	  }   
			          }
			       ]    	 
	           });
} 
   // 抓取商品总数
    function   grabTotalNumberGoods(){
    	var url= '/job/grabTotalNumberGoodsData?pageNum=1&databases=CrawlElve';
		$("#exampleSum").bootstrapTable('destroy');
		hotSaleTablePro=$('#exampleSum').bootstrapTable({ 
			 url: url,
			 method: 'post',
			 url: url,
			cache: false,////是否使用缓存，默认为true  
			striped: true, //是否显示行间隔色
			search: false,  //是否显示表格搜索，此搜索是客户端搜索，不会进服务端， 
			showRefresh: false,//是否显示刷新按钮
			clickToSelect: true, //是否启用点击选中行 
			pagination: false,//是否显示分页（*）
			striped: true,//条纹
			pageSize: 15, //每页的记录行数（*）    
			pageList: [15],         //可供选择的每页的行数（*）  25, 50, 100
			sortOrder : 'desc',
			height:330,
			showColumns: false, //是否显示所有的列
			cardView: false, //是否显示详细视图
			showToggle:false, //是否显示详细视图和列表视图的切换按钮
			columns: [ 
			          {
			        	  field: 'platform_name',
			        	  title: '平台',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			                return  platformsData(data); 
			        	  }
			          },		
			          { 
			        	  field: 'sums',
			        	  title: '商品总数',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true ,
			        	  formatter:function(data,full,type,meta){
			        		  return '<span  style=\' font-size: 20px; color:red;\'>'+data+'</span>';  
			        	  }
			          },
			          { 
			        	  field: 'for_craw',
			        	  title: '待抓商品',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  visible: true,
			        	  sortable:true,
			        	  formatter:function(data,full,type,meta){//padding:0px;
			        		  if(data==0){
				        		  return '<span  class=\"label label-success\" style=\' font-size: 20px;\'>完成</span>';	  
				        		  }else{
				        		   return '<span  class=\"label label-warning\" style=\' font-size: 20px;\'>'+data+'</span>'	  
				        		  }
				        	  }
			        	  
			          },
			          {
			        	  field: 'complete',
			        	  title: '成功率',
			        	  align: 'center',
			        	  valign: 'middle',
			        	  sortable:true,
			        	  visible: true,
			        	  formatter:function(data,full,type,meta){
			        		var  dataMessage=data.split('.');
			        		if(parseInt(dataMessage)>=80){
			        			return '<span  style=\' font-size: 20px; color:green;\'>'+data+'</span>';	
			        		}else if(parseInt(dataMessage)>=70 && parseInt(dataMessage)<80){
			        			return '<span   style=\' font-size: 20px; color:#ffcc66;\'>'+data+'</span>';	
			        		}else {
			        			return '<span  style=\' font-size: 20px; color:red;\'>'+data+'</span>';	
			        		}
			        	  }
			          }
			       ]    	 
	           });     
   } 
	function crawlerMonitoring(databases,jobName){
		$("#modalBody").html("确定要启动: "+jobName+" 进行爬取数据？");	
		$("#databases").val(databases);
		$("#jobName").val(jobName);
	}
	function buttionSpanPower(obj){
		$.post('/job/crawlerMonitoringData',{"databases":$("#databases").val(),"jobName":$("#jobName").val()},function (data){
			if(data.success=1){
				alert('数据抓取完成!');	
			}else{
				alert('抓取数据失败!');	
			}
		});	
		$("#example2").bootstrapTable('refresh');
		$('#exampleSum').bootstrapTable('refresh');
		$('#myModal').modal('hide')
	}
	
	//更新商品状态
	function goodsStatus(){
		$.post('/job/crawlergoodsStatus',{"databases":'CrawlElve'},function (data){
			if(data.success=1){
				alert('商品状态更新完成!');	
			}else{
				alert('商品状态更新失败!');	
			}
			$("#example2").bootstrapTable('refresh');
			$('#exampleSum').bootstrapTable('refresh');
		});	
		
	}
	//抓取京东评论 已经所有商品上下时间
	function goodscomments(count,message){
		$("#commentsId").html("确定要获取: "+count+":"+message +"  数据？");
		$("#jobName").val(count);
		
	}
	function buttionSpanPowerComments(){
		$('#myComments').modal('hide');
		$.post('/job/crawlerMonitoringData',{"databases":'CrawlElve',"jobName":$("#jobName").val()},function (data){
		/*	if(data.success=1){ 
				alert('数据抓取完成!');	 
			}else{
				alert('抓取数据失败!');	
			}*/
		});	
		
	}
	